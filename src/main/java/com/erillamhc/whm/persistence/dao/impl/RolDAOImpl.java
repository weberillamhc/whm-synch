package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.BiddingDAO;
import com.erillamhc.whm.persistence.dao.RoleDAO;
import com.erillamhc.whm.persistence.dao.SimpleDAO;
import com.erillamhc.whm.persistence.entity.Bidding;
import com.erillamhc.whm.persistence.entity.Role;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class RolDAOImpl extends SimpleDAOImpl<Role, String> implements RoleDAO{

	@Override
	public List<Role> listAllRoles() throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT role FROM Role role";
		jql += " ORDER BY role.name ASC";
		return getResultList(jql, new HashMap());
	}

	@Override
	public Role findByName(String name) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT role FROM Role role";
		jql += " WHERE role.name = :name";
		Map<String, Object> params = new HashMap();
		params.put("name", name);
		return singleResult(jql, params);
	}

	@Override
	public Role findByIDWithUsers(String idRole) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT role FROM Role role";
		jql += " LEFT JOIN FETCH role.users users";
		jql += " WHERE role.idRole = :id";
		Map<String, Object> params = new HashMap();
		params.put("id", idRole);
		return singleResult(jql, params);
	}

	@Override
	public List<Role> listAllRolesWithPermissions() throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT role FROM Role role";
		jql += " LEFT JOIN FETCH role.permissions permissions";
		return getResultList(jql, new HashMap());
	}

	@Override
	public Role findByIdWithPermissions(String idRole) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT role FROM Role role";
		jql += " LEFT JOIN FETCH role.permissions permissions";
		jql += " WHERE role.idRole = :idRole";
		Map<String, Object> params = new HashMap();
		params.put("idRole", idRole);
		return singleResult(jql, params);
	}
}