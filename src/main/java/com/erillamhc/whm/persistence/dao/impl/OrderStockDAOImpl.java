package com.erillamhc.whm.persistence.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.OrderStockDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class OrderStockDAOImpl extends SimpleDAOImpl<OrdersStock, OrdersStockPK> implements OrderStockDAO {

    @Override
    public List<OrdersStock> findAllByUserAndBranch(Integer user, Integer branch) throws SimpleDAOException {
        EntityManager em = getEntityManager();
        String sql = "SELECT o FROM OrdersStock o "
                + "JOIN o.order ord JOIN ord.userBranchOffice userb JOIN userb.user "
                + "WHERE userb.user = :user AND userb.branchOffice = :branch";
        TypedQuery<OrdersStock> typedQuery = em.createQuery(sql, OrdersStock.class);
        typedQuery.setParameter("user", new User(user));
        typedQuery.setParameter("branch", new BranchOffice(branch));

        return typedQuery.getResultList();
    }

    @Override
    public OrdersStock findByStockAndOrder(Order order, Stock stock) throws SimpleDAOException {
        Map<String, Object> params = new HashMap<>();
        params.put("order", order);
        params.put("stock", stock);
        return uniqueResult("OrdersStock.findByStockAndOrder", params);
    }

    @Override
    public List<OrdersStock> findAllOrdersStockByOrder(String order) throws SimpleDAOException {
        EntityManager em = getEntityManager();
        String sql = "SELECT o FROM OrdersStock o "
                + "WHERE o.order = :order";
        TypedQuery<OrdersStock> typedQuery = em.createQuery(sql, OrdersStock.class);
        typedQuery.setParameter("order", new Order(order));

        return typedQuery.getResultList();
    }

    @Override
    public Boolean deleteOrderStock(Stock stockEntity, String order, String stock) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            //Actualizo la cantidad en stock
            getEntityManager().merge(stockEntity);

            OrdersStock orStkEntity = new OrdersStock(new Order(order), new Stock(stock));
            orStkEntity.setId(new OrdersStockPK(stock, order));

            if (!getEntityManager().contains(orStkEntity)) {
                OrdersStock merge = getEntityManager().merge(orStkEntity);
                getEntityManager().remove(merge);
            } else {
                getEntityManager().remove(orStkEntity);
            }

            transaction.commit();
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
        return true;
    }

    @Override
    public List<OrdersStock> findByBranchOfficeShipment(BranchOffice branchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT orderStock FROM OrdersStock orderStock";
        jql += " INNER JOIN FETCH orderStock.order orden";
        jql += " INNER JOIN FETCH orderStock.stock stock";
        jql += " INNER JOIN FETCH stock.product product";
        jql += " INNER JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " INNER JOIN FETCH orden.shipments shipment";
        jql += " INNER JOIN FETCH shipment.branchOffice branchOffice";
        jql += " LEFT JOIN FETCH orderStock.incidence incidence";
        jql += " WHERE branchOffice.idBranchoffice = :idBranchoffice";
        jql += " AND orderStock.incidence IS NULL";

        Map<String, Object> params = new HashMap<>();
        params.put("idBranchoffice", branchOffice.getIdBranchoffice());

        return getResultList(jql, params);

    }

    @Override
    public List<OrdersStock> findAllByShipment(String idShipment) throws SimpleDAOException {

        String jql = "";
        jql += " SELECT DISTINCT orderStock FROM OrdersStock orderStock";
        jql += " INNER JOIN FETCH orderStock.order orden";
        jql += " INNER JOIN FETCH orderStock.stock stock";
        jql += " INNER JOIN FETCH orden.shipments shipment";
        jql += " WHERE shipment.idShipment = :idShipment";

        Map<String, Object> params = new HashMap<>();
        params.put("idShipment", idShipment);

        return getResultList(jql, params);
    }
    
    public Integer quantityOrderStock(BranchOffice branchOffice, Stock stock) throws SimpleDAOException {
        EntityManager em = getEntityManager();
        String jpl = "SELECT SUM(o.quantity) FROM OrdersStock o " +
                "JOIN o.order orders " +
                "JOIN orders.userBranchOffice userBranchOffice " +
                "WHERE userBranchOffice.branchOffice = :branchOffice AND o.stock = :stock";

        Query query = em.createQuery(jpl);
        query.setParameter("branchOffice",branchOffice);
        query.setParameter("stock",stock);

        Integer result = 0;
        try {
            Number sumQuantity = (Number) query.getSingleResult();
            result = sumQuantity.intValue();
        } catch (NullPointerException ex) {
            result = 0;
        }

        return result;
    }

}
