package com.erillamhc.whm.persistence.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.StockProductDAO;
import com.erillamhc.whm.persistence.entity.StockProduct;
import com.erillamhc.whm.persistence.entity.StockProductPK;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class StockProductDAOImpl extends SimpleDAOImpl<StockProduct, StockProductPK> implements StockProductDAO{

//	@Override
//	public StockProduct findByID(StockProductPK id) throws SimpleDAOException {
//		// TODO Auto-generated method stub
//		return null;
//	}


	@Override
	public StockProduct findByProductAndBranchOffice(Integer idProduct, Integer idBranchOffice)throws SimpleDAOException {
		String sql = "";
		sql += " SELECT stockProduct FROM StockProduct stockProduct";
		sql += " INNER JOIN FETCH stockProduct.product product";
		sql += " INNER JOIN stockProduct.branchOffice branchOffice";
		sql += " WHERE product.idProduct = :idProduct AND branchOffice.idBranchoffice = :idBranchOffice";
		Map<String, Object> params = new HashMap<>();
		params.put("idProduct", idProduct);
		params.put("idBranchOffice", idBranchOffice);
		return  singleResult(sql, params);
	}

	@Override
	public List<StockProduct> listAllStockProductByBranchOffice(Integer idBranchOffice) throws SimpleDAOException {
		String sql = "";
		sql += " SELECT stockProduct FROM StockProduct stockProduct";
		sql += " INNER JOIN FETCH stockProduct.product product";
		sql += " INNER JOIN stockProduct.branchOffice branchOffice";
		sql += " WHERE branchOffice.idBranchoffice = :idBranchOffice";
		Map<String, Object> params = new HashMap<>();
		params.put("idBranchOffice", idBranchOffice);
		return  getResultList(sql, params);
	}



}