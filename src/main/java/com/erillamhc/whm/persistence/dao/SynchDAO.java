package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Synch;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public interface SynchDAO extends SimpleDAO<Synch,String >  {


    List<Synch> findAllByStatus(Integer status) throws SimpleDAOException;
}
