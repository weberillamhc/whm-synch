package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Stock;
import com.erillamhc.whm.persistence.entity.StockHistory;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public interface StockHistoryDAO extends SimpleDAO<StockHistory, String> {


    List<StockHistory> listAllStockHistoryByStock(String idStock) throws SimpleDAOException;

    StockHistory findByIDWithParent(String idStockHistory) throws SimpleDAOException;
}
