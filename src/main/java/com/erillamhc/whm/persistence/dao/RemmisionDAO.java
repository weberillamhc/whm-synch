package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface RemmisionDAO extends SimpleDAO<Remmision, String> {

    List<Remmision> listAllRemmisions() throws SimpleDAOException;

    List<Remmision> listAllRemmisionsByIdBranchOffice(Integer idBranchOffice) throws SimpleDAOException;

    Remmision findByRemissionkey(String remissionkey) throws SimpleDAOException;

    Remmision findByRemissionkeyAndIdNotEqual(String remissionkey, String idRemmision) throws SimpleDAOException;

    boolean saveStockWithRemmision(Remmision rem, List<ProductsRemmision> products, List<Stock> stocks, Integer idUser) throws SimpleDAOException;

    Remmision findByIdWithFiscalFund(String id) throws SimpleDAOException;

    Remmision findByIdWithFiscalFundAndSuppliers(String id) throws SimpleDAOException;

    boolean saveStockWithRemmision(Remmision remmision, List<ProductsRemmision> products, List<Stock> stocks, Integer idUser, Synch synchRemmision, Movement movementRemision, List<Synch> synchProductRemmisions, List<Movement> movementsProductRemmisions, List<Synch> synchStocks, List<Movement> movementsStocks) throws SimpleDAOException;

    Remmision findByIdWithUserAndRequisitionGob(String idREmmision)throws SimpleDAOException;
}
