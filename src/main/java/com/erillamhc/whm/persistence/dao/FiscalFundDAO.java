package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.FiscalFund;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface FiscalFundDAO extends SimpleDAO<FiscalFund, Integer> {

    List<FiscalFund> listAllFiscalFund() throws SimpleDAOException;


    List<FiscalFund> listAllFiscalFundByBranchOfficeWithProductExistence(Integer idBranchOffice) throws SimpleDAOException;

    FiscalFund findByKey(String key) throws SimpleDAOException;

    FiscalFund findByIDWithRequisitionGobs(Integer idFiscalFund) throws SimpleDAOException;
}
