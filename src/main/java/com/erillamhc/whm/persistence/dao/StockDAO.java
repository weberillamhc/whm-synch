package com.erillamhc.whm.persistence.dao;

import java.sql.Timestamp;
import java.util.List;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface StockDAO extends SimpleDAO<Stock, String> {

    //Stock getProductbyStock (Integer branchOffice) throws SimpleDAOException;

    List<Stock> findAllStockByOrder(String order) throws SimpleDAOException;

    List<Stock> findAllStocks() throws SimpleDAOException;

    List<Stock> listAllStockByBranchOffice(Integer idBranchOffice) throws SimpleDAOException;

    List<Stock> listAllStockByBranchOfficeAndStatus(Integer idBranchOffice, Integer status) throws SimpleDAOException;

    boolean saveStock(Stock stock, Stock oldStock, Movement movement) throws SimpleDAOException;

    boolean saveStockTransaction(Stock stock, OrdersStock orderStock,Boolean isUpdate) throws SimpleDAOException;

    boolean saveStockPrescriptionTransaction(Order order,MedicalPrescription prescription, List<Stock> listStock, List<OrdersStock> listOrderStock, Shipment shipment) throws SimpleDAOException;
    
    List<Stock> findAllByBranchOffice(Integer branch) throws SimpleDAOException;
		
    Stock findByIdWithProductRemmisionAndRemision(String idStock) throws SimpleDAOException;

    List<Stock> listAllStockWithExistenceByBranchOffice(Integer idBranchOffice) throws SimpleDAOException;

    List<Stock> listAllStockWithOutExistenceByBranchOffice(Integer idBranchOffice) throws SimpleDAOException;

    boolean saveStock(Stock stock, StockHistory stockHistory) throws SimpleDAOException;

    boolean saveStock(Stock oldStock, StockHistory oldStockHistory, Stock newStock, StockHistory newStockHistory) throws SimpleDAOException;

    Stock findByIDWithParent(String id) throws SimpleDAOException;

    List<Stock> findByIDWithParents(String id, List<Stock> parents) throws SimpleDAOException;
	
	Stock findStockById(String idStock) throws SimpleDAOException;

    boolean saveStock(Stock stock, StockHistory oldStockHistory, Stock newStock, StockHistory newStockHistory, Synch synchOldStock, Movement movementOldStock, Synch synchOldStockHistory, Movement movementOldStockHistory, Synch synchNewStock, Movement movementNewStock, Synch synchNewStockHistory, Movement movementNewStockHistory) throws SimpleDAOException;

    boolean saveStockTransaction(Stock stEntity, OrdersStock ordersStockEntity, Boolean isUpdate, Synch synchOrderStock, Movement movementOrderStock, Synch synchStock, Movement movementStock) throws SimpleDAOException;
    List<Stock> listProductMovements(Integer branchOffice, Timestamp startDate, Timestamp endDate) throws SimpleDAOException;

    List<Stock> findAllLot(Integer product, Integer branch, Integer ff) throws SimpleDAOException;

    Stock findByIDWithBranchOfficeAndParentAndProductAndProductRemmision(String idEntity) throws SimpleDAOException;
}
