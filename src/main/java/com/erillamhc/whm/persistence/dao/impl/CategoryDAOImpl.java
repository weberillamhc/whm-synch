package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.CategoryDAO;
import com.erillamhc.whm.persistence.entity.Category;
import com.erillamhc.whm.persistence.entity.Supplier;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryDAOImpl extends SimpleDAOImpl<Category, Integer> implements CategoryDAO {

    @Override
    public List<Category> listAllCateogies() throws SimpleDAOException {
        String jql = "";
        jql += " SELECT category FROM Category category";
        jql += " ORDER BY category.name ASC";
        return getResultList(jql,  new HashMap());
    }

    @Override
    public Category findByName(String name) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT category FROM Category category";
        jql += " WHERE category.name = :name";
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        return singleResult(jql, params);
    }

    @Override
    public Category findByIDWithProducts(Integer idCategory) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT category FROM Category category";
        jql += " INNER JOIN FETCH category.products product";
        jql += " WHERE category.idCategory = :idCategory";
        Map<String, Object> params = new HashMap<>();
        params.put("idCategory", idCategory);
        return singleResult(jql, params);
    }
}