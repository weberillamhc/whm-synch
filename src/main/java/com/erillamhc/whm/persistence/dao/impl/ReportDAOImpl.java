package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.ReportDAO;
import com.erillamhc.whm.persistence.entity.Category;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;

public class ReportDAOImpl extends SimpleDAOImpl<Category, Integer> implements ReportDAO {

    @Resource(lookup = "java:/whmDS")
    private DataSource dataSource;

    @Override
    public Connection getConnection() throws SimpleDAOException {
        try{
            return dataSource.getConnection();
        }catch (Exception e){
            throw  new SimpleDAOException(e);
        }
    }
}