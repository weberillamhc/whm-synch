package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Bidding;
import com.erillamhc.whm.persistence.entity.Supplier;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;


public interface SupplierDAO extends SimpleDAO<Supplier, Integer> {


    List<Supplier> listAllSuppliersByRequisitionGob(String idRequisitionGob) throws SimpleDAOException;

    List<Supplier> listAllSuppliers() throws SimpleDAOException;

    Supplier findBySupplierKey(String supplierKey) throws SimpleDAOException;

    Supplier findByIDWithBiddings(Integer idSupplier) throws SimpleDAOException;

    List<Supplier> findAllByBidding(Bidding bidding) throws SimpleDAOException;
}
