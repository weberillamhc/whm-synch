package com.erillamhc.whm.persistence.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.erillamhc.whm.persistence.dao.MovementDAO;
import com.erillamhc.whm.persistence.entity.Movement;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class MovementDAOImpl extends SimpleDAOImpl<Movement, Integer> implements MovementDAO{


    @Override
    public Movement findByEntityAndTypeAndIdEntity(String entity, Integer typeMovement, String idEntity) throws SimpleDAOException {
        String jql = "";
        jql += "SELECT movement FROM Movement movement";
        jql += " WHERE movement.entity = :entity AND movement.movementtype = :typeMovement AND movement.idEntity = :idEntity";
        Map<String, Object> params = new HashMap<>();
        params.put("entity", entity);
        params.put("typeMovement", typeMovement);
        params.put("idEntity", idEntity);
        return singleResult(jql, params);
    }
}