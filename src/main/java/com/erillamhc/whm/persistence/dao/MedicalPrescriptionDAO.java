package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.MedicalPrescription;
import com.erillamhc.whm.persistence.entity.Order;
import com.erillamhc.whm.persistence.entity.OrderPrescription;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public interface MedicalPrescriptionDAO extends SimpleDAO<MedicalPrescription, String> {

    List<MedicalPrescription> findAllPrescriptionByBranchOffice(Integer branch) throws SimpleDAOException;

    MedicalPrescription findPrescriptionByOrder(String order) throws  SimpleDAOException;

    String[] savePrescription(MedicalPrescription prescription, Order order, OrderPrescription orderPrescription) throws SimpleDAOException;

}
