package com.erillamhc.whm.persistence.dao.impl;

import com.erillamhc.whm.persistence.dao.DecreaseDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DecreaseDAOImpl extends SimpleDAOImpl<Decrease,String> implements DecreaseDAO {

    @Override
    public boolean saveDecrease(Order order, Decrease decrease) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().merge(order);

            getEntityManager().persist(decrease);

            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }


    @Override
    public List<Decrease> findAllByBranch(Integer branch,Integer user) throws SimpleDAOException {

        String sql = "SELECT d FROM Decrease d " +
                "JOIN FETCH d.order ord " +
                "JOIN FETCH ord.userBranchOffice usb " +
                "JOIN FETCH usb.user user " +
                "WHERE usb.branchOffice = :branch AND ord.status = 8";

        Map<String, Object> params = new HashMap<>();
        //params.put("user", new User( user ));
        params.put("branch",new BranchOffice(branch));

        return getResultList(sql, params);
    }

    @Override
    public boolean saveWithSynch(Order orderEntity, Decrease decreaseEntity, Movement movementDecrease, Movement movementOrder, Synch synchDecrease, Synch synchOrder) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().merge(orderEntity);

            getEntityManager().persist(decreaseEntity);


            movementDecrease.setIdEntity(decreaseEntity.getIdDecrease());
            getEntityManager().persist(movementDecrease);

            synchDecrease.setIdEntity(decreaseEntity.getIdDecrease());
            getEntityManager().persist(synchDecrease);

            getEntityManager().persist(movementOrder);
            getEntityManager().persist(synchOrder);


            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }

    }

    @Override
    public Decrease finByIdWithOrder(String idDecrease) throws SimpleDAOException {
        String sql = "SELECT decrease FROM Decrease decrease " +
                "JOIN FETCH decrease.order ord " +
                "WHERE decrease.idDecrease = :idDecrease";
        Map<String, Object> params = new HashMap<>();
        params.put("idDecrease",idDecrease);
        return singleResult(sql, params);
    }

}
