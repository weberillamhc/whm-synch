package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.ShipmentsDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class ShipmentsDAOImpl extends SimpleDAOImpl<Shipment, String> implements ShipmentsDAO {


    @Override
    public List<Shipment> findAllShipmentByPicking(Integer branch, Integer status) throws SimpleDAOException {
        EntityManager em = getEntityManager();

        String sql = "";

        sql = "SELECT s FROM Shipment s JOIN s.order ord JOIN ord.userBranchOffice usb " +
                "WHERE usb.branchOffice = :branch AND ord.status = :status";

        TypedQuery<Shipment> typedQuery = em.createQuery(sql, Shipment.class);

        typedQuery.setParameter("branch", new BranchOffice(branch));
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    @Override
    public List<Shipment> findAllShipmentByShip(Integer branch, Integer status) throws SimpleDAOException {
        EntityManager em = getEntityManager();

        //Solo los que tengan status de enviado en la orden pueden mostrarse en envios
        String sql = "SELECT s FROM Shipment s JOIN s.order ord JOIN ord.userBranchOffice usb " +
                "WHERE usb.branchOffice = :branch AND s.status = :status AND ord.status = 4";
        TypedQuery<Shipment> typedQuery = em.createQuery(sql, Shipment.class);

        typedQuery.setParameter("branch", new BranchOffice(branch));
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    @Override
	public List<Shipment> findAllByBranchOffice(Integer branch) throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String sql = "SELECT s FROM Shipment s where s.branchOffice = :branch";
        TypedQuery<Shipment> typedQuery = em.createQuery(sql, Shipment.class);
        
        BranchOffice branchEntity = new BranchOffice();
        branchEntity.setIdBranchoffice(branch);
        
        typedQuery.setParameter("branch", branchEntity);
        return typedQuery.getResultList();
	}

    @Override
    public List<Shipment> findAllByBranchOfficeAndStatus(BranchOffice branchOffice, Integer status) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT shipment FROM Shipment shipment";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice";
        jql += " INNER JOIN FETCH userBranchOffice.branchOffice origen";
        jql += " WHERE shipment.branchOffice = :branchOffice";
        jql += " AND shipment.status = :status";


        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", branchOffice);
        params.put("status", status);
        return getResultList(jql, params);
    }

    @Override
    public Shipment findByIDWithOrderStockProduct(String idShipment) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT shipment FROM Shipment shipment";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " INNER JOIN FETCH orden.ordersStocks orderStock";
        jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice";
        jql += " INNER JOIN FETCH userBranchOffice.branchOffice origen";
        jql += " INNER JOIN FETCH orderStock.stock stock";
        jql += " INNER JOIN FETCH stock.product product";
        jql += " INNER JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " WHERE shipment.idShipment = :idShipment";

        Map<String, Object> params = new HashMap<>();
        params.put("idShipment", idShipment);
        return singleResult(jql, params);
    }

    @Override
    public Shipment findByIDWithDestination(String idShipment) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT shipment FROM Shipment shipment";
        jql += " INNER JOIN FETCH shipment.branchOffice destino";
        jql += " WHERE shipment.idShipment = :idShipment";

        Map<String, Object> params = new HashMap<>();
        params.put("idShipment", idShipment);
        return singleResult(jql, params);
    }

    @Override
    public List<Shipment> findAllByBranchOfficePending(BranchOffice branchOffice, Integer statusEnviado, Integer statusTrancito) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT shipment FROM Shipment shipment";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice";
        jql += " INNER JOIN FETCH userBranchOffice.branchOffice origen";
        jql += " WHERE shipment.branchOffice = :branchOffice";
        jql += " AND (shipment.status = :statusEnviado OR shipment.status = :statusTrancito)";


        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", branchOffice);
        params.put("statusEnviado", statusEnviado);
        params.put("statusTrancito", statusTrancito);
        return getResultList(jql, params);
    }

    @Override
    public Shipment findByIDWithDestinationAndOrigin(String idShipment) throws SimpleDAOException {

        String jql = "";

        jql += " SELECT DISTINCT shipment FROM Shipment shipment";
        jql += " INNER JOIN FETCH shipment.branchOffice destino";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice";
        jql += " INNER JOIN FETCH userBranchOffice.branchOffice origen";
        jql += " WHERE shipment.idShipment = :idShipment";

        Map<String, Object> params = new HashMap<>();
        params.put("idShipment", idShipment);
        return singleResult(jql, params);
    }

    @Override
    public boolean updateWithStocks(Shipment shipmentEntity, List<StockHistory> histories) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            EntityManager em = getEntityManager();

            transaction.begin();

            em.merge(shipmentEntity);


            for (StockHistory s:histories) {
               em.persist(s);
            }
            transaction.commit();

            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }

    @Override
    public Shipment findByIDWithDestinationAndOrder(String idShipment) throws SimpleDAOException {
        String jql = "";

        jql += " SELECT DISTINCT shipment FROM Shipment shipment";
        jql += " INNER JOIN FETCH shipment.branchOffice destino";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " WHERE shipment.idShipment = :idShipment";

        Map<String, Object> params = new HashMap<>();
        params.put("idShipment", idShipment);
        return singleResult(jql, params);
    }


}
