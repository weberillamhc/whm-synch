package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.AddressBranchOffice;
import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface AddressBranchOfficeDAO extends SimpleDAO<AddressBranchOffice, Integer> {

	List<AddressBranchOffice> findAllAddressByIdBranchOffice(BranchOffice branch) throws SimpleDAOException;


    AddressBranchOffice findByBranchOffice(BranchOffice branchEntity) throws SimpleDAOException;
}
