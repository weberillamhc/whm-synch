package com.erillamhc.whm.persistence.dao.impl;


import java.util.List;

import javax.persistence.EntityManager;

import com.erillamhc.whm.persistence.dao.ProductsRemmisionDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ProductsRemmisionDAOImpl extends SimpleDAOImpl<ProductsRemmision, String> implements ProductsRemmisionDAO{


    @Override
    public ProductsRemmision findByLot(String lot) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT productRemmision FROM ProductsRemmision productRemmision";
        jql += " WHERE productRemmision.lot = :lot";
        Map<String, Object> params = new HashMap<>();
        params.put("lot", lot);
        return  singleResult(jql, params);
    }

    @Override
    public ProductsRemmision findByStock(String idStock) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT productRemmision FROM ProductsRemmision productRemmision";
        jql += " INNER JOIN FETCH productRemmision.stocks stock";
        jql += " WHERE stock.idStock = :idStock";
        Map<String, Object> params = new HashMap<>();
        params.put("idStock", idStock);
        return  singleResult(jql, params);
    }

    @Override
    public ProductsRemmision findByIDWithRemmision(String idProductremmison) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT productRemmision FROM ProductsRemmision productRemmision";
        jql += " INNER JOIN FETCH productRemmision.remmision remmision";
        jql += " WHERE productRemmision.idProductremmison = :idProductremmison";
        Map<String, Object> params = new HashMap<>();
        params.put("idProductremmison", idProductremmison);
        return  singleResult(jql, params);
    }


    @Override
    public List<ProductsRemmision> findAllByRemmisionId(String idRemmision) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT productRemmision FROM ProductsRemmision productRemmision";
        jql += " INNER JOIN productRemmision.remmision remmision";
        jql += " LEFT JOIN FETCH productRemmision.stocks stock";
        jql += " LEFT JOIN FETCH stock.product product";
        jql += " WHERE remmision.idRemmision = :idRemmision";
        Map<String, Object> params = new HashMap<>();
        params.put("idRemmision", idRemmision);
        return  getResultList(jql, params);
    }
}