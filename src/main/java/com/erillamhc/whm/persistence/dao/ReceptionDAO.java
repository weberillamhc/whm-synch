package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;


public interface ReceptionDAO extends SimpleDAO<Reception, String> {


	boolean saveWithReceptionStock(Reception reception, List<ReceptionStock> receptionStocks, ReceptionComment receptionComment, Shipment shipment, List<Stock> stocks, List<StockHistory> stockHistories, List<Incidence> incidences, List<OrdersStock> ordersStocks) throws SimpleDAOException;

	Reception findByShipment(Shipment shipment) throws SimpleDAOException;

    List<Reception> findAllByBranchOffice(BranchOffice branchOffice) throws SimpleDAOException;

    Reception findByIDWithDetail(String idReception) throws SimpleDAOException;

    Reception findByIDWithShipmentAndComment(String idReception) throws SimpleDAOException;
}
