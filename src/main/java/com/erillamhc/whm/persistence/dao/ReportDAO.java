package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Category;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.sql.Connection;

public interface ReportDAO extends SimpleDAO<Category, Integer> {

    Connection getConnection() throws SimpleDAOException;
}
