package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;


public interface ReceptionStockDAO extends SimpleDAO<ReceptionStock, String> {


    ReceptionStock findByIDWithReceptionAndOrderStock(String idReceptionStock) throws SimpleDAOException;
}
