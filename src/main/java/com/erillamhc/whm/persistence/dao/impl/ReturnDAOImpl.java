package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.ReturnDAO;
import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.ReturnProduct;
import com.erillamhc.whm.persistence.entity.ReturnStock;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public class ReturnDAOImpl extends SimpleDAOImpl<ReturnProduct, String> implements ReturnDAO {

    @Override
    public List<ReturnProduct> listAllByDestination(BranchOffice destination) throws SimpleDAOException {
        return null;
    }

    @Override
    public List<ReturnProduct> listAllByOrign(BranchOffice origin) throws SimpleDAOException {
        return null;
    }

    @Override
    public boolean saveWithReturnStock(ReturnProduct returns, List<ReturnStock> returnStocks) throws SimpleDAOException {
        return false;
    }

    @Override
    public ReturnProduct findByIDWithBranchsAndSupplier(String idEntity) throws SimpleDAOException {
        return null;
    }
}
