package com.erillamhc.whm.persistence.dao.impl;

import com.erillamhc.whm.persistence.dao.SynchDAO;
import com.erillamhc.whm.persistence.entity.Synch;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SynchDAOImpl extends SimpleDAOImpl<Synch, String> implements SynchDAO {


    @Override
    public List<Synch> findAllByStatus(Integer status) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT synch FROM Synch synch";
        jql += " WHERE synch.status = :status ";
//        jql += " ORDER BY synch.typeSynch, synch.registrationDate ASC ";

        Map<String, Object> params = new HashMap<>();
        params.put("status", status);
        return getResultList(jql, params);
    }
}