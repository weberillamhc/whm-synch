package com.erillamhc.whm.persistence.dao.impl;

import com.erillamhc.whm.persistence.dao.MedicalPrescriptionDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MedicalPrescriptionDAOImpl extends SimpleDAOImpl<MedicalPrescription, String> implements  MedicalPrescriptionDAO{

    @Override
    public List<MedicalPrescription> findAllPrescriptionByBranchOffice(Integer branch) throws SimpleDAOException {
        EntityManager em = getEntityManager();


        String sql = "SELECT m FROM MedicalPrescription m " +
                "JOIN FETCH m.orderPrescriptions ordp " +
                "JOIN FETCH ordp.order ord " +
                "JOIN FETCH ord.userBranchOffice usb " +
                "JOIN FETCH usb.user user "+
                "JOIN FETCH m.doctor dr " +
                "WHERE ord.status = 6 AND usb.branchOffice = :branch";

        return  em.createQuery(sql).setParameter("branch", new BranchOffice( branch )).getResultList();
    }

    @Override
    public MedicalPrescription findPrescriptionByOrder(String order) throws SimpleDAOException {
        Map<String, Object> params = new HashMap<>();
        params.put("order", new Order(order));
        return uniqueResult("MedicalPrescription.findPrescriptionByOrder", params);
    }

    @Override
    public String[] savePrescription(MedicalPrescription prescription, Order order,OrderPrescription orderPrescription) throws SimpleDAOException {
        String[] arrayId = new String[2];
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().persist(order);

            getEntityManager().persist(prescription);

            //OrderPrescription orderPrescription = new OrderPrescription();
            orderPrescription.setId( new OrderPrescriptionPK( order.getIdOrder(), prescription.getIdPrescription() ) );
            orderPrescription.setOrder(order);
            orderPrescription.setMedicalPrescription(prescription);


            getEntityManager().persist(orderPrescription);

            arrayId[0] = order.getIdOrder();
            arrayId[1] = prescription.getIdPrescription();

            transaction.commit();

            return arrayId;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }


}
