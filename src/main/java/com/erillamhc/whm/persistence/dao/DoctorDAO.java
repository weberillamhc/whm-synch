package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.Doctor;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface DoctorDAO extends SimpleDAO<Doctor, String> {
	
	List<Doctor> findDoctorByBranch(Integer branch) throws SimpleDAOException;
	
	Doctor findDoctorCurp(String curp,String idDoctor,Boolean isUpdate) throws SimpleDAOException;
	
	Doctor findProfessionalId(String id,Boolean isUpdate) throws SimpleDAOException;

    Doctor findByIDWithBranch(String idEntity) throws SimpleDAOException;
}
