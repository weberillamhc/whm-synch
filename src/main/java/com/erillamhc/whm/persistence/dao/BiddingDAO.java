package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.Bidding;
import com.erillamhc.whm.persistence.entity.Movement;
import com.erillamhc.whm.persistence.entity.Supplier;
import com.erillamhc.whm.persistence.entity.Synch;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface BiddingDAO extends SimpleDAO<Bidding, String> {

	Bidding findByBiddingKey(String biddingKey) throws SimpleDAOException;

    List<Bidding> findByAllBiddingKey(String biddingKey) throws SimpleDAOException;

    List<Bidding> listAllWithSuppliers() throws SimpleDAOException;

    List<Bidding> listAll() throws SimpleDAOException;

    Bidding findByIdWithRequisitionGobs(String idBidding) throws SimpleDAOException;

    Bidding findByIdWithSuppliers(String idBidding) throws SimpleDAOException;

    List<Bidding> findAllByNameAndBiddingKey(String name, String biddingKey)throws SimpleDAOException;

    boolean saveWithSynch(Bidding bidding, List<Supplier> suppliers, Movement movement, Synch synch) throws SimpleDAOException;
}
