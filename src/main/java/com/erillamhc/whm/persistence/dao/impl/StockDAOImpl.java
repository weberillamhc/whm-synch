package com.erillamhc.whm.persistence.dao.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.StockDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class StockDAOImpl extends SimpleDAOImpl<Stock, String> implements StockDAO {

    @Override
    public List<Stock> findAllStockByOrder(String order) throws SimpleDAOException {
        EntityManager em = getEntityManager();
        String sql = "SELECT st FROM Order ord INNER JOIN ord.ordersStocks os "
                + "INNER JOIN os.stock st where ord.idOrder = :orderId";
        TypedQuery<Stock> typedQuery = em.createQuery(sql, Stock.class);
        typedQuery.setParameter("orderId", order);

        return typedQuery.getResultList();
    }

    @Override
    public List<Stock> findAllStocks() throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT st FROM Stock st ";
        jql += " INNER JOIN FETCH st.branchOffice branchOffice";
        jql += " ORDER BY st.dateIn DESC";
        return getResultList(jql, new HashMap());
    }

    @Override
    public List<Stock> listAllStockByBranchOffice(Integer idBranchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT st FROM Stock st ";
        jql += " INNER JOIN FETCH st.product product";
        jql += " LEFT JOIN FETCH st.productsRemmision productsRemmision";
        jql += " LEFT JOIN FETCH productsRemmision.remmision remmision";
        jql += " WHERE st.branchOffice = :p_branchOffice";
        jql += " ORDER BY st.dateIn DESC";
        TypedQuery<Stock> typedQuery = getEntityManager().createQuery(jql, Stock.class);
        typedQuery.setParameter("p_branchOffice", new BranchOffice(idBranchOffice));
        return typedQuery.getResultList();
    }

    @Override
    public boolean saveStockTransaction(Stock stock, OrdersStock orderStock,Boolean isUpdate) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().merge(stock);

            if (isUpdate) {
                getEntityManager().merge(orderStock);
            } else {
                getEntityManager().persist(orderStock);
            }


            transaction.commit();

            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }

    }

    @Override
    public boolean saveStock(Stock stock, Stock oldStock, Movement movement) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().persist(stock);

            String messageOgigin = "Nuevo stock:\n";
            messageOgigin += "idStock: " + stock.getIdStock() + "\n";
            messageOgigin += "Cantidad asignada: " + stock.getQuantity() + "\n";
            messageOgigin += "Lugar: " + stock.getLocationkey() + "\n";
            if (oldStock != null) {
                messageOgigin += "\n";
                messageOgigin += "Anterior stock: \n";
                messageOgigin += "idStock: " + oldStock.getIdStock() + "\n";
                messageOgigin += "Cantidad anterior: " + (oldStock.getQuantity() + stock.getQuantity()) + "\n";
                messageOgigin += "Cantidad restante: " + oldStock.getQuantity() + "\n";
                messageOgigin += "Lugar: " + oldStock.getLocationkey() + "\n";
                getEntityManager().merge(oldStock);
            }
            movement.setOriginmovement(messageOgigin);

            getEntityManager().persist(movement);

            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }

    @Override
    public List<Stock> listAllStockByBranchOfficeAndStatus(Integer idBranchOffice, Integer status) throws SimpleDAOException {

        String jql = "";
        jql += " SELECT DISTINCT st FROM Stock st ";
        jql += " INNER JOIN FETCH st.product product";
        jql += " LEFT JOIN FETCH st.productsRemmision productsRemmision";
        jql += " LEFT JOIN FETCH productsRemmision.remmision remmision";
        jql += " WHERE st.branchOffice = :branchOffice";
        jql += " AND st.status = :status";
        jql += " ORDER BY st.dateIn DESC";
        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", new BranchOffice(idBranchOffice));
        params.put("status", status);
        return getResultList(jql, params);
    }

    @Override
    public List<Stock> findAllByBranchOffice(Integer branch) throws SimpleDAOException {
        EntityManager em = getEntityManager();
        String sql = "SELECT s FROM Stock s where s.branchOffice = :branch";
        TypedQuery<Stock> typedQuery = em.createQuery(sql, Stock.class);


        typedQuery.setParameter("branch", new BranchOffice(branch));

        return typedQuery.getResultList();
    }

    @Override
    public Stock findByIdWithProductRemmisionAndRemision(String idStock) throws SimpleDAOException {
        String jql = "";

        jql += " SELECT DISTINCT stock FROM Stock stock ";
        jql += " LEFT JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " LEFT JOIN FETCH productsRemmision.remmision remmision";
        jql += " WHERE stock.idStock = :idStock";

        Map<String, Object> params = new HashMap<>();
        params.put("idStock", idStock);

        return singleResult(jql, params);
    }

    @Override
    public List<Stock> listAllStockWithExistenceByBranchOffice(Integer idBranchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT st FROM Stock st ";
        jql += " INNER JOIN FETCH st.product product";
        jql += " LEFT JOIN FETCH st.productsRemmision productsRemmision";
        jql += " LEFT JOIN FETCH productsRemmision.remmision remmision";
        jql += " LEFT JOIN FETCH remmision.requisitionGob requisitionGob";
        jql += " LEFT JOIN FETCH requisitionGob.fiscalFund fiscalFund";
        jql += " WHERE st.branchOffice = :branchOffice";
        jql += " AND st.status != 0 AND st.quantity >0";
        jql += " ORDER BY st.dateIn DESC";

        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", new BranchOffice(idBranchOffice));

        return getResultList(jql, params);
    }

    @Override
    public List<Stock> listAllStockWithOutExistenceByBranchOffice(Integer idBranchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT st FROM Stock st ";
        jql += " INNER JOIN FETCH st.product product";
        jql += " LEFT JOIN FETCH st.productsRemmision productsRemmision";
        jql += " LEFT JOIN FETCH productsRemmision.remmision remmision";
        jql += " WHERE st.branchOffice = :branchOffice";
        jql += " AND (st.status = 0 OR st.quantity <= 0)";
        jql += " ORDER BY st.dateIn DESC";

        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", new BranchOffice(idBranchOffice));

        return getResultList(jql, params);
    }

    @Override
    public boolean saveStock(Stock stock, StockHistory stockHistory)  throws SimpleDAOException{
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().persist(stock);

            stockHistory.setStock(stock);

            getEntityManager().persist(stockHistory);



            transaction.commit();
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
        return true;

    }

    @Override
    public boolean saveStock(Stock oldStock, StockHistory oldStockHistory, Stock newStock, StockHistory newStockHistory) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().merge(oldStock);
            oldStockHistory.setStock(oldStock);
            getEntityManager().persist(oldStockHistory);

            getEntityManager().persist(newStock);
            newStockHistory.setStock(newStock);
            getEntityManager().persist(newStockHistory);

            transaction.commit();
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
        return true;
    }

    @Override
    public Stock findByIDWithParent(String idStock) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT stock FROM Stock stock ";
        jql += " LEFT JOIN FETCH stock.parent parent";
        jql += " WHERE stock.idStock = :idStock";

        Map<String, Object> params = new HashMap<>();
        params.put("idStock", idStock);

        return singleResult(jql, params);
    }

    @Override
    public List<Stock> findByIDWithParents(String idStock, List<Stock> parents) throws SimpleDAOException {
        Stock parent = findByIDWithParent(idStock);
        if(parent.getParent() == null){
            return parents;
        }
        parents.add(parent.getParent());
        return findByIDWithParents(parent.getParent().getIdStock(), parents);
    }


	@Override
	public Stock findStockById(String idStock) throws SimpleDAOException {
		//EntityManager em = getEntityManager();

        String sql = "SELECT s FROM Stock s " +
                "JOIN FETCH s.product pr " +
                "WHERE s.idStock = :idStock";

        Map<String, Object> params = new HashMap<>();
        params.put("idStock", idStock);

        return singleResult(sql, params);
		//return  em.createQuery(sql).setParameter("idStock", idStock).getResultList();
        
	}

    @Override
    public boolean saveStock(Stock oldStock, StockHistory oldStockHistory, Stock newStock, StockHistory newStockHistory,
                             Synch synchOldStock, Movement movementOldStock,
                             Synch synchOldStockHistory, Movement movementOldStockHistory,
                             Synch synchNewStock, Movement movementNewStock,
                             Synch synchNewStockHistory, Movement movementNewStockHistory) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            EntityManager em = getEntityManager();

            em.merge(oldStock);

            oldStockHistory.setStock(oldStock);
            em.persist(oldStockHistory);

            em.persist(newStock);

            newStockHistory.setStock(newStock);
            em.persist(newStockHistory);

            /**             SINCRONIZACION Y MOVIMIENTO          **/

            /*      OLD STOCK   */
            em.persist(synchOldStock);
            em.persist(movementOldStock);


            /*      OLD STOCK  HISTORY */

            synchOldStockHistory.setIdEntity(oldStockHistory.getIdStockHistory());
            movementOldStockHistory.setIdEntity(oldStockHistory.getIdStockHistory());
            em.persist(synchOldStockHistory);
            em.persist(movementOldStockHistory);

            /*      NEW     STOCK       */
            synchNewStock.setIdEntity(newStock.getIdStock());
            movementNewStock.setIdEntity(newStock.getIdStock());
            em.persist(synchNewStock);
            em.persist(movementNewStock);


            /*      OLD STOCK HISTORY  */
            synchNewStockHistory.setIdEntity(newStockHistory.getIdStockHistory());
            movementNewStockHistory.setIdEntity(newStockHistory.getIdStockHistory());
            em.persist(synchNewStockHistory);
            em.persist(movementNewStockHistory);


            transaction.commit();
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
        return true;
    }

    @Override
    public boolean saveStockTransaction(Stock stock, OrdersStock orderStock, Boolean isUpdate,
                                     Synch synchOrderStock,   Movement movementOrderStock,
                                     Synch synchStock,  Movement movementStock) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();
            EntityManager em = getEntityManager();


            em.merge(stock);

            if (isUpdate) {
                em.merge(orderStock);
            } else {
                em.persist(orderStock);
            }

            em.persist(synchOrderStock);
            em.persist(movementOrderStock);

            if(synchStock != null && movementStock != null){
                em.persist(synchStock);
                em.persist(movementStock);
            }



            transaction.commit();
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
        return true;
    }

    @Override
	public boolean saveStockPrescriptionTransaction(Order order, MedicalPrescription prescription, List<Stock> listStock, List<OrdersStock> listOrderStock, Shipment shipment) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            //Registro la orden
            getEntityManager().persist(order);
            
            //Registro la receta
            getEntityManager().persist(prescription);
            
			/*OrderPrescription oP = new OrderPrescription();
            oP.setMedicalPrescription(prescription);
            oP.setId(new OrderPrescriptionPK(order.getIdOrder(),prescription.getIdPrescription()));*/
            
            //getEntityManager().persist(oP);


            for (Stock stock : listStock) {
                getEntityManager().merge(stock);
            }

            for (OrdersStock orderStock : listOrderStock) {

                orderStock.setOrder(order);
                orderStock.getId().setFkIdorder(order.getIdOrder());

                getEntityManager().persist(orderStock);
            }

            getEntityManager().persist(shipment);

            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
	}



    @Override
    public List<Stock> listProductMovements(Integer branchOffice, Timestamp startDate, Timestamp endDate) throws SimpleDAOException {

        String jql = "";
        jql += " SELECT DISTINCT st FROM Stock st ";
        jql += " INNER JOIN FETCH st.product product";
        jql += " LEFT JOIN FETCH st.productsRemmision productsRemmision";
        jql += " LEFT JOIN FETCH productsRemmision.remmision remmision";
        jql += " LEFT JOIN FETCH remmision.requisitionGob requisitionGob";
        jql += " LEFT JOIN FETCH requisitionGob.fiscalFund fiscalFund";
        jql += " WHERE st.branchOffice = :branchOffice ";
        jql += " AND remmision.dateinput >= :startDate AND remmision.dateinput <= :endDate ";
        jql += " AND st.status != 0 ";
        jql += " ORDER BY st.dateIn DESC";


        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", new BranchOffice(branchOffice));
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        return getResultList(jql, params);
    }

    @Override
    public List<Stock> findAllLot(Integer product, Integer branch, Integer ff) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT stock FROM Stock stock ";
        jql += " JOIN FETCH stock.productsRemmision productRemmision ";
        jql += " JOIN FETCH productRemmision.remmision remmision ";
        jql += " JOIN FETCH remmision.requisitionGob requisitionGob ";
        jql += " WHERE stock.branchOffice = :branchOffice ";
        jql += " AND requisitionGob.fiscalFund = :fiscalFund ";
        jql += " AND stock.status != 0 AND stock.status != 3";
        jql += " AND stock.product = :product ";


        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", new BranchOffice( branch ));
        params.put("fiscalFund", new FiscalFund( ff ));
        params.put("product", new Product(product));

        return  getResultList(jql, params);
    }

    @Override
    public Stock findByIDWithBranchOfficeAndParentAndProductAndProductRemmision(String idStock) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT stock FROM Stock stock ";
        jql += " INNER JOIN FETCH stock.productsRemmision productRemmision ";
        jql += " INNER JOIN FETCH stock.branchOffice branchOffice ";
        jql += " INNER JOIN FETCH stock.product product ";
        jql += " LEFT JOIN FETCH stock.parent parent ";
        jql += " WHERE stock.idStock = :idStock ";

        Map<String, Object> params = new HashMap<>();
        params.put("idStock", idStock);

        return  singleResult(jql, params);
    }

}
