package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.IncidenceDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.transaction.UserTransaction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IncidenceDAOImpl extends SimpleDAOImpl<Incidence, String> implements IncidenceDAO {


    @Override
    public List<Incidence> findAllByBranchOffice(BranchOffice branchOffice) throws SimpleDAOException {
        String jql = "";

        jql += " SELECT DISTINCT incidence FROM Incidence incidence";
        jql += " INNER JOIN FETCH incidence.branchOffice origin";
        jql += " INNER JOIN FETCH incidence.product product";
        jql += " LEFT JOIN FETCH incidence.supplier supplier";
        jql += " LEFT JOIN FETCH incidence.supplierBranchOffice supplierBranchOffice";
        jql += " LEFT JOIN FETCH incidence.ordersStock ordersStock";
        jql += " LEFT JOIN FETCH ordersStock.id haydi";
        jql += " LEFT JOIN FETCH incidence.stock stock";
        jql += " LEFT JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " WHERE origin.idBranchoffice = :idBranchoffice";
        jql += " ORDER BY incidence.dateIncidence DESC";

        Map<String, Object> params = new HashMap<>();
        params.put("idBranchoffice", branchOffice.getIdBranchoffice());

        return getResultList(jql, params);
    }

    @Override
    public boolean saveWithSynch(OrdersStock ordersStock, Movement movement, Synch synch) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().merge(ordersStock);

            getEntityManager().persist(movement);

            getEntityManager().persist(synch);

            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }

    @Override
    public List<Incidence> findAllByBranchOfficeAndStatus(BranchOffice branchOffice, Integer status) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT incidence FROM IncidenceOrder incidence";
        jql += " INNER JOIN FETCH incidence.branchOffice branchOffice";
        jql += " INNER JOIN FETCH incidence.ordersStock orderStock";
        jql += " WHERE incidence.branchOffice = :branchOffice";
        jql += " AND orderStock.status = :status";

        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", branchOffice);
        params.put("status", status);

        return getResultList(jql, params);
    }

    @Override
    public Incidence findByIDWithOrderStock(String idIncidenceOrder) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT incidence FROM Incidence incidence";
        jql += " LEFT JOIN FETCH incidence.ordersStock orderStock";
        jql += " LEFT JOIN FETCH orderStock.id haydi";
        jql += " WHERE incidence.idIncidenceOrder = :idIncidenceOrder";

        Map<String, Object> params = new HashMap<>();
        params.put("idIncidenceOrder", idIncidenceOrder);

        return singleResult(jql, params);
    }

    @Override
    public List<Incidence> findAllBySupplierBranchOffice(BranchOffice supplierBranchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT incidence FROM Incidence incidence";
        jql += " INNER JOIN FETCH incidence.branchOffice branchOffice";
        jql += " INNER JOIN FETCH incidence.product product";
        jql += " LEFT JOIN FETCH incidence.ordersStock ordersStock";
        jql += " LEFT JOIN FETCH ordersStock.id haydi";
        jql += " LEFT JOIN FETCH incidence.stock stock";
        jql += " LEFT JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " WHERE incidence.supplierBranchOffice = :supplierBranchOffice";
        jql += " ORDER BY incidence.dateIncidence DESC";

        Map<String, Object> params = new HashMap<>();
        params.put("supplierBranchOffice", supplierBranchOffice);

        return getResultList(jql, params);
    }

    @Override
    public boolean saveWithSynch(Incidence incidence, Synch synch, Movement movement) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().persist(incidence);
            movement.setIdEntity(incidence.getIdIncidence());
            synch.setIdEntity(incidence.getIdIncidence());

            getEntityManager().persist(movement);
            getEntityManager().persist(synch);

            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }

    @Override
    public boolean updateWithSynch(Incidence incidence, Synch synch, Movement movement) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().merge(incidence);

            getEntityManager().persist(movement);
            getEntityManager().persist(synch);

            transaction.commit();
            return true;
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
    }

    @Override
    public Incidence findByIdFull(String idIncidence) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT incidence FROM Incidence incidence";
        jql += " INNER JOIN FETCH incidence.branchOffice origin";
        jql += " INNER JOIN FETCH incidence.product product";
        jql += " LEFT JOIN FETCH incidence.supplier supplier";
        jql += " LEFT JOIN FETCH incidence.supplierBranchOffice supplierBranchOffice";
        jql += " LEFT JOIN FETCH incidence.ordersStock ordersStock";
        jql += " LEFT JOIN FETCH ordersStock.id haydi";
        jql += " LEFT JOIN FETCH incidence.stock stock";
        jql += " LEFT JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " WHERE incidence.idIncidence = :idIncidence";

        Map<String, Object> params = new HashMap<>();
        params.put("idIncidence", idIncidence);

        return singleResult(jql, params);
    }
}
