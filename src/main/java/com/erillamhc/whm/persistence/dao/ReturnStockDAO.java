

package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.ReturnStock;


public interface ReturnStockDAO extends SimpleDAO<ReturnStock, String> {


}
