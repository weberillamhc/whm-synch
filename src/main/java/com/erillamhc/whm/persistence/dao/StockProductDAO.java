package com.erillamhc.whm.persistence.dao;

import java.util.List;
import com.erillamhc.whm.persistence.entity.Stock;
import com.erillamhc.whm.persistence.entity.StockProduct;
import com.erillamhc.whm.persistence.entity.StockProductPK;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface StockProductDAO extends SimpleDAO<StockProduct, StockProductPK>  {
	
	 StockProduct findByProductAndBranchOffice(Integer idProduct, Integer idBranchOffice) throws SimpleDAOException;
	 List<StockProduct> listAllStockProductByBranchOffice(Integer idBranchOffice)throws SimpleDAOException;

}
