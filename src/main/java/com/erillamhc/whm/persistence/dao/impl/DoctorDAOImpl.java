package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.erillamhc.whm.persistence.dao.DoctorDAO;
import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.Doctor;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class DoctorDAOImpl extends SimpleDAOImpl<Doctor, String> implements DoctorDAO {

	@Override
	public List<Doctor> findDoctorByBranch(Integer branch) throws SimpleDAOException {
        EntityManager em = getEntityManager();
        String sql = "SELECT d FROM Doctor d where d.branchOffice = :branch";
        TypedQuery<Doctor> typedQuery = em.createQuery(sql, Doctor.class);
        
        BranchOffice branchEntity = new BranchOffice();
        branchEntity.setIdBranchoffice(branch);
        
        typedQuery.setParameter("branch", branchEntity);
        // em.close();
        return typedQuery.getResultList();
	}

	@Override
	public Doctor findDoctorCurp(String curp,String idDoctor,Boolean isUpdate) throws SimpleDAOException {
		return null;
	}

	@Override
	public Doctor findProfessionalId(String id,Boolean isUpdate) throws SimpleDAOException {
		String sql = "SELECT d FROM Doctor d WHERE d.professionalId LIKE :id";
		Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        return singleResult(sql, params);
	}

    @Override
    public Doctor findByIDWithBranch(String idDoctor) throws SimpleDAOException {
        String sql = "SELECT d FROM Doctor d INNER JOIN FETCH d.branchOffice branchOffice WHERE d.idDoctor = :idDoctor";
        Map<String, Object> params = new HashMap<>();
        params.put("idDoctor", idDoctor);
        return singleResult(sql, params);
    }

}
