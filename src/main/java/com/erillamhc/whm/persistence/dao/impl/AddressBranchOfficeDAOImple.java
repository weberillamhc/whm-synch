package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.erillamhc.whm.persistence.dao.AddressBranchOfficeDAO;
import com.erillamhc.whm.persistence.entity.AddressBranchOffice;
import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class AddressBranchOfficeDAOImple extends SimpleDAOImpl<AddressBranchOffice, Integer> implements AddressBranchOfficeDAO {

	@Override
	public List<AddressBranchOffice> findAllAddressByIdBranchOffice(BranchOffice branch) throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String sql = "SELECT a FROM AddressBranchOffice a where a.branchOffice = :branch";
        TypedQuery<AddressBranchOffice> typedQuery = em.createQuery(sql, AddressBranchOffice.class);
        typedQuery.setParameter("branch", branch);

        //em.close();
        return typedQuery.getResultList();
	}

    @Override
    public AddressBranchOffice findByBranchOffice(BranchOffice branchOffice) throws SimpleDAOException {
       String jql = "";
       jql +=" SELECT ab FROM AddressBranchOffice ab";
       jql +=" WHERE ab.branchOffice = :branchOffice ";
       Map<String, Object> params = new HashMap<>();
       params.put("branchOffice", branchOffice);
       return singleResult(jql, params);
    }

}
