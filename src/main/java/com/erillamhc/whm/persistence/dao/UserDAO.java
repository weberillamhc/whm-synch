package com.erillamhc.whm.persistence.dao;

import java.sql.Connection;
import java.util.List;

import com.erillamhc.whm.persistence.entity.UserBranchOffice;
//import com.erillamhc.whm.persistence.entity.User;
import com.erillamhc.whm.persistence.entity.User;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface UserDAO extends SimpleDAO<User, Integer> {

    User auth(String email, String password) throws SimpleDAOException;

    User findByEmail(String email) throws SimpleDAOException;

    User findByEmailAndIdNotEqual(String email, Integer idUser) throws SimpleDAOException;

    boolean saveUserAndEmploye(User user, List<UserBranchOffice> userBranchOffice) throws SimpleDAOException;

    List<User> listAllUsers() throws SimpleDAOException;

    List<User> listAllUsersByBranchOffice(Integer idBranchOffice) throws SimpleDAOException;

    User findByIdWithBranchOffices(Integer idUser) throws SimpleDAOException;

    User findUserByOrderAndUser(String order, Integer status) throws SimpleDAOException;

    User findByIdWithBranchOfficesAndRole(int parseInt) throws SimpleDAOException;
}
