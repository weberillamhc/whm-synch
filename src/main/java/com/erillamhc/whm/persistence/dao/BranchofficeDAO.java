package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.AddressBranchOffice;
import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.Shipment;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface BranchofficeDAO extends SimpleDAO<BranchOffice, Integer>  {
	
	List<BranchOffice> findAllBranchJurisdictions() throws SimpleDAOException;
	
	List<BranchOffice> findAllByParent(Integer parentBranch) throws SimpleDAOException;
	
	boolean saveBranchAndAddress(BranchOffice branchOffice, List<AddressBranchOffice> listAddress) throws SimpleDAOException;

    BranchOffice findByBranchOfficeKey(String branchOfficeKey) throws SimpleDAOException;

	BranchOffice findByWarehouseId(String warehouseId) throws SimpleDAOException;

    List<BranchOffice> findAllByUser(Integer idUser) throws SimpleDAOException;
    
    List<BranchOffice> findByStatus(Integer status) throws SimpleDAOException;

    List<BranchOffice> listAllFull()  throws SimpleDAOException;

    BranchOffice findByIdWithParentAndJurisdiction(Integer id) throws SimpleDAOException;

    BranchOffice findByShipment(String idShipment) throws SimpleDAOException;

    BranchOffice findByStock(String idStock) throws SimpleDAOException;
}
