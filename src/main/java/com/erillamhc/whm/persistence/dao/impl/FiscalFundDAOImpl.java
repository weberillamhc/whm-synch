package com.erillamhc.whm.persistence.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.erillamhc.whm.persistence.dao.FiscalFundDAO;
import com.erillamhc.whm.persistence.entity.FiscalFund;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class FiscalFundDAOImpl extends SimpleDAOImpl<FiscalFund, Integer> implements FiscalFundDAO{

	@Override
	public List<FiscalFund> listAllFiscalFund() throws SimpleDAOException {
		return findAll();
	}

    @Override
    public List<FiscalFund> listAllFiscalFundByBranchOfficeWithProductExistence(Integer idBranchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT fiscalFund FROM FiscalFund fiscalFund";
        jql += " INNER JOIN fiscalFund.requisitionGobs requisitionGob";
        jql += " INNER JOIN requisitionGob.remmisions remmision";
        jql += " INNER JOIN remmision.productsRemmisions productsRemmision";
        jql += " INNER JOIN productsRemmision.stocks stock";
        jql += " INNER JOIN stock.branchOffice branchOffice";
        jql += " WHERE branchOffice.idBranchoffice = :idBranchOffice AND (stock.status = 1 OR stock.status = 2)";

        Map<String, Object> params = new HashMap();
        params.put("idBranchOffice", idBranchOffice);
	    return  getResultList(jql, params);
    }

    @Override
    public FiscalFund findByKey(String key) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT fiscalFund FROM FiscalFund fiscalFund";
        jql += " WHERE fiscalFund.key = :key";

        Map<String, Object> params = new HashMap();
        params.put("key", key);

        return  singleResult(jql, params);
    }

    @Override
    public FiscalFund findByIDWithRequisitionGobs(Integer idFiscalfund) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT fiscalFund FROM FiscalFund fiscalFund";
        jql += " LEFT JOIN FETCH fiscalFund.requisitionGobs requisitionGob";
        jql += " WHERE fiscalFund.idFiscalfund = :idFiscalfund";

        Map<String, Object> params = new HashMap();
        params.put("idFiscalfund", idFiscalfund);

        return  singleResult(jql, params);
    }


}