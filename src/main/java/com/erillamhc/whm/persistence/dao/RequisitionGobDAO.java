package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.Movement;
import com.erillamhc.whm.persistence.entity.RequisitionGob;
import com.erillamhc.whm.persistence.entity.Synch;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface RequisitionGobDAO extends SimpleDAO<RequisitionGob, String> {

    RequisitionGob findByRequisitiongKey(String requisitiongKey) throws SimpleDAOException;

    RequisitionGob findRequisitionGobAndRemmisions(String idRequisitionGob) throws SimpleDAOException;

    RequisitionGob findByRequisitiongKeyAndIdNotEqual(String requisitiongKey, String id) throws SimpleDAOException;

    List<RequisitionGob> listRequisitionGobByBranchOffice(Integer idBranchOffice) throws SimpleDAOException;

    List<RequisitionGob> findAllByBranchOffice(Integer branch) throws SimpleDAOException;


    RequisitionGob findByIDWithBranchOffice(String requisitionGobId) throws SimpleDAOException;

    boolean saveWithScyn(RequisitionGob requisitionGob, Synch synch, Movement movement) throws SimpleDAOException;

    RequisitionGob findByIDFiscalFundAndBidding(String idREquisitionGob) throws SimpleDAOException;
}
