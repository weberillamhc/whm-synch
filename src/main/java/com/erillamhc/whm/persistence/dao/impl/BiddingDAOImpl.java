package com.erillamhc.whm.persistence.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.BiddingDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class BiddingDAOImpl extends SimpleDAOImpl<Bidding, String> implements BiddingDAO{

	

	@Override
	public Bidding findByBiddingKey(String biddingKey) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("biddingKey", biddingKey);
		return uniqueResult("Bidding.findByBiddingKey", params);
	}

	@Override
	public List<Bidding> findByAllBiddingKey(String biddingKey) throws SimpleDAOException {
		String jql = "SELECT b FROM Bidding b WHERE b.biddingKey = :biddingKey";
		Map<String, Object> params = new HashMap<>();
		params.put("biddingKey", biddingKey);
		return getResultList(jql, params);
	}

	@Override
	public List<Bidding> listAllWithSuppliers() throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT bidding FROM Bidding bidding";
		jql += " LEFT JOIN FETCH bidding.suppliers suppliers";
		jql += " ORDER BY bidding.registrationDate DESC";
		return getResultList(jql,  new HashMap());
	}
	@Override
	public List<Bidding> listAll() throws SimpleDAOException {
		String jql = "";
		jql += " SELECT bidding FROM Bidding bidding";
		jql += " ORDER BY bidding.registrationDate DESC";
		return getResultList(jql,  new HashMap());
	}

    @Override
    public Bidding findByIdWithRequisitionGobs(String idBidding) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT bidding FROM Bidding bidding";
		jql += " LEFT JOIN FETCH bidding.requisitionGobs requisitionGob";
		jql += " WHERE bidding.idBidding = :idBidding";
		Map<String, Object> params = new HashMap();
		params.put("idBidding", idBidding);
		return singleResult(jql, params);
    }

	@Override
	public Bidding findByIdWithSuppliers(String idBidding) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT bidding FROM Bidding bidding";
		jql += " LEFT JOIN FETCH bidding.suppliers supplier";
		jql += " WHERE bidding.idBidding = :idBidding";
		Map<String, Object> params = new HashMap();
		params.put("idBidding", idBidding);
		return singleResult(jql, params);
	}

	@Override
	public List<Bidding> findAllByNameAndBiddingKey(String name, String biddingKey) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT bidding FROM Bidding bidding";
		jql += " WHERE bidding.biddingKey = :biddingKey AND bidding.name = :name";
		Map<String, Object> params = new HashMap();
		params.put("name", name);
		params.put("biddingKey", biddingKey);
		return getResultList(jql, params);
	}

    @Override
    public boolean saveWithSynch(Bidding bidding, List<Supplier> suppliers, Movement movement, Synch synch) throws SimpleDAOException {
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			EntityManager em = getEntityManager();

			transaction.begin();

			em.persist(bidding);

			for (Supplier s:suppliers) {
				s.getBiddings().add(bidding);
				em.merge(s);
			}

			movement.setIdEntity(bidding.getIdBidding());
			em.persist(movement);

			synch.setIdEntity(bidding.getIdBidding());
			em.persist(synch);

			transaction.commit();

			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}
    }


}