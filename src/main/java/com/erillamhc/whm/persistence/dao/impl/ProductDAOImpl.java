package com.erillamhc.whm.persistence.dao.impl;


import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.persistence.EntityManager;

import com.erillamhc.whm.persistence.dao.ProductDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class ProductDAOImpl extends SimpleDAOImpl<Product, Integer> implements ProductDAO{

	@Override
	public List<Product> listAllProducts() throws SimpleDAOException {
		String jql = "";
		jql += "SELECT DISTINCT product FROM Product product";
		jql += " LEFT JOIN FETCH product.categories";
		return getResultList(jql, new HashMap<String, Object>());
	}


//	@Override
//	public List<Product> findAllProductosByStock(Integer branch,Integer ff) throws SimpleDAOException {
//		EntityManager em = getEntityManager();
//
//		String sql = "SELECT DISTINCT p FROM Product p JOIN p.stocks st "
//				+ "JOIN st.productsRemmision prre JOIN prre.remmision re "
//				+ "JOIN re.requisitionGob regob "
//				+ "WHERE st.branchOffice = :branch AND regob.fiscalFund = :ff AND st.status <> 0 AND st.status <> 3 ORDER BY p.name";
//
//        TypedQuery<Product> typedQuery = em.createQuery(sql, Product.class);
//        typedQuery.setParameter("branch", new BranchOffice(branch));
//        typedQuery.setParameter("ff", new FiscalFund(ff));
//
//        return typedQuery.getResultList();
//	}
	@Override
	public List<Product> findAllProductosByStock(BranchOffice branchOffice, FiscalFund fiscalFund) throws SimpleDAOException {

		String jql = "";
		jql += " SELECT product FROM Product product";
		jql += " INNER JOIN product.stocks stock ";
		jql += " INNER JOIN stock.productsRemmision productRemmision ";
		jql += " INNER JOIN productRemmision.remmision remmision ";
		jql += " INNER JOIN remmision.requisitionGob requisitionGob ";
		jql += " WHERE stock.branchOffice = :branchOffice ";
		jql += " AND requisitionGob.fiscalFund = :fiscalFund ";
		jql += " AND stock.status <> 0 AND stock.status <> 3 ";
		jql += " ORDER BY product.name ";

		Map<String, Object> params = new HashMap<>();
		params.put("branchOffice", branchOffice);
		params.put("fiscalFund", fiscalFund);
		return getResultList(jql, params);
	}

	@Override
	public List<Product> listProductMovements(Integer branchOffice,Timestamp startDate, Timestamp endDate) throws SimpleDAOException {
		return null;
	}


	@Override
	public Product findWithRemmision(Integer id_product) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id_product);
		return uniqueResult("Product.findWithRemmision", params);
	}

	@Override
	public Product findWithRequisiton(Integer id_product) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id_product);
		return uniqueResult("Product.findWithRequisiton", params);
	}

	@Override
	public List<Product> listAllProductsSimple() throws SimpleDAOException {
		String jql = "";
		jql += " SELECT product FROM Product product";
		jql += " ORDER BY product.name ASC";
		return getResultList(jql, new HashMap());
	}

	@Override
	public Product findByIDWithCateories(Integer idProduct) throws SimpleDAOException {
		String jql = "SELECT p FROM Product p LEFT JOIN FETCH p.categories categories WHERE p.idProduct = :id";
		Map<String, Object> params = new HashMap();
		params.put("id", idProduct);
		return singleResult(jql, params);
	}

	@Override
	public Product findByProductKey(String productKey) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("productKey", productKey);
		return uniqueResult("Product.findByProductKey", params);
	}
	@Override
	public Product findByProductKeyAndIdNotEqual(String productKey, Integer idProduct) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("productKey", productKey);
		params.put("idProduct", idProduct);
		return uniqueResult("Product.findByProductKeyAndIdNotEqual", params);
	}


	@Override
	public List<Object[]> listAllProductsByOrder(String order) throws SimpleDAOException {
		EntityManager em = getEntityManager();

		String sql = "SELECT p,orst,prre,ff,st FROM Product p " +
				"JOIN p.stocks st " +
				"JOIN st.ordersStock orst " +
				"JOIN orst.order ord " +
				"JOIN st.productsRemmision prre " +
				"JOIN prre.remmision re " +
				"JOIN re.requisitionGob regob " +
				"JOIN regob.fiscalFund ff " +
				"WHERE ord.idOrder = :order ";

		List<Object[]> typedQuery = em.createQuery(sql).setParameter("order", order).getResultList();

		return typedQuery;
	}

	@Override
	public List<Object[]> listAllProductsByOrderAndStatus(String order,Integer status) throws SimpleDAOException {
		EntityManager em = getEntityManager();

		String sql = "SELECT p,orst,prre,ff FROM Product p " +
				"JOIN p.stocks st " +
				"JOIN st.ordersStock orst " +
				"JOIN orst.order ord " +
				"JOIN st.productsRemmision prre " +
				"JOIN prre.remmision re " +
				"JOIN re.requisitionGob regob " +
				"JOIN regob.fiscalFund ff " +
				"WHERE ord.idOrder = :order AND ord.status = :status";

		List<Object[]> typedQuery = em.createQuery(sql).setParameter("order", order).setParameter("status", status).getResultList();

		return typedQuery;
	}



}