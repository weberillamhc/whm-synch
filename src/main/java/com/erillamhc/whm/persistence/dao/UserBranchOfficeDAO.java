package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.User;
import com.erillamhc.whm.persistence.entity.UserBranchOffice;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface UserBranchOfficeDAO extends SimpleDAO<UserBranchOffice, Integer> {

	UserBranchOffice findByUser(Integer idUser) throws SimpleDAOException;
	
	List<UserBranchOffice> findByIdBranchOffice(Integer branch) throws SimpleDAOException;

	UserBranchOffice findByUserAndBranchOffice(Integer user, Integer branch) throws  SimpleDAOException;

	UserBranchOffice findByUserAndBranchOfficeWithOrders(User user, BranchOffice branch) throws  SimpleDAOException;

	UserBranchOffice findBranchOfficeByOrder(String order) throws SimpleDAOException;


	
}
