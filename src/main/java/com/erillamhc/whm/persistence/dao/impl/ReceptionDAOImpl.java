package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.ReceptionDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.transaction.UserTransaction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReceptionDAOImpl extends SimpleDAOImpl<Reception, String> implements ReceptionDAO {


    @Override
    public boolean saveWithReceptionStock(Reception reception, List<ReceptionStock> receptionStocks, ReceptionComment receptionComment, Shipment shipment, List<Stock> stocks, List<StockHistory> stockHistories, List<Incidence> incidences, List<OrdersStock> ordersStocks) throws SimpleDAOException {
        UserTransaction transaction = null;
        try {
            transaction = getUserTransaction();
            transaction.begin();

            getEntityManager().persist(reception);

            for(ReceptionStock receptionStock : receptionStocks){
                receptionStock.setReception(reception);
                getEntityManager().persist(receptionStock);
            }

            if(receptionComment != null){
                receptionComment.setReception(reception);
                getEntityManager().persist(receptionComment);
            }

            for (int i = 0; i < stocks.size(); i++) {
                Stock stock = stocks.get(i);
                getEntityManager().persist(stock);
                StockHistory stockHistory = stockHistories.get(i);
                stockHistory.setStock(stock);
                getEntityManager().persist(stockHistory);
            }

            for (Incidence incidence: incidences){
                getEntityManager().persist(incidence);
            }

            for (OrdersStock ordersStock: ordersStocks){
                getEntityManager().merge(ordersStock);
            }

            getEntityManager().merge(shipment);

            transaction.commit();
        } catch (Exception e) {
            simpleRollback(transaction);
            throw new SimpleDAOException(e);
        }
        return true;
    }

    @Override
    public Reception findByShipment(Shipment shipment) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT reception FROM Reception reception";
        jql += " WHERE reception.shipment = :shipment";
        Map<String, Object> params = new HashMap<>();
        params.put("shipment", shipment);
        return singleResult(jql, params);
    }

    @Override
    public List<Reception> findAllByBranchOffice(BranchOffice branchOffice) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT reception FROM Reception reception";
        jql += " INNER JOIN FETCH reception.shipment shipment";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice";
        jql += " INNER JOIN FETCH userBranchOffice.branchOffice destino";
        jql += " WHERE shipment.branchOffice = :branchOffice";

        Map<String, Object> params = new HashMap<>();
        params.put("branchOffice", branchOffice);

        return getResultList(jql, params);
    }

    @Override
    public Reception findByIDWithDetail(String idReception) throws SimpleDAOException {
        String jql = "";

        jql += " SELECT DISTINCT reception FROM Reception reception";
        jql += " INNER JOIN FETCH reception.shipment shipment";
        jql += " INNER JOIN FETCH shipment.order orden";
        jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice";
        jql += " INNER JOIN FETCH userBranchOffice.branchOffice origen";
        jql += " LEFT JOIN FETCH reception.receptionComment receptionComment";
        jql += " LEFT JOIN FETCH reception.receptionStocks receptionStock";
        jql += " LEFT JOIN FETCH receptionStock.ordersStock orderStock";
        jql += " LEFT JOIN FETCH orderStock.stock stock";
        jql += " LEFT JOIN FETCH stock.product product";
        jql += " LEFT JOIN FETCH stock.productsRemmision productsRemmision";
        jql += " WHERE reception.idReception = :idReception";

        Map<String, Object> params = new HashMap<>();
        params.put("idReception", idReception);

        return singleResult(jql, params);
    }

    @Override
    public Reception findByIDWithShipmentAndComment(String idReception) throws SimpleDAOException {
        String jql = "";

        jql += " SELECT DISTINCT reception FROM Reception reception";
        jql += " INNER JOIN FETCH reception.shipment shipment";
        jql += " WHERE reception.idReception = :idReception";

        Map<String, Object> params = new HashMap<>();
        params.put("idReception", idReception);

        return singleResult(jql, params);
    }
}
