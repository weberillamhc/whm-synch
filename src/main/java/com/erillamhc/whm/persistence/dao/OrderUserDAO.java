package com.erillamhc.whm.persistence.dao;


import com.erillamhc.whm.persistence.entity.OrderUser;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface OrderUserDAO extends SimpleDAO<OrderUser, Integer> {


    OrderUser findByIDWithUserAndOrder(int id) throws SimpleDAOException;
}
