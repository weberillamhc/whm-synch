package com.erillamhc.whm.persistence.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface OrderStockDAO extends SimpleDAO<OrdersStock, OrdersStockPK>  {
	
	List<OrdersStock> findAllByUserAndBranch(Integer user, Integer branch) throws SimpleDAOException;

	OrdersStock findByStockAndOrder(Order order, Stock stock) throws SimpleDAOException;

	List<OrdersStock> findAllOrdersStockByOrder(String order) throws  SimpleDAOException;

	Boolean deleteOrderStock(Stock stockEntity, String order, String stock) throws  SimpleDAOException;

    List<OrdersStock> findByBranchOfficeShipment(BranchOffice branchOffice) throws SimpleDAOException;

    List<OrdersStock> findAllByShipment(String idShipment) throws SimpleDAOException;
	
	Integer quantityOrderStock(BranchOffice branchOffice, Stock stock) throws SimpleDAOException;

}
