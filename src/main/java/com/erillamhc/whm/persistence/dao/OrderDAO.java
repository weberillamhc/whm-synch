package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;


public interface OrderDAO extends SimpleDAO<Order, String>  {
	
	Order getStockByOrder (String order) throws SimpleDAOException;

	Order findByStatus( Integer user, Integer branch, Integer status) throws SimpleDAOException;

	List<Order> findAllOrdersByUserAndBranch(Integer user,Integer branch) throws  SimpleDAOException;

	List<Object[]> findAllOrderByBranch(Integer branch) throws  SimpleDAOException;

	boolean saveDispensing(Order order, MedicalPrescription prescription) throws SimpleDAOException;

	List<Order> findByAllByUserAndOriginAndStatus(Integer userId, Integer originId, Integer status) throws SimpleDAOException;

	List<Order> orderHistory(BranchOffice branchOffice) throws  SimpleDAOException;

	Order findByIDWithOrigin(String idOrder) throws  SimpleDAOException;

    Order findByShipment(String idShipment) throws  SimpleDAOException;
}
