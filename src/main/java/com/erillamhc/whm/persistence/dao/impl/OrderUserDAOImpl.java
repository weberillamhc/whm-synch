package com.erillamhc.whm.persistence.dao.impl;

import com.erillamhc.whm.persistence.dao.OrderUserDAO;
import com.erillamhc.whm.persistence.entity.OrderUser;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.HashMap;
import java.util.Map;


public class OrderUserDAOImpl extends SimpleDAOImpl<OrderUser, Integer> implements OrderUserDAO {

    @Override
    public OrderUser findByIDWithUserAndOrder(int idOrderUser) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT orderUser FROM OrderUser orderUser";
        jql += " INNER JOIN FETCH orderUser.order orden ";
        jql += " INNER JOIN FETCH orderUser.user user ";
        jql += " WHERE orderUser.idOrderUser = :idOrderUser ";

        Map<String, Object> params = new HashMap<>();
        params.put("idOrderUser", idOrderUser);
        return singleResult(jql, params);
    }
}
