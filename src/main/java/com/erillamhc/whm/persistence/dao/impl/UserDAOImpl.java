package com.erillamhc.whm.persistence.dao.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.UserDAO;
import com.erillamhc.whm.persistence.entity.Order;
import com.erillamhc.whm.persistence.entity.UserBranchOffice;
import com.erillamhc.whm.persistence.entity.User;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class UserDAOImpl extends SimpleDAOImpl<User, Integer> implements UserDAO{

	@Override
	public User auth(String email, String password) throws SimpleDAOException {
		String jql ="";
		jql += " SELECT DISTINCT user FROM User user";
		jql += " LEFT JOIN FETCH user.role role";
		jql += " LEFT JOIN FETCH role.permissions permission";
		jql += " WHERE user.email = :email";
		Map<String, Object> params = new HashMap<>();
		params.put("email", email);
		return singleResult(jql, params);
	}

	@Override
	public User findByEmail(String email) throws SimpleDAOException {
		String jql = " SELECT user FROM User user WHERE email = :email";
		Map<String, Object> params = new HashMap<>();
		params.put("email", email);
		return singleResult(jql, params);
	}

	@Override
	public User findByEmailAndIdNotEqual(String email, Integer idUser) throws SimpleDAOException {
        String jql = " SELECT user FROM User user WHERE email = :email AND idUser <> :idUser";
		Map<String, Object> params = new HashMap<>();
		params.put("email", email);
		params.put("idUser", idUser);
		return singleResult(jql, params, User.class);
	}

	@Override
	public boolean saveUserAndEmploye(User user, List<UserBranchOffice> userBranchOffices) throws SimpleDAOException {
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			EntityManager em = getEntityManager();
			
			transaction.begin();
			
			em.persist(user);

			for (UserBranchOffice userBranchOffice:userBranchOffices) {
				userBranchOffice.setUser(user);
				em.persist(userBranchOffice);
			}

			transaction.commit();
			
			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}

	}

	@Override
	public List<User> listAllUsers() throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT user FROM User user";
		jql += " INNER JOIN FETCH user.role role";
		jql += " LEFT JOIN FETCH user.userBranchOffices userBranchOffices";
		jql += " LEFT JOIN FETCH userBranchOffices.branchOffice branchOffice";
		jql += " ORDER BY user.idUser DESC";
		Map<String, Object> params = new HashMap<>();
		return getResultList(jql, params);
	}

	@Override
	public List<User> listAllUsersByBranchOffice(Integer idBranchOffice) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT user FROM User user";
		jql += " INNER JOIN FETCH user.role role";
		jql += " INNER JOIN user.userBranchOffices userBranchOffices";
		jql += " INNER JOIN userBranchOffices.branchOffice branchOffice";
		jql += " WHERE branchOffice.idBranchoffice = :idBranchOffice";
		jql += " ORDER BY user.idUser DESC";
		Map<String, Object> params = new HashMap<>();
		params.put("idBranchOffice", idBranchOffice);
		return getResultList(jql, params);
	}

    @Override
    public User findByIdWithBranchOffices(Integer idUser) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT user FROM User user";
		jql += " LEFT JOIN FETCH user.userBranchOffices userBranchOffices";
		jql += " LEFT JOIN FETCH userBranchOffices.branchOffice branchOffice";
		jql += " WHERE user.idUser = :idUser";
		Map<String, Object> params = new HashMap();
		params.put("idUser", idUser);
		return singleResult(jql, params);
    }

	@Override
	public User findUserByOrderAndUser(String order, Integer status) throws SimpleDAOException {

		String jql = "SELECT us FROM OrderUser o " +
				"JOIN o.user us " +
				"WHERE o.status = :status " +
				"AND o.order = :order";

		Map<String, Object> params = new HashMap<>();
		params.put("status", status);
		params.put("order", new Order(order));

		return singleResult(jql, params);
	}

    @Override
    public User findByIdWithBranchOfficesAndRole(int idUser) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT user FROM User user";
		jql += " LEFT JOIN FETCH user.userBranchOffices userBranchOffices";
		jql += " LEFT JOIN FETCH userBranchOffices.branchOffice branchOffice";
		jql += " INNER JOIN FETCH user.role role";
		jql += " WHERE user.idUser = :idUser";
		Map<String, Object> params = new HashMap();
		params.put("idUser", idUser);
		return singleResult(jql, params);
    }

}