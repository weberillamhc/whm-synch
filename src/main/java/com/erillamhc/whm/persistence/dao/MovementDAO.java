package com.erillamhc.whm.persistence.dao;

import java.util.List;
import com.erillamhc.whm.persistence.entity.Movement;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface MovementDAO extends SimpleDAO<Movement, Integer>  {

    Movement findByEntityAndTypeAndIdEntity(String entity, Integer typeMovement, String idEntity) throws SimpleDAOException;
}
