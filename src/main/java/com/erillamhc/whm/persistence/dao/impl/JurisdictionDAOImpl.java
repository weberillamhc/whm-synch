package com.erillamhc.whm.persistence.dao.impl;

import com.erillamhc.whm.persistence.dao.JurisdictionDAO;
import com.erillamhc.whm.persistence.entity.Jurisdiction;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JurisdictionDAOImpl extends SimpleDAOImpl<Jurisdiction, Integer> implements JurisdictionDAO {

    @Override
    public List<Jurisdiction> findAllJurisdictions() throws SimpleDAOException {
        String jql = "SELECT jurisdiction FROM Jurisdiction jurisdiction ORDER BY jurisdiction.jurisdictionname ASC";
        return getResultList(jql, new HashMap());
    }

    @Override
    public List<Jurisdiction> findAllJurisdictionsWithBranchOffices() throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT jurisdiction FROM Jurisdiction jurisdiction";
        jql += " LEFT JOIN FETCH jurisdiction.branchOffices";
        jql += " ORDER BY jurisdiction.jurisdictionname ASC";
        return getResultList(jql, new HashMap());
    }

    @Override
    public Jurisdiction findByName(String jurisdictionname) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT jurisdiction FROM Jurisdiction jurisdiction";
        jql += " WHERE jurisdiction.jurisdictionname = :jurisdictionname";
        Map<String, Object> params = new HashMap<>();
        params.put("jurisdictionname", jurisdictionname);
        return singleResult(jql, params);
    }

    @Override
    public Jurisdiction findByIDWithBranchOffices(Integer idJurisdiction) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT jurisdiction FROM Jurisdiction jurisdiction";
        jql += " LEFT JOIN FETCH jurisdiction.branchOffices";
        jql += " WHERE jurisdiction.idJurisdiction = :idJurisdiction";
        Map<String, Object> params = new HashMap<>();
        params.put("idJurisdiction", idJurisdiction);
        return singleResult(jql, params);
    }
}
