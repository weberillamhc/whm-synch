package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.SupplierDAO;
import com.erillamhc.whm.persistence.entity.Bidding;
import com.erillamhc.whm.persistence.entity.Supplier;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplierDAOImpl extends SimpleDAOImpl<Supplier, Integer> implements SupplierDAO{


    @Override
    public List<Supplier> listAllSuppliersByRequisitionGob(String idRequisitionGob) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT supplier FROM Supplier supplier";
        jql += " INNER JOIN supplier.biddings bidding";
        jql += " INNER JOIN bidding.requisitionGobs requisitionGob";
        jql += " WHERE requisitionGob.idRequisitionGob = :idRequisitionGob";
        jql += " ORDER BY supplier.name ASC";
        Map<String, Object> params = new HashMap<>();
        params.put("idRequisitionGob", idRequisitionGob);
        return getResultList(jql, params);
    }

    @Override
    public List<Supplier> listAllSuppliers() throws SimpleDAOException {
        String jql = "";
        jql += " SELECT supplier FROM Supplier supplier";
        jql += " ORDER BY supplier.name ASC";
        return new ArrayList<>();
    }

    @Override
    public Supplier findBySupplierKey(String supplierKey) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT supplier FROM Supplier supplier";
        jql += " WHERE supplier.supplierKey = :supplierKey";
        Map<String, Object> params = new HashMap<>();
        params.put("supplierKey", supplierKey);
        return singleResult(jql, params);
    }

    @Override
    public Supplier findByIDWithBiddings(Integer idSupplier) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT supplier FROM Supplier supplier";
        jql += " LEFT JOIN FETCH supplier.biddings bidding";
        jql += " WHERE supplier.idSupplier = :idSupplier";
        Map<String, Object> params = new HashMap<>();
        params.put("idSupplier", idSupplier);
        return singleResult(jql, params);
    }

    @Override
    public List<Supplier> findAllByBidding(Bidding bidding) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT supplier FROM Supplier supplier";
        jql += " INNER JOIN FETCH supplier.biddings bidding";
        jql += " WHERE bidding = :bidding";
        Map<String, Object> params = new HashMap<>();
        params.put("bidding", bidding);
        return getResultList(jql, params);
    }


}