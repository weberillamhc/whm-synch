package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.Order;
import com.erillamhc.whm.persistence.entity.Shipment;
import com.erillamhc.whm.persistence.entity.StockHistory;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface ShipmentsDAO extends SimpleDAO<Shipment, String> {


	List<Shipment> findAllShipmentByPicking(Integer branch, Integer status) throws SimpleDAOException;
	List<Shipment> findAllShipmentByShip(Integer branch, Integer status) throws SimpleDAOException;
	List<Shipment> findAllByBranchOffice(Integer branch) throws SimpleDAOException;
	List<Shipment> findAllByBranchOfficeAndStatus(BranchOffice branchOffice, Integer status) throws SimpleDAOException;
    Shipment findByIDWithOrderStockProduct(String idShipment) throws SimpleDAOException;
    Shipment findByIDWithDestination(String idShipment) throws SimpleDAOException;
	List<Shipment> findAllByBranchOfficePending(BranchOffice branchOffice, Integer statusEnviado, Integer statusTrancito) throws SimpleDAOException;
    Shipment findByIDWithDestinationAndOrigin(String idShipment) throws SimpleDAOException;
    boolean updateWithStocks(Shipment shipmentEntity, List<StockHistory> histories) throws SimpleDAOException;


    Shipment findByIDWithDestinationAndOrder(String idEntity) throws SimpleDAOException;
}
