package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.BranchofficeDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class BranchOfficeDAOImpl extends SimpleDAOImpl<BranchOffice, Integer> implements BranchofficeDAO {

	
	
	
	@Override
	public List<BranchOffice> findAllBranchJurisdictions() throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String sql = "SELECT b FROM BranchOffice b INNER JOIN b.jurisdiction j";
        TypedQuery<BranchOffice> typedQuery = em.createQuery(sql, BranchOffice.class);

        //em.close();
        return typedQuery.getResultList();
	}

	@Override
	public boolean saveBranchAndAddress(BranchOffice branchOffice, List<AddressBranchOffice> listAddress) throws SimpleDAOException {
		
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			transaction.begin();
			
			//Registro la orden
			getEntityManager().persist(branchOffice);
			
			for (AddressBranchOffice address : listAddress) {
				address.setBranchOffice(branchOffice);
				getEntityManager().merge(address);
			}
			
			
			transaction.commit();
			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}
	}

    @Override
    public BranchOffice findByBranchOfficeKey(String branchOfficeKey) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT branchOffice FROM BranchOffice branchOffice";
		jql += " WHERE branchOffice.branchOfficeKey = :branchOfficeKey";
		Map<String, Object> params = new HashMap();
		params.put("branchOfficeKey", branchOfficeKey);
        return singleResult(jql, params);
    }

    @Override
    public BranchOffice findByWarehouseId(String warehouseId) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT branchOffice FROM BranchOffice branchOffice";
		jql += " WHERE branchOffice.warehouseId = :warehouseId";
		Map<String, Object> params = new HashMap();
		params.put("warehouseId", warehouseId);
		return singleResult(jql, params);
    }

    @Override
    public List<BranchOffice> findAllByUser(Integer idUser) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT branchOffice FROM BranchOffice branchOffice";
		jql += " INNER JOIN branchOffice.usersBranchOffice usersBranchOffice";
		jql += " INNER JOIN usersBranchOffice.user user";
		jql += " WHERE user.idUser = :idUser";
		Map<String, Object> params = new HashMap();
		params.put("idUser", idUser);
		return getResultList(jql, params);
    }

    @Override
	public List<BranchOffice> findAllByParent(Integer parentBranch) throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String sql = "SELECT b FROM BranchOffice b WHERE b.branchOffice = :parent";
        TypedQuery<BranchOffice> typedQuery = em.createQuery(sql, BranchOffice.class);
        
        BranchOffice parent = new BranchOffice();
        parent.setIdBranchoffice(parentBranch);
        
        typedQuery.setParameter("parent", parent);

        return typedQuery.getResultList();
	}

	@Override
	public List<BranchOffice> findByStatus(Integer status) throws SimpleDAOException {
		
		EntityManager em = getEntityManager();
		String sql = "SELECT b FROM BranchOffice b WHERE b.status = :status";
		
        TypedQuery<BranchOffice> typedQuery = em.createQuery(sql, BranchOffice.class);
        
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
	}

    @Override
    public List<BranchOffice> listAllFull() throws SimpleDAOException {
		String jql = "";
		jql +=" SELECT DISTINCT branchOffice FROM BranchOffice branchOffice";
		jql +=" LEFT JOIN FETCH branchOffice.branchOffice parent";
		jql +=" LEFT JOIN FETCH branchOffice.jurisdiction jurisdiction";
		jql +=" LEFT JOIN FETCH branchOffice.addressBranchOffice addressBranchOffice";
		jql +=" ORDER BY branchOffice.idBranchoffice DESC";
        return getResultList(jql, new HashMap());
    }

    @Override
    public BranchOffice findByIdWithParentAndJurisdiction(Integer idBranchoffice) throws SimpleDAOException {
		String jql = "";
		jql +=" SELECT DISTINCT branchOffice FROM BranchOffice branchOffice";
		jql +=" LEFT JOIN FETCH branchOffice.branchOffice parent";
		jql +=" LEFT JOIN FETCH branchOffice.jurisdiction jurisdiction";
		jql +=" LEFT JOIN FETCH branchOffice.addressBranchOffice addressBranchOffice";
		jql +=" WHERE branchOffice.idBranchoffice = :idBranchoffice";
		Map<String, Object> params = new HashMap<>();
		params.put("idBranchoffice", idBranchoffice);
		return singleResult(jql, params);
    }

    @Override
    public BranchOffice findByShipment(String idShipment) throws SimpleDAOException {
		String jql = "";
		jql +=" SELECT DISTINCT branchOffice FROM BranchOffice branchOffice";
		jql +=" INNER JOIN FETCH branchOffice.shipments shipment";
		jql +=" WHERE shipment.idShipment = :idShipment";
		Map<String, Object> params = new HashMap<>();
		params.put("idShipment", idShipment);
		return singleResult(jql, params);
    }

    @Override
    public BranchOffice findByStock(String idStock) throws SimpleDAOException {
		String jql = "";
		jql +=" SELECT DISTINCT branchOffice FROM BranchOffice branchOffice";
		jql +=" INNER JOIN FETCH branchOffice.stocks stock";
		jql +=" WHERE stock.idStock = :idStock";
		Map<String, Object> params = new HashMap<>();
		params.put("idStock", idStock);
		return singleResult(jql, params);
    }

}
