package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Decrease;
import com.erillamhc.whm.persistence.entity.Movement;
import com.erillamhc.whm.persistence.entity.Order;
import com.erillamhc.whm.persistence.entity.Synch;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public interface DecreaseDAO extends SimpleDAO<Decrease, String> {

    boolean saveDecrease(Order order, Decrease decrease) throws SimpleDAOException;
    List<Decrease> findAllByBranch(Integer branch,Integer user) throws SimpleDAOException;


    boolean saveWithSynch(Order orderEntity, Decrease decreaseEntity, Movement movementDecrease, Movement movementOrder, Synch synchDecrease, Synch synchOrder) throws SimpleDAOException;

    Decrease finByIdWithOrder(String idDecrease) throws SimpleDAOException;
}
