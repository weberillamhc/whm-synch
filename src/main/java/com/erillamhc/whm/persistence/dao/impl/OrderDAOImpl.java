package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.erillamhc.whm.persistence.dao.OrderDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

public class OrderDAOImpl extends SimpleDAOImpl<Order, String> implements OrderDAO {
	
	@Override
	public Order getStockByOrder(String order) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("orderId", order);
		return uniqueResult("Stock.findByStock", params);
	}

	@Override
	public Order findByStatus(Integer user, Integer branch, Integer status) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("user", new User(user));
		params.put("branch", new BranchOffice(branch));
		params.put("status", status);

		return uniqueResult("Order.findByStatus", params);
	}

	@Override
	public List<Order> findAllOrdersByUserAndBranch(Integer user,Integer branch) throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String query = "SELECT o FROM Order o " +
				"JOIN FETCH o.userBranchOffice ub " +
				"JOIN FETCH ub.user us " +
				"JOIN FETCH o.shipments sp " +
				"JOIN FETCH sp.branchOffice bra " +
				"WHERE ub.branchOffice = :branch";

		//"WHERE ub.user = :user AND ub.branchOffice = :branch";
		//return  em.createQuery(query).setParameter("user", new User(user)).setParameter("branch", new BranchOffice(branch)).getResultList();

		return  em.createQuery(query).setParameter("branch", new BranchOffice(branch)).getResultList();
	}

	@Override
	public List<Object[]> findAllOrderByBranch(Integer branch) throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String query = "SELECT o,br,usb FROM Order o " +
				"JOIN o.shipments sh " +
				"JOIN sh.branchOffice br " +
				"JOIN o.userBranchOffice usb " +
				"WHERE usb.branchOffice = :branch";

		return  em.createQuery(query).setParameter("branch", new BranchOffice(branch)).getResultList();
	}

	@Override
	public boolean saveDispensing(Order order,MedicalPrescription prescription) throws SimpleDAOException {
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			transaction.begin();

			getEntityManager().merge(order);

			getEntityManager().merge(prescription);

			transaction.commit();
			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}
	}

    @Override
    public List<Order> findByAllByUserAndOriginAndStatus(Integer userId, Integer originId, Integer status) throws SimpleDAOException {
		String jql = "SELECT o FROM Order o JOIN o.userBranchOffice usbr WHERE usbr.user = :user AND usbr.branchOffice = :branch AND o.status = :status";
		Map<String, Object> params = new HashMap<>();
		params.put("user", new User(userId));
		params.put("branch", new BranchOffice(originId));
		params.put("status", status);

		return getResultList(jql, params);
    }

	@Override
	public List<Order> orderHistory(BranchOffice branchOffice) throws SimpleDAOException {

		String jql = "SELECT o FROM Order o " +
				"RIGHT JOIN FETCH o.shipments shipments " +
				"RIGHT JOIN FETCH shipments.branchOffice " +
				"RIGHT JOIN FETCH o.userBranchOffice userBranchOffice " +
				"RIGHT JOIN FETCH userBranchOffice.user user " +
				"WHERE userBranchOffice.branchOffice = :branch " +
				"AND o.status <= 4 ";

		Map<String, Object> params = new HashMap<>();
		params.put("branch", branchOffice);

		return getResultList(jql, params);

	}

    @Override
    public Order findByIDWithOrigin(String idOrder) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT orden FROM Order orden";
		jql += " INNER JOIN FETCH orden.userBranchOffice userBranchOffice ";
		jql += " INNER JOIN FETCH userBranchOffice.branchOffice branchOffice ";
		jql += " INNER JOIN FETCH userBranchOffice.user user ";
		jql += " WHERE orden.idOrder = :idOrder ";

		Map<String, Object> params = new HashMap<>();
		params.put("idOrder", idOrder);
		return singleResult(jql, params);
    }

    @Override
    public Order findByShipment(String idShipment) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT orden FROM Order orden";
		jql += " INNER JOIN FETCH orden.shipments shipment ";
		jql += " WHERE shipment.idShipment = :idShipment ";

		Map<String, Object> params = new HashMap<>();
		params.put("idShipment", idShipment);
		
		return singleResult(jql, params);
    }

}
