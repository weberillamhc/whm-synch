package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Jurisdiction;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;
import java.util.List;

public interface JurisdictionDAO extends SimpleDAO<Jurisdiction, Integer> {

    List<Jurisdiction> findAllJurisdictions() throws SimpleDAOException;

    List<Jurisdiction> findAllJurisdictionsWithBranchOffices()  throws SimpleDAOException;

    Jurisdiction findByName(String jurisdictionname)  throws SimpleDAOException;

    Jurisdiction findByIDWithBranchOffices(Integer idJurisdiction) throws SimpleDAOException;
}
