package com.erillamhc.whm.persistence.dao.impl;


import com.erillamhc.whm.persistence.dao.ReceptionCommentDAO;
import com.erillamhc.whm.persistence.dao.ReceptionStockDAO;
import com.erillamhc.whm.persistence.entity.ReceptionComment;
import com.erillamhc.whm.persistence.entity.ReceptionStock;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.HashMap;
import java.util.Map;


public class ReceptionStockDAOImpl extends SimpleDAOImpl<ReceptionStock, String> implements ReceptionStockDAO {


    @Override
    public ReceptionStock findByIDWithReceptionAndOrderStock(String idReceptionStock) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT DISTINCT receptionStock FROM ReceptionStock receptionStock";
        jql += " INNER JOIN FETCH receptionStock.ordersStock ordersStock";
        jql += " INNER JOIN FETCH receptionStock.reception reception";
        jql += " WHERE receptionStock.idReceptionStock = :idReceptionStock";
        Map<String, Object> params = new HashMap<>();
        params.put("idReceptionStock", idReceptionStock);
        return  singleResult(jql, params);
    }
}
