

package com.erillamhc.whm.persistence.dao;


import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.ReturnProduct;
import com.erillamhc.whm.persistence.entity.ReturnStock;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public interface ReturnDAO extends SimpleDAO<ReturnProduct, String> {

    List<ReturnProduct> listAllByDestination(BranchOffice destination) throws SimpleDAOException;

    List<ReturnProduct> listAllByOrign(BranchOffice origin) throws SimpleDAOException;

    boolean saveWithReturnStock(ReturnProduct returns, List<ReturnStock> returnStocks) throws SimpleDAOException;

    ReturnProduct findByIDWithBranchsAndSupplier(String idEntity) throws SimpleDAOException;
}
