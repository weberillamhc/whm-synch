package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;


public interface IncidenceDAO extends SimpleDAO<Incidence, String> {


    List<Incidence> findAllByBranchOffice(BranchOffice branchOffice) throws SimpleDAOException;

    boolean saveWithSynch(OrdersStock incidence, Movement movement, Synch synch) throws SimpleDAOException;

    List<Incidence> findAllByBranchOfficeAndStatus(BranchOffice branchOffice, Integer status) throws SimpleDAOException;

    Incidence findByIDWithOrderStock(String idIncidenceOrder) throws SimpleDAOException;

    List<Incidence> findAllBySupplierBranchOffice(BranchOffice branchOffice) throws SimpleDAOException;

    boolean saveWithSynch(Incidence incidence, Synch synch, Movement movement) throws SimpleDAOException;

    boolean updateWithSynch(Incidence incidence, Synch synch, Movement movement) throws SimpleDAOException;

    Incidence findByIdFull(String idEntity) throws SimpleDAOException;
}
