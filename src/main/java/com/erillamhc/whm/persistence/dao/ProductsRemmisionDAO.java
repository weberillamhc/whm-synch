package com.erillamhc.whm.persistence.dao;

import java.util.List;

import com.erillamhc.whm.persistence.entity.ProductsRemmision;
import com.erillamhc.whm.persistence.entity.Remmision;
import com.erillamhc.whm.persistence.entity.RequisitionGob;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public interface ProductsRemmisionDAO extends SimpleDAO<ProductsRemmision, String> {


    List<ProductsRemmision> findAllByRemmisionId(String id) throws SimpleDAOException;
    
	ProductsRemmision findByLot(String lot) throws SimpleDAOException;

    ProductsRemmision findByStock(String idStock) throws SimpleDAOException;

    ProductsRemmision findByIDWithRemmision(String idSynch) throws SimpleDAOException;
}
