package com.erillamhc.whm.persistence.dao.impl;

import com.erillamhc.whm.persistence.dao.StockHistoryDAO;
import com.erillamhc.whm.persistence.entity.Stock;
import com.erillamhc.whm.persistence.entity.StockHistory;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockHistoryDAOImpl extends SimpleDAOImpl<StockHistory, String> implements StockHistoryDAO {


    @Override
    public List<StockHistory> listAllStockHistoryByStock(String idStock) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT stockHistory FROM StockHistory stockHistory ";
        jql += " INNER JOIN FETCH stockHistory.stock stock";
        jql += " WHERE stock.idStock = :idStock";
        jql += " ORDER BY stockHistory.dateIn DESC";

        Map<String, Object> params = new HashMap<>();
        params.put("idStock", idStock);

        return getResultList(jql, params);
    }

    @Override
    public StockHistory findByIDWithParent(String idStockHistory) throws SimpleDAOException {
        String jql = "";
        jql += " SELECT stockHistory FROM StockHistory stockHistory ";
        jql += " INNER JOIN FETCH stockHistory.stock stock";
        jql += " WHERE stockHistory.idStockHistory = :idStockHistory";

        Map<String, Object> params = new HashMap<>();
        params.put("idStockHistory", idStockHistory);

        return singleResult(jql, params);
    }
}
