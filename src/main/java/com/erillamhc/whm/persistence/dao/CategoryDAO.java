package com.erillamhc.whm.persistence.dao;

import com.erillamhc.whm.persistence.entity.Category;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;


public interface CategoryDAO extends SimpleDAO<Category, Integer> {


    List<Category> listAllCateogies() throws SimpleDAOException;

    Category findByName(String name)  throws SimpleDAOException;

    Category findByIDWithProducts(Integer idCategory)  throws SimpleDAOException;
}
