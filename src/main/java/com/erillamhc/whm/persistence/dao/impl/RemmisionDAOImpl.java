package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.RemmisionDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public class RemmisionDAOImpl extends SimpleDAOImpl<Remmision, String> implements RemmisionDAO {

	@Override
	public List<Remmision> listAllRemmisions() throws SimpleDAOException {
		String sql = "SELECT remmision FROM Remmision remmision ORDER BY remmision.dateinput DESC";
		TypedQuery<Remmision> typedQuery = getEntityManager().createQuery(sql, Remmision.class);
		List<Remmision> result = typedQuery.getResultList();
		return result;
	}

	@Override
	public Remmision findByRemissionkey(String remmisionKey) throws SimpleDAOException {
		Map<String, Object> params = new HashMap();
		params.put("remmisionKey", remmisionKey);
		return uniqueResult("Remmision.findByRemmisionKey", params, Remmision.class);
	}

	@Override
	public List<Remmision> listAllRemmisionsByIdBranchOffice(Integer idBranchOffice) throws SimpleDAOException {
//		String sql = "SELECT DISTINCT r FROM Remmision r ";
//		sql += " INNER JOIN FETCH r.requisitionGob rqg ";
//		sql += " INNER JOIN rqg.branchOffice bO ";
//		sql += " INNER JOIN rqg.bidding bidding ";
//		sql += " INNER JOIN FETCH rqg.bidding bidding ";
//		sql += " LEFT JOIN FETCH bidding.suppliers suppliers ";
//		sql += " WHERE bO.idBranchoffice = :idBranchoffice";
//		sql += " ORDER BY  r.dateinput DESC";
//		TypedQuery<Remmision> typedQuery = getEntityManager().createQuery(sql, Remmision.class);
//		typedQuery.setParameter("idBranchoffice", idBranchOffice);
//		List<Remmision> result = typedQuery.getResultList();
//		return result;

		String jql = "";
		jql += " SELECT  DISTINCT remmision FROM Remmision remmision";
		jql += " INNER JOIN FETCH remmision.requisitionGob requisitionGob";
		jql += " INNER JOIN FETCH requisitionGob.branchOffice branchOffice";
		jql += " INNER JOIN FETCH requisitionGob.bidding bidding";
		jql += " INNER JOIN FETCH  bidding.suppliers suppliers";
		jql += " WHERE branchOffice.idBranchoffice = :idBranchoffice";
		Map<String, Object> params = new HashMap<>();
		params.put("idBranchoffice", idBranchOffice);

		return getResultList(jql, params);
	}

	@Override
	public Remmision findByRemissionkeyAndIdNotEqual(String remmisionKey, String idRemmision)
			throws SimpleDAOException {
		Map<String, Object> params = new HashMap();
		params.put("remmisionKey", remmisionKey);
		params.put("idRemmision", idRemmision);
		return uniqueResult("Remmision.findByRemissionkeyAndIdNotEqual", params, Remmision.class);
	}

	@Override
	public boolean saveStockWithRemmision(Remmision remmision, List<ProductsRemmision> productsRemmisions, List<Stock> stocks, Integer idUser)
			throws SimpleDAOException {
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			transaction.begin();
			
			getEntityManager().persist(remmision);
			
			
			for (int i = 0; i < productsRemmisions.size(); i++) {
				ProductsRemmision productsRemmision = productsRemmisions.get(i);
				getEntityManager().persist(productsRemmision);
				Stock stock = stocks.get(i);
				stock.setProductsRemmision(productsRemmision);
				getEntityManager().persist(stock);

				StockHistory stockHistory = new StockHistory();
				stockHistory.setQuantity(stock.getQuantity());
				stockHistory.setLocationkey(stock.getLocationkey());
				stockHistory.setStatus(stock.getStatus());
				stockHistory.setDateIn(stock.getDateIn());
				stockHistory.setStock(stock);
				stockHistory.setUserId(idUser);
				getEntityManager().persist(stockHistory);

			}
			
			transaction.commit();
			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}
	}

    @Override
    public Remmision findByIdWithFiscalFund(String idRemmision) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT  DISTINCT remmision FROM Remmision remmision";
		jql += " INNER JOIN FETCH remmision.requisitionGob requisitionGob";
		jql += " INNER JOIN FETCH requisitionGob.fiscalFund fiscalFund";
		jql += " WHERE remmision.idRemmision = :idRemmision";
		Map<String, Object> params = new HashMap<>();
		params.put("idRemmision", idRemmision);

		return singleResult(jql, params);
    }

    @Override
    public Remmision findByIdWithFiscalFundAndSuppliers(String idRemmision) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT  DISTINCT remmision FROM Remmision remmision";
		jql += " INNER JOIN FETCH remmision.requisitionGob requisitionGob";
		jql += " INNER JOIN FETCH requisitionGob.fiscalFund fiscalFund";
		jql += " INNER JOIN FETCH requisitionGob.bidding bidding";
		jql += " LEFT JOIN FETCH  bidding.suppliers suppliers";
		jql += " LEFT JOIN FETCH  remmision.user user";
		jql += " WHERE remmision.idRemmision = :idRemmision";

		Map<String, Object> params = new HashMap<>();
		params.put("idRemmision", idRemmision);

		return singleResult(jql, params);
    }

	@Override
	public boolean saveStockWithRemmision(Remmision remmision, List<ProductsRemmision> productsRemmisions, List<Stock> stocks, Integer idUser, Synch synchRemmision, Movement movementRemision, List<Synch> synchProductRemmisions, List<Movement> movementsProductRemmisions, List<Synch> synchStocks, List<Movement> movementsStocks) throws SimpleDAOException {
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			transaction.begin();
			EntityManager em = getEntityManager();

			em.persist(remmision);

			/*			REMMISION		*/
			synchRemmision.setIdEntity(remmision.getIdRemmision());
			movementRemision.setIdEntity(remmision.getIdRemmision());
			em.persist(synchRemmision);
			em.persist(movementRemision);

			for (int i = 0; i < productsRemmisions.size(); i++) {
				ProductsRemmision productsRemmision = productsRemmisions.get(i);
				em.persist(productsRemmision);
				Stock stock = stocks.get(i);
				stock.setProductsRemmision(productsRemmision);
				em.persist(stock);

				StockHistory stockHistory = new StockHistory();
				stockHistory.setQuantity(stock.getQuantity());
				stockHistory.setLocationkey(stock.getLocationkey());
				stockHistory.setStatus(stock.getStatus());
				stockHistory.setDateIn(stock.getDateIn());
				stockHistory.setStock(stock);
				stockHistory.setUserId(idUser);
				em.persist(stockHistory);

				/*			PRODUCT REMMISION	*/
				Synch synchProductRemmision = synchProductRemmisions.get(i);
				Movement movementProductRemmision  = movementsProductRemmisions.get(i);

				movementProductRemmision.setIdEntity(productsRemmision.getIdProductremmison());
				synchProductRemmision.setIdEntity(productsRemmision.getIdProductremmison());

				em.persist(synchProductRemmision);
				em.persist(movementProductRemmision);

				/*			STOCK		*/

				Synch synchStock = synchStocks.get(i);
				Movement movementStock = movementsStocks.get(i);

				synchStock.setIdEntity(stock.getIdStock());
				movementStock.setIdEntity(stock.getIdStock());

				em.persist(synchStock);
				em.persist(movementStock);

				/*			STOCK HISTORY		*/

				Synch synchStockHistory = new Synch();
				synchStockHistory.setIdEntity(stockHistory.getIdStockHistory());
				synchStockHistory.setEntity("stock_history");
				synchStockHistory.setTypeSynch(synchStock.getTypeSynch());
				synchStockHistory.setDateSynch(synchStock.getDateSynch());
				synchStockHistory.setRegistrationDate(synchStock.getRegistrationDate());
				synchStockHistory.setIdBranchOffice(synchStock.getIdBranchOffice());
				synchStockHistory.setIdUser(synchStock.getIdUser());
				synchStockHistory.setDataEntity(synchStock.getDataEntity());
				synchStockHistory.setStatus(synchStock.getStatus());

				Movement movementStockHistory = new Movement();
				movementStockHistory.setIdEntity(stockHistory.getIdStockHistory());
				movementStockHistory.setEntity("stock_history");
				movementStockHistory.setDatemovement(movementStock.getDatemovement());
				movementStockHistory.setUserId(movementStock.getUserId());
				movementStockHistory.setOriginmovement(movementStock.getOriginmovement());
				movementStockHistory.setData(movementStock.getData());
				movementStockHistory.setIdBranchOffice(movementStock.getIdBranchOffice());
				movementStockHistory.setMovementtype(movementStock.getMovementtype());

				em.persist(synchStockHistory);
				em.persist(movementStockHistory);

			}

			transaction.commit();
			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}
	}

    @Override
    public Remmision findByIdWithUserAndRequisitionGob(String idRemmision) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT  DISTINCT remmision FROM Remmision remmision";
		jql += " INNER JOIN FETCH remmision.requisitionGob requisitionGob";
		jql += " LEFT JOIN FETCH  remmision.user user";
		jql += " WHERE remmision.idRemmision = :idRemmision";

		Map<String, Object> params = new HashMap<>();
		params.put("idRemmision", idRemmision);

		return singleResult(jql, params);
    }

}