package com.erillamhc.whm.persistence.dao;

import java.sql.Timestamp;
import java.util.List;

import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

public interface ProductDAO extends SimpleDAO<Product, Integer> {

//    List<Product> findAllProductosByStock(Integer branch, Integer ff) throws SimpleDAOException;

    List<Product> listAllProducts() throws SimpleDAOException;

    Product findByProductKey(String productKey) throws SimpleDAOException;

    Product findByProductKeyAndIdNotEqual(String productKey, Integer idProduct) throws SimpleDAOException;

    Product findWithRemmision(Integer id_product) throws SimpleDAOException;

    Product findWithRequisiton(Integer id_product) throws SimpleDAOException;


    List<Product> listAllProductsSimple() throws SimpleDAOException;

    Product findByIDWithCateories(Integer idProduct) throws SimpleDAOException;
    
    List<Object[]> listAllProductsByOrder(String order) throws SimpleDAOException;

    List<Object[]> listAllProductsByOrderAndStatus(String order,Integer status) throws SimpleDAOException;

    List<Product> findAllProductosByStock(BranchOffice branchOffice, FiscalFund fiscalFund) throws SimpleDAOException;

    List<Product> listProductMovements(Integer branchOffice,Timestamp startDate, Timestamp endDate) throws SimpleDAOException;



}
