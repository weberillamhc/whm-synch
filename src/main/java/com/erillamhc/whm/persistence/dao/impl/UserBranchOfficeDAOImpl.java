package com.erillamhc.whm.persistence.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.erillamhc.whm.persistence.dao.UserBranchOfficeDAO;
import com.erillamhc.whm.persistence.entity.BranchOffice;
import com.erillamhc.whm.persistence.entity.Order;
import com.erillamhc.whm.persistence.entity.User;
import com.erillamhc.whm.persistence.entity.UserBranchOffice;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class UserBranchOfficeDAOImpl extends SimpleDAOImpl<UserBranchOffice, Integer> implements UserBranchOfficeDAO {

	@Override
	public UserBranchOffice findByUser(Integer idUser) throws SimpleDAOException {
		String jql =  "SELECT userBranchOffice FROM UserBranchOffice userBranchOffice ";
		jql += " INNER JOIN FETCH userBranchOffice.user user";
		jql += " INNER JOIN FETCH userBranchOffice.branchOffice branchOffice";
		jql += " WHERE user.idUser = :idUser";
		Map<String, Object> params = new HashMap<>();
		params.put("idUser", idUser);
		List<UserBranchOffice> userBranchOffices = getResultList(jql, params);
		return   (userBranchOffices != null && !userBranchOffices.isEmpty()) ? userBranchOffices.get(0) : null;
	}

	@Override
	public List<UserBranchOffice> findByIdBranchOffice(Integer branch) throws SimpleDAOException {
		EntityManager em = getEntityManager();
		String sql = "SELECT e FROM UserBranchOffice e where e.branchOffice = :branch";
        TypedQuery<UserBranchOffice> typedQuery = em.createQuery(sql, UserBranchOffice.class);
        
        BranchOffice branchEntity = new BranchOffice();
        branchEntity.setIdBranchoffice(branch);
        
        typedQuery.setParameter("branch", branchEntity);
        return typedQuery.getResultList();
	}

	@Override
	public UserBranchOffice findByUserAndBranchOffice(Integer user, Integer branch) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("user", new User(user));
		params.put("branch", new BranchOffice(branch));

		return uniqueResult("UserBranchOffice.findByUserAndBranchOffice", params);
	}

    @Override
    public UserBranchOffice findByUserAndBranchOfficeWithOrders(User user, BranchOffice branchOffice) throws SimpleDAOException {
       String jql = "";
       jql += " SELECT ub FROM UserBranchOffice ub";
       jql += " LEFT JOIN FETCH ub.orders orders";
       jql += " WHERE ub.user = :user AND ub.branchOffice = :branchOffice";

		Map<String, Object> params = new HashMap<>();

		params.put("user", user);
		params.put("branchOffice", branchOffice);

		return singleResult(jql, params);
    }
	
	@Override
	public UserBranchOffice findBranchOfficeByOrder(String order) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("order", order);
		return uniqueResult("Order.findBranchOfficeByOrder", params);
	}




}