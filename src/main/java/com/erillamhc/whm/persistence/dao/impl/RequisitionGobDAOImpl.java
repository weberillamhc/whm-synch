package com.erillamhc.whm.persistence.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.erillamhc.whm.persistence.dao.RequisitionGobDAO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;


public class RequisitionGobDAOImpl extends SimpleDAOImpl<RequisitionGob, String> implements RequisitionGobDAO{


	@Override
	public RequisitionGob findByRequisitiongKey(String requisitionKey) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("requisitionKey", requisitionKey);
		return uniqueResult("RequisitionGob.findByRequisitionKey", params);
	}

	@Override
	public RequisitionGob findRequisitionGobAndRemmisions(String idRequisitionGob) throws SimpleDAOException {
		String jpl = "";
		jpl +=" SELECT requisitionGob FROM RequisitionGob requisitionGob";
		jpl +=" INNER JOIN FETCH requisitionGob.fiscalFund fiscalFund";
		jpl +=" INNER JOIN FETCH requisitionGob.bidding bidding";
		jpl +=" LEFT JOIN FETCH requisitionGob.remmisions remmisions";
		jpl +=" WHERE requisitionGob.idRequisitionGob = :idRequisitionGob";
		Map<String, Object> params = new HashMap<>();
		params.put("idRequisitionGob", idRequisitionGob);
		return singleResult(jpl, params);
	}

	@Override
	public RequisitionGob findByRequisitiongKeyAndIdNotEqual(String requisitionKey, String idRequisitionGob) throws SimpleDAOException {
		Map<String, Object> params = new HashMap<>();
		params.put("requisitionKey", requisitionKey);
		params.put("idRequisitionGob", idRequisitionGob);
		return uniqueResult("RequisitionGob.findByRequisitiongKeyAndIdNotEqual", params);
	}

	@Override
	public List<RequisitionGob> listRequisitionGobByBranchOffice(Integer idBranchoffice) throws SimpleDAOException {
		String jpl = "";
		jpl +=" SELECT DISTINCT requisitionGob FROM RequisitionGob requisitionGob";
		jpl +=" INNER JOIN FETCH requisitionGob.branchOffice branchOffice";
		jpl +=" INNER JOIN FETCH requisitionGob.bidding bidding";
		jpl +=" WHERE branchOffice.idBranchoffice = :idBranchoffice";
		EntityManager em = getEntityManager();
		TypedQuery<RequisitionGob> typedQuery = em.createQuery(jpl, RequisitionGob.class);
		typedQuery.setParameter("idBranchoffice", idBranchoffice);
		return typedQuery.getResultList();
	}

	@Override
	public List<RequisitionGob> findAllByBranchOffice(Integer branch) throws SimpleDAOException {
		String jpl = "SELECT r FROM RequisitionGob r where r.branchOffice = :branch";
		EntityManager em = getEntityManager();
		TypedQuery<RequisitionGob> typedQuery = em.createQuery(jpl, RequisitionGob.class);
		
		BranchOffice branchEntity = new BranchOffice();
		branchEntity.setIdBranchoffice(branch);
		
		typedQuery.setParameter("branch", branchEntity);
		
		return typedQuery.getResultList();
	}

    @Override
    public RequisitionGob findByIDWithBranchOffice(String idRequisitionGob) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT requisitionGob FROM RequisitionGob requisitionGob";
		jql += " INNER JOIN FETCH requisitionGob.branchOffice branchOffice";
		jql += " WHERE requisitionGob.idRequisitionGob = :idRequisitionGob";
		Map<String, Object> params = new HashMap();
		params.put("idRequisitionGob", idRequisitionGob);
		return singleResult(jql, params);
    }

    @Override
    public boolean saveWithScyn(RequisitionGob requisitionGob, Synch synch, Movement movement) throws SimpleDAOException {
		UserTransaction transaction = null;
		try {
			transaction = getUserTransaction();
			EntityManager em = getEntityManager();

			transaction.begin();

			em.persist(requisitionGob);

			movement.setIdEntity(requisitionGob.getIdRequisitionGob());
			em.persist(movement);

			synch.setIdEntity(requisitionGob.getIdRequisitionGob());
			em.persist(synch);

			transaction.commit();

			return true;
		} catch (Exception e) {
			simpleRollback(transaction);
			throw new SimpleDAOException(e);
		}
    }

    @Override
    public RequisitionGob findByIDFiscalFundAndBidding(String idRequisitionGob) throws SimpleDAOException {
		String jql = "";
		jql += " SELECT DISTINCT requisitionGob FROM RequisitionGob requisitionGob";
		jql += " INNER JOIN FETCH requisitionGob.branchOffice branchOffice";
		jql += " INNER JOIN FETCH requisitionGob.bidding bidding";
		jql += " INNER JOIN FETCH requisitionGob.fiscalFund fiscalFund";
		jql += " WHERE requisitionGob.idRequisitionGob = :idRequisitionGob";
		Map<String, Object> params = new HashMap();
		params.put("idRequisitionGob", idRequisitionGob);
		return singleResult(jql, params);
    }


}