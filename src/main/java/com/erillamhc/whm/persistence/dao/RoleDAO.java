package com.erillamhc.whm.persistence.dao;


import com.erillamhc.whm.persistence.entity.Role;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;

import java.util.List;

public interface RoleDAO extends SimpleDAO<Role, String> {

    List<Role> listAllRoles() throws SimpleDAOException;

    Role findByName(String name) throws SimpleDAOException;

    Role findByIDWithUsers(String idRole) throws SimpleDAOException;

    List<Role> listAllRolesWithPermissions() throws SimpleDAOException;

    Role findByIdWithPermissions(String id) throws SimpleDAOException;
}
