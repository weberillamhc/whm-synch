package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceptionDetailDTO extends SynchMinDTO {

    private String idShipment;
    private BranchOfficeDTO origin;
    private String driver;
    private String dateshipment;
    private String tag;

    private String idReception;
    private String idReceptionComment;
    private Integer status;
    private String dateReception;
    private String comment;

    private List<ReceptionOrderStockDTO> orderStocks;

    public String getIdShipment() {
        return idShipment;
    }

    public void setIdShipment(String idShipment) {
        this.idShipment = idShipment;
    }

    public BranchOfficeDTO getOrigin() {
        return origin;
    }

    public void setOrigin(BranchOfficeDTO origin) {
        this.origin = origin;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDateshipment() {
        return dateshipment;
    }

    public void setDateshipment(String dateshipment) {
        this.dateshipment = dateshipment;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getIdReception() {
        return idReception;
    }

    public void setIdReception(String idReception) {
        this.idReception = idReception;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }



    public String getDateReception() {
        return dateReception;
    }

    public void setDateReception(String dateReception) {
        this.dateReception = dateReception;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<ReceptionOrderStockDTO> getOrderStocks() {
        return orderStocks;
    }

    public void setOrderStocks(List<ReceptionOrderStockDTO> orderStocks) {
        this.orderStocks = orderStocks;
    }

    public String getIdReceptionComment() {
        return idReceptionComment;
    }

    public void setIdReceptionComment(String idReceptionComment) {
        this.idReceptionComment = idReceptionComment;
    }
}
