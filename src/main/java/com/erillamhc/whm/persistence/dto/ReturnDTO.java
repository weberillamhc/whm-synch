package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnDTO extends SynchMinDTO {


    private String idReturn;

    private String dateReturn;

    private String dateReceived;

    private Integer status;

    private BranchOfficeDTO origin;

    private BranchOfficeDTO destination;


    public String getIdReturn() {
        return idReturn;
    }

    public void setIdReturn(String idReturn) {
        this.idReturn = idReturn;
    }

    public String getDateReturn() {
        return dateReturn;
    }

    public void setDateReturn(String dateReturn) {
        this.dateReturn = dateReturn;
    }

    public String getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(String dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BranchOfficeDTO getOrigin() {
        return origin;
    }

    public void setOrigin(BranchOfficeDTO origin) {
        this.origin = origin;
    }

    public BranchOfficeDTO getDestination() {
        return destination;
    }

    public void setDestination(BranchOfficeDTO destination) {
        this.destination = destination;
    }
}
