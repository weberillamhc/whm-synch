package com.erillamhc.whm.persistence.dto.user;


import java.util.List;

import com.erillamhc.whm.persistence.dto.ProductsDTO;
import com.erillamhc.whm.persistence.mapper.facade.exception.FacadeException;

public interface ProductFacade {
	
	List<ProductsDTO> listAllProducts() throws FacadeException;
	
}
