package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequisitionsDTO {
	
	private String idRequisition;
	private Integer status;
	private Date date;
	private Integer destinationId;
	private Integer userId;
	
	
	public String getIdRequisition() {
		return idRequisition;
	}
	public void setIdRequisition(String idRequisition) {
		this.idRequisition = idRequisition;
	}
	public Integer getDestinationId() {
		return destinationId;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
