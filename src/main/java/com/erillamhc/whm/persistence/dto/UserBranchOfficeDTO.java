package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserBranchOfficeDTO {

	private Integer idUserBranchOffice;
	private Integer status;
	private Integer userId;
	private Integer branchOfficeId;
	private BranchOfficeDTO branchOffice;
	private UsersDTO user;

	public Integer getIdUserBranchOffice() {
		return idUserBranchOffice;
	}
	public void setIdUserBranchOffice(Integer idUserBranchOffice) {
		this.idUserBranchOffice = idUserBranchOffice;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public BranchOfficeDTO getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(BranchOfficeDTO branchOffice) {
		this.branchOffice = branchOffice;
	}
	public UsersDTO getUser() {
		return user;
	}
	public void setUser(UsersDTO user) {
		this.user = user;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getBranchOfficeId() {
		return branchOfficeId;
	}

	public void setBranchOfficeId(Integer branchOfficeId) {
		this.branchOfficeId = branchOfficeId;
	}
}
