package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BranchOfficeFull_MDTO {

    private Integer idBranchoffice;
    private String branchOfficeKey;
    private String warehouseId;
    private String name;
    private Integer type;
    private Integer parent;
    private Integer jurisdictionId;
    private String fullAddress;

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Integer getIdBranchoffice() {
        return idBranchoffice;
    }

    public void setIdBranchoffice(Integer idBranchoffice) {
        this.idBranchoffice = idBranchoffice;
    }

    public String getBranchOfficeKey() {
        return branchOfficeKey;
    }

    public void setBranchOfficeKey(String branchOfficeKey) {
        this.branchOfficeKey = branchOfficeKey;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }
}
