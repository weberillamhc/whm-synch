package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemmisionProductsRemmisionDTO {

    private String idRemmision;
    private String dateInput;
    private String status;
    private String remissionkey;
    private List<ProductRemmisionDTO> productsRemmision;
    private List<SuppliersDTO> suppliers;
    private String managerInput;

    public String getIdRemmision() {
        return idRemmision;
    }

    public void setIdRemmision(String idRemmision) {
        this.idRemmision = idRemmision;
    }

    public String getDateInput() {
        return dateInput;
    }

    public void setDateInput(String dateInput) {
        this.dateInput = dateInput;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemissionkey() {
        return remissionkey;
    }

    public void setRemissionkey(String remissionkey) {
        this.remissionkey = remissionkey;
    }

    public List<ProductRemmisionDTO> getProductsRemmision() {
        return productsRemmision;
    }

    public void setProductsRemmision(List<ProductRemmisionDTO> productsRemmision) {
        this.productsRemmision = productsRemmision;
    }

    public List<SuppliersDTO> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<SuppliersDTO> suppliers) {
        this.suppliers = suppliers;
    }

    public String getManagerInput() {
        return managerInput;
    }

    public void setManagerInput(String managerInput) {
        this.managerInput = managerInput;
    }
}
