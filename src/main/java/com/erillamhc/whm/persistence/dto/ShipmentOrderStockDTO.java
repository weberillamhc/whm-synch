package com.erillamhc.whm.persistence.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentOrderStockDTO extends ShipmentReceptionDTO {

    private  List<ReceptionOrderStockDTO> orderStocks;

    public List<ReceptionOrderStockDTO> getOrderStocks() {
        return orderStocks;
    }

    public void setOrderStocks(List<ReceptionOrderStockDTO> orderStocks) {
        this.orderStocks = orderStocks;
    }
}
