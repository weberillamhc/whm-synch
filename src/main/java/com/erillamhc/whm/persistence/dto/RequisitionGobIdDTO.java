package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequisitionGobIdDTO {
    private String idRequisitionGob;

    public String getIdRequisitionGob() {
        return idRequisitionGob;
    }

    public void setIdRequisitionGob(String idRequisitionGob) {
        this.idRequisitionGob = idRequisitionGob;
    }
}
