package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BranchOfficeDTO extends SynchMinDTO {

    private Integer idBranchoffice;
    private String name;
    private Integer type;
    private Integer jurisdictionId;
    private Integer parent;
    private String branchOfficeKey;
    private String warehouseId;
    private String fullAddress;
    private Integer idUser;
    private Integer status;

    private List<AddressBranchOfficeDTO> listAddress;


    public List<AddressBranchOfficeDTO> getListAddress() {
        return listAddress;
    }

    public void setListAddress(List<AddressBranchOfficeDTO> listAddress) {
        this.listAddress = listAddress;
    }

    public Integer getIdBranchoffice() {
        return idBranchoffice;
    }

    public void setIdBranchoffice(Integer idBranchoffice) {
        this.idBranchoffice = idBranchoffice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public String getBranchOfficeKey() {
        return branchOfficeKey;
    }

    public void setBranchOfficeKey(String branchOfficeKey) {
        this.branchOfficeKey = branchOfficeKey;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
