package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnStockDTO {

    private String idReturnStock;

    private Integer quantity;

    private ReturnDTO returns;

    private StockDTO stock;


    public String getIdReturnStock() {
        return idReturnStock;
    }

    public void setIdReturnStock(String idReturnStock) {
        this.idReturnStock = idReturnStock;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ReturnDTO getReturns() {
        return returns;
    }

    public void setReturns(ReturnDTO returns) {
        this.returns = returns;
    }

    public StockDTO getStock() {
        return stock;
    }

    public void setStock(StockDTO stock) {
        this.stock = stock;
    }


}
