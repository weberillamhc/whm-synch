package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IncidenceDTO extends SynchMinDTO {

    private String idIncidence;
    private String reason;
    private String dateIncidence;
    private String statusName;
    private Integer status;
    private String typeIncidenceName;
    private String supplierName;
    private boolean resolved;
    private String idStock;
    private String idStockSimple;
    private String idOrder;
    private String productName;
    private String productKey;
    private String productDescription;
    private String lot;
    private Integer idBranchOffice;
    private Integer idBranchOfficeSupplier;
    private Integer idSupplier;
    private Integer idProduct;
    private Integer typeIncidence;

    private BranchOfficeDTO branchOffice;

    public String getIdIncidence() {
        return idIncidence;
    }

    public void setIdIncidence(String idIncidence) {
        this.idIncidence = idIncidence;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDateIncidence() {
        return dateIncidence;
    }

    public void setDateIncidence(String dateIncidence) {
        this.dateIncidence = dateIncidence;
    }

    public BranchOfficeDTO getBranchOffice() {
        return branchOffice;
    }

    public void setBranchOffice(BranchOfficeDTO branchOffice) {
        this.branchOffice = branchOffice;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public String getTypeIncidenceName() {
        return typeIncidenceName;
    }

    public void setTypeIncidenceName(String typeIncidenceName) {
        this.typeIncidenceName = typeIncidenceName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getIdStock() {
        return idStock;
    }

    public void setIdStock(String idStock) {
        this.idStock = idStock;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Integer getIdBranchOffice() {
        return idBranchOffice;
    }

    public void setIdBranchOffice(Integer idBranchOffice) {
        this.idBranchOffice = idBranchOffice;
    }

    public Integer getIdBranchOfficeSupplier() {
        return idBranchOfficeSupplier;
    }

    public void setIdBranchOfficeSupplier(Integer idBranchOfficeSupplier) {
        this.idBranchOfficeSupplier = idBranchOfficeSupplier;
    }

    public Integer getIdSupplier() {
        return idSupplier;
    }

    public void setIdSupplier(Integer idSupplier) {
        this.idSupplier = idSupplier;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getTypeIncidence() {
        return typeIncidence;
    }

    public void setTypeIncidence(Integer typeIncidence) {
        this.typeIncidence = typeIncidence;
    }

    public String getIdStockSimple() {
        return idStockSimple;
    }

    public void setIdStockSimple(String idStockSimple) {
        this.idStockSimple = idStockSimple;
    }
}
