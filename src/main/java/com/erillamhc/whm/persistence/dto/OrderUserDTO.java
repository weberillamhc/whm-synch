package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author ALAN-SAUCEDO
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderUserDTO extends SynchMinDTO {

    private Integer idOrderUser;
    private String orderId;
    private Integer userId;
    private Integer status;
    private String destination;
    private String datemovement;
    private String clues;
    private String picking;
    private String validator;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getClues() {
        return clues;
    }

    public void setClues(String clues) {
        this.clues = clues;
    }

    public String getPicking() {
        return picking;
    }

    public void setPicking(String picking) {
        this.picking = picking;
    }

    public String getValidator() {
        return validator;
    }

    public void setValidator(String validator) {
        this.validator = validator;
    }

    public Integer getIdOrderUser() {
        return idOrderUser;
    }

    public void setIdOrderUser(Integer idOrderUser) {
        this.idOrderUser = idOrderUser;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDatemovement() {
        return datemovement;
    }

    public void setDatemovement(String datemovement) {
        this.datemovement = datemovement;
    }
}
