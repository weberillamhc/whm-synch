package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductRemmisionDTO extends SynchMinDTO {

    private String idProductRemmison;
    private String lot;
    private Integer quantity;
    private String expirationDate;
    private double unitprice;
    private String productName;
    private String productDescription;
    private String productKey;
    private String fiscalFund;
    private String remmisionId;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    public String getIdProductRemmison() {
        return idProductRemmison;
    }

    public void setIdProductRemmison(String idProductRemmison) {
        this.idProductRemmison = idProductRemmison;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getFiscalFund() {
        return fiscalFund;
    }

    public void setFiscalFund(String fiscalFund) {
        this.fiscalFund = fiscalFund;
    }

    public String getRemmisionId() {
        return remmisionId;
    }

    public void setRemmisionId(String remmisionId) {
        this.remmisionId = remmisionId;
    }
}
