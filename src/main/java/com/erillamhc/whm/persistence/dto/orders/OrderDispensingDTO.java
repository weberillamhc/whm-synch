package com.erillamhc.whm.persistence.dto.orders;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDispensingDTO {
    private String idPrescription;
    private String user;
    private String prescriptionKey;
    private String medicalRecord;
    private String doctor;
    private String datePrescription;
    private String Status;

    public String getIdPrescription() {
        return idPrescription;
    }

    public void setIdPrescription(String idPrescription) {
        this.idPrescription = idPrescription;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPrescriptionKey() {
        return prescriptionKey;
    }

    public void setPrescriptionKey(String prescriptionKey) {
        this.prescriptionKey = prescriptionKey;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getDatePrescription() {
        return datePrescription;
    }

    public void setDatePrescription(String datePrescription) {
        this.datePrescription = datePrescription;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
