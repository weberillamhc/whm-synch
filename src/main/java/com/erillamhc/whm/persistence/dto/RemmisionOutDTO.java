package com.erillamhc.whm.persistence.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemmisionOutDTO {

	private String idRemmision;
	private String idRequisitionGob;
	private String dateInput;
	private String remissionkey;
	private String requisitionDate;
	private String fiscalFundKey;
	private String requisitionKey;
	private String status;
	private List<SuppliersDTO> suppliers;

	public String getIdRemmision() {
		return idRemmision;
	}
	public void setIdRemmision(String idRemmision) {
		this.idRemmision = idRemmision;
	}
	public String getDateInput() {
		return dateInput;
	}
	public void setDateInput(String dateInput) {
		this.dateInput = dateInput;
	}
	public String getRemissionkey() {
		return remissionkey;
	}
	public void setRemissionkey(String remissionkey) {
		this.remissionkey = remissionkey;
	}
	public String getRequisitionDate() {
		return requisitionDate;
	}
	public void setRequisitionDate(String requisitionDate) {
		this.requisitionDate = requisitionDate;
	}
	public String getFiscalFundKey() {
		return fiscalFundKey;
	}
	public void setFiscalFundKey(String fiscalFundKey) {
		this.fiscalFundKey = fiscalFundKey;
	}
	public String getRequisitionKey() {
		return requisitionKey;
	}
	public void setRequisitionKey(String requisitionKey) {
		this.requisitionKey = requisitionKey;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdRequisitionGob() {
		return idRequisitionGob;
	}

	public void setIdRequisitionGob(String idRequisitionGob) {
		this.idRequisitionGob = idRequisitionGob;
	}

	public List<SuppliersDTO> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(List<SuppliersDTO> suppliers) {
		this.suppliers = suppliers;
	}
}
