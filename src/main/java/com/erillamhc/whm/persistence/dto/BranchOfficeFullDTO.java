package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BranchOfficeFullDTO {

    private Integer idBranchoffice;
    private String name;
    private String type;
    private JurisdictionsDTO jurisdiction;
    private String parentName;
    private Integer idParent;
    private String branchOfficeKey;
    private String warehouseId;
    private String status;
    private String fullAddress;

    public Integer getIdBranchoffice() {
        return idBranchoffice;
    }

    public void setIdBranchoffice(Integer idBranchoffice) {
        this.idBranchoffice = idBranchoffice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public JurisdictionsDTO getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(JurisdictionsDTO jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public String getBranchOfficeKey() {
        return branchOfficeKey;
    }

    public void setBranchOfficeKey(String branchOfficeKey) {
        this.branchOfficeKey = branchOfficeKey;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }
}
