package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemmisionsDTO extends SynchMinDTO {
	
	private String idRemmision;
	private Date dateinput;
	private String dateInput;
	private Integer idUser;
	private Integer status;
	private String remissionkey;
	private RequisitionGobDTO requisitionGob;
	private String requisitionGobId;
	
	public String getIdRemmision() {
		return idRemmision;
	}
	public void setIdRemmision(String idRemmision) {
		this.idRemmision = idRemmision;
	}
	public Date getDateinput() {
		return dateinput;
	}
	public void setDateinput(Date dateinput) {
		this.dateinput = dateinput;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemissionkey() {
		return remissionkey;
	}
	public void setRemissionkey(String remissionkey) {
		this.remissionkey = remissionkey;
	}
	public RequisitionGobDTO getRequisitionGob() {
		return requisitionGob;
	}
	public void setRequisitionGob(RequisitionGobDTO requisitionGob) {
		this.requisitionGob = requisitionGob;
	}
	public String getRequisitionGobId() {
		return requisitionGobId;
	}
	public void setRequisitionGobId(String requisitionGobId) {
		this.requisitionGobId = requisitionGobId;
	}


	public String getDateInput() {
		return dateInput;
	}

	public void setDateInput(String dateInput) {
		this.dateInput = dateInput;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
}
