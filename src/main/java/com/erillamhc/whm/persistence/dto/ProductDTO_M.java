package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO_M extends ProductsDTO{

	private Integer userID;

	private List<ProductCategoriesDTO> productsCategories;

	public List<ProductCategoriesDTO> getProductsCategories() {
		return productsCategories;
	}

	public void setProductsCategories(List<ProductCategoriesDTO> productsCategories) {
		this.productsCategories = productsCategories;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}
}
