package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicalPrescriptionDTO {

	private String idPrescription;
	private String prescriptionKey;
	private String medicalRecord;
	private String doctorId;
	private Integer originId;
	private Integer destinationId;
	private String curp;
	
	private String idStock;
	private Integer quantity;
	private Integer prescription;
	private Integer userId;

	private String orderId;

	
	private List<MedicalPrescriptionDTO> listStock;
	
	public String getPrescriptionKey() {
		return prescriptionKey;
	}
	public void setPrescriptionKey(String prescriptionKey) {
		this.prescriptionKey = prescriptionKey;
	}
	public String getMedicalRecord() {
		return medicalRecord;
	}
	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}
	public String getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}
	public Integer getOriginId() {
		return originId;
	}
	public void setOriginId(Integer originId) {
		this.originId = originId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getPrescription() {
		return prescription;
	}
	public void setPrescription(Integer prescription) {
		this.prescription = prescription;
	}
	public String getIdStock() {
		return idStock;
	}
	public void setIdStock(String idStock) {
		this.idStock = idStock;
	}
	public List<MedicalPrescriptionDTO> getListStock() {
		return listStock;
	}
	public void setListStock(List<MedicalPrescriptionDTO> listStock) {
		this.listStock = listStock;
	}
	public Integer getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getIdPrescription() {
		return idPrescription;
	}

	public void setIdPrescription(String idPrescription) {
		this.idPrescription = idPrescription;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}
}
