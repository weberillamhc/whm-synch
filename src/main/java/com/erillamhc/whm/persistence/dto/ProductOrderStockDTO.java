package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductOrderStockDTO {

    private String productKey;
    private String productName;
    private String productDescripcion;
    private List<OrdersStockDTO> orderStocks;

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescripcion() {
        return productDescripcion;
    }

    public void setProductDescripcion(String productDescripcion) {
        this.productDescripcion = productDescripcion;
    }

    public List<OrdersStockDTO> getOrderStocks() {
        return orderStocks;
    }

    public void setOrderStocks(List<OrdersStockDTO> orderStocks) {
        this.orderStocks = orderStocks;
    }
}
