package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JurisdictionBranchOfficesDTO extends JurisdictionsDTO{

    List<BranchOfficeDTO> branchOffices;

    public List<BranchOfficeDTO> getBranchOffices() {
        return branchOffices;
    }

    public void setBranchOffices(List<BranchOfficeDTO> branchOffices) {
        this.branchOffices = branchOffices;
    }
}
