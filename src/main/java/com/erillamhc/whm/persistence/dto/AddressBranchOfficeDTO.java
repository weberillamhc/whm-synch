package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressBranchOfficeDTO {

    private Integer idAddress;
    private String fulladdress;
    private Integer branchofficeId;
    
    public Integer getIdAddress() {
		return idAddress;
	}

	public void setIdAddress(Integer idAddress) {
		this.idAddress = idAddress;
	}

    public String getFulladdress() {
        return fulladdress;
    }

    public void setFulladdress(String fulladdress) {
        this.fulladdress = fulladdress;
    }

    public Integer getBranchofficeId() {
		return branchofficeId;
	}

	public void setBranchofficeId(Integer branchofficeId) {
		this.branchofficeId = branchofficeId;
	}
    
    
    
}
