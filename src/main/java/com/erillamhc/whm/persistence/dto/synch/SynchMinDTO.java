package com.erillamhc.whm.persistence.dto.synch;


public class SynchMinDTO {

    private String idSynch;
    private String entitySynch;
    private String dataEntitySynch;
    private Integer statusSynch;
    private String registrationDateSynch;
    private Integer typeSynch;

    public String getIdSynch() {
        return idSynch;
    }

    public void setIdSynch(String idSynch) {
        this.idSynch = idSynch;
    }

    public String getEntitySynch() {
        return entitySynch;
    }

    public void setEntitySynch(String entitySynch) {
        this.entitySynch = entitySynch;
    }

    public Integer getStatusSynch() {
        return statusSynch;
    }

    public void setStatusSynch(Integer statusSynch) {
        this.statusSynch = statusSynch;
    }

    public String getRegistrationDateSynch() {
        return registrationDateSynch;
    }

    public void setRegistrationDateSynch(String registrationDateSynch) {
        this.registrationDateSynch = registrationDateSynch;
    }

    public Integer getTypeSynch() {
        return typeSynch;
    }

    public void setTypeSynch(Integer typeSynch) {
        this.typeSynch = typeSynch;
    }

    public String getDataEntitySynch() {
        return dataEntitySynch;
    }

    public void setDataEntitySynch(String dataEntitySynch) {
        this.dataEntitySynch = dataEntitySynch;
    }
}
