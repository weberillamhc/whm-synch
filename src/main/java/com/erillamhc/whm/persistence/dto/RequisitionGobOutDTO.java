package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequisitionGobOutDTO{


	private String idRequisitionGob;
	private String requisitionKey;
	private String status;
	private String dateRequisition;

	/*FiscalFund*/
	private String fiscalFundName;
	private String fiscalFundKey;

	/*Bidding*/
	private String biddingName;
	private String biddingKey;
	

	public String getIdRequisitionGob() {
		return idRequisitionGob;
	}

	public void setIdRequisitionGob(String idRequisitionGob) {
		this.idRequisitionGob = idRequisitionGob;
	}

	public String getRequisitionKey() {
		return requisitionKey;
	}

	public void setRequisitionKey(String requisitionKey) {
		this.requisitionKey = requisitionKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateRequisition() {
		return dateRequisition;
	}

	public void setDateRequisition(String dateRequisition) {
		this.dateRequisition = dateRequisition;
	}

	public String getFiscalFundName() {
		return fiscalFundName;
	}

	public void setFiscalFundName(String fiscalFundName) {
		this.fiscalFundName = fiscalFundName;
	}

	public String getFiscalFundKey() {
		return fiscalFundKey;
	}

	public void setFiscalFundKey(String fiscalFundKey) {
		this.fiscalFundKey = fiscalFundKey;
	}

	public String getBiddingName() {
		return biddingName;
	}

	public void setBiddingName(String biddingName) {
		this.biddingName = biddingName;
	}

	public String getBiddingKey() {
		return biddingKey;
	}

	public void setBiddingKey(String biddingKey) {
		this.biddingKey = biddingKey;
	}
	
	
}
