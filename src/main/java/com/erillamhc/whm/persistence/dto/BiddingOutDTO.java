package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BiddingOutDTO extends BiddingsDTO{

    
    private List<SuppliersDTO> suppliers;

    public List<SuppliersDTO> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<SuppliersDTO> suppliers) {
        this.suppliers = suppliers;
    }
}
