package com.erillamhc.whm.persistence.dto.user;

public class OrderShipmentDTO {
    private String orderId;
    private String shipmentsId;
    private String destination;
    private String date;
    private String status;
    private String orderUser;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getShipmentsId() {
        return shipmentsId;
    }

    public void setShipmentsId(String shipmentsId) {
        this.shipmentsId = shipmentsId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderUser() {
        return orderUser;
    }

    public void setOrderUser(String orderUser) {
        this.orderUser = orderUser;
    }
}
