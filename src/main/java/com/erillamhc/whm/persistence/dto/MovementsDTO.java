package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovementsDTO {

    private Integer idmovement;
    private Integer userId;
    private Integer movementtype;
    private String originmovement;
    private Date datemovement;
    private String data;
    private String entity;
    private Integer idBranchOffice;
    private String idEntity;
    private String idEntitySecondary;

    public MovementsDTO(){
    }

    public MovementsDTO(Integer movementtype, Integer idBranchOffice, Integer idUser) {
        this.movementtype = movementtype;
        this.idBranchOffice = idBranchOffice;
        this.userId = idUser;
    }

    public MovementsDTO(Integer movementtype, Integer idBranchOffice, Integer idUser, String idEntity) {
        this.movementtype = movementtype;
        this.idBranchOffice = idBranchOffice;
        this.userId = idUser;
        this.idEntity = idEntity;
    }
    public MovementsDTO(Integer movementtype, Integer idBranchOffice, Integer idUser, String idEntity, String idEntitySecondary) {
        this.movementtype = movementtype;
        this.idBranchOffice = idBranchOffice;
        this.userId = idUser;
        this.idEntity = idEntity;
        this.idEntitySecondary = idEntitySecondary;
    }
    public Integer getIdmovement() {
        return idmovement;
    }

    public void setIdmovement(Integer idmovement) {
        this.idmovement = idmovement;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMovementtype() {
        return movementtype;
    }

    public void setMovementtype(Integer movementtype) {
        this.movementtype = movementtype;
    }

    public String getOriginmovement() {
        return originmovement;
    }

    public void setOriginmovement(String originmovement) {
        this.originmovement = originmovement;
    }

    public Date getDatemovement() {
        return datemovement;
    }

    public void setDatemovement(Date datemovement) {
        this.datemovement = datemovement;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }


    public Integer getIdBranchOffice() {
        return idBranchOffice;
    }

    public void setIdBranchOffice(Integer idBranchOffice) {
        this.idBranchOffice = idBranchOffice;
    }

    public String getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(String idEntity) {
        this.idEntity = idEntity;
    }

    public String getIdEntitySecondary() {
        return idEntitySecondary;
    }

    public void setIdEntitySecondary(String idEntitySecondary) {
        this.idEntitySecondary = idEntitySecondary;
    }
}
