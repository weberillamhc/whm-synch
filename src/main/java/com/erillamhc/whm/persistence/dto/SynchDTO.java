package com.erillamhc.whm.persistence.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SynchDTO {

    private String idSynch;

    private String entity;

    private Integer status;

    private Integer typeSynch;

    private Date dateSynch;

    private Date registrationDate;

    private String registrationDateSynch;

    private Integer idBranchOffice;

    private Integer idUser;

    private String idEntity;

    private String idEntitySecondary;

    private String dataEntity;

    private String meesage;

    public SynchDTO() {
    }

    public SynchDTO(Integer typeSynch, Integer idBranchOffice, Integer idUser) {
        this.typeSynch = typeSynch;
        this.idBranchOffice = idBranchOffice;
        this.idUser = idUser;
    }

    public SynchDTO(Integer typeSynch, Integer idBranchOffice, Integer idUser, String idEntity) {
        this.typeSynch = typeSynch;
        this.idBranchOffice = idBranchOffice;
        this.idUser = idUser;
        this.idEntity = idEntity;
    }
    public SynchDTO(Integer typeSynch, Integer idBranchOffice, Integer idUser, String idEntity, String idEntitySecondary) {
        this.typeSynch = typeSynch;
        this.idBranchOffice = idBranchOffice;
        this.idUser = idUser;
        this.idEntity = idEntity;
        this.idEntitySecondary = idEntitySecondary;
    }

    public String getIdSynch() {
        return idSynch;
    }

    public void setIdSynch(String idSynch) {
        this.idSynch = idSynch;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTypeSynch() {
        return typeSynch;
    }

    public void setTypeSynch(Integer typeSynch) {
        this.typeSynch = typeSynch;
    }

    public Date getDateSynch() {
        return dateSynch;
    }

    public void setDateSynch(Date dateSynch) {
        this.dateSynch = dateSynch;
    }

    public Integer getIdBranchOffice() {
        return idBranchOffice;
    }

    public void setIdBranchOffice(Integer idBranchOffice) {
        this.idBranchOffice = idBranchOffice;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(String idEntity) {
        this.idEntity = idEntity;
    }

    public String getIdEntitySecondary() {
        return idEntitySecondary;
    }

    public void setIdEntitySecondary(String idEntitySecondary) {
        this.idEntitySecondary = idEntitySecondary;
    }

    public String getDataEntity() {
        return dataEntity;
    }

    public void setDataEntity(String dataEntity) {
        this.dataEntity = dataEntity;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getMeesage() {
        return meesage;
    }

    public void setMeesage(String meesage) {
        this.meesage = meesage;
    }

    public String getRegistrationDateSynch() {
        return registrationDateSynch;
    }

    public void setRegistrationDateSynch(String registrationDateSynch) {
        this.registrationDateSynch = registrationDateSynch;
    }
}
