package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BiddingIdDTO {
    private String idBidding;

    public String getIdBidding() {
        return idBidding;
    }

    public void setIdBidding(String idBidding) {
        this.idBidding = idBidding;
    }
}
