package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO extends  ProductsDTO{

    private List<CategoriesDTO>  productsCategories;

    public List<CategoriesDTO> getProductsCategories() {
        return productsCategories;
    }

    public void setProductsCategories(List<CategoriesDTO> productsCategories) {
        this.productsCategories = productsCategories;
    }
}
