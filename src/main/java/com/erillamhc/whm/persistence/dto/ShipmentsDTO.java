package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class ShipmentsDTO extends SynchMinDTO {
	
	private String idShipment;
	private String driver;
	private String tag;
	private String licenseplate;
	private Integer number;
	private Integer status;

	private String dateshipment;
	private Integer destinationId;
	private String destination;
	private String destinationKey;
	private String orderId;

	private Integer idUser;
	
	public String getIdShipment() {
		return idShipment;
	}
	public void setIdShipment(String idShipment) {
		this.idShipment = idShipment;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDateshipment() {
		return dateshipment;
	}

	public void setDateshipment(String dateshipment) {
		this.dateshipment = dateshipment;
	}

	public Integer getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationKey() {
		return destinationKey;
	}

	public void setDestinationKey(String destinationKey) {
		this.destinationKey = destinationKey;
	}

	public String getLicenseplate() {
		return licenseplate;
	}

	public void setLicenseplate(String licenseplate) {
		this.licenseplate = licenseplate;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "ShipmentsDTO [idShipment=" + idShipment + ", driver=" + driver + ", tag=" + tag + ", status=" + status
				+ ", dateshipment=" + dateshipment + ", destinationId=" + destinationId + ", orderId=" + orderId + "]";
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
}
