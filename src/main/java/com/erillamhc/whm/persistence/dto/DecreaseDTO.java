package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DecreaseDTO extends SynchMinDTO {

    private String idDecrease;
    private Integer status;
    private String idOrder;
    private Integer idUser;
    private Integer idBranchoffice;
    private String date;

    public String getIdDecrease() {
        return idDecrease;
    }

    public void setIdDecrease(String idDecrease) {
        this.idDecrease = idDecrease;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdBranchoffice() {
        return idBranchoffice;
    }

    public void setIdBranchoffice(Integer idBranchoffice) {
        this.idBranchoffice = idBranchoffice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
