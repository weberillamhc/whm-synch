package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockSimpleDTO extends SynchMinDTO {


    private String idStock;
    private Integer quantity;
    private String locationkey;
    private Integer status;
    private Integer branchOfficeId;
    private String remmisionId;
    private String parentId;
    private Integer userId;
    private StockDTO parent;
    private String productRemmisionId;
    private String dateIn;
    private Integer productId;

    public String getIdStock() {
        return idStock;
    }

    public void setIdStock(String idStock) {
        this.idStock = idStock;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getLocationkey() {
        return locationkey;
    }

    public void setLocationkey(String locationkey) {
        this.locationkey = locationkey;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBranchOfficeId() {
        return branchOfficeId;
    }

    public void setBranchOfficeId(Integer branchOfficeId) {
        this.branchOfficeId = branchOfficeId;
    }

    public String getRemmisionId() {
        return remmisionId;
    }

    public void setRemmisionId(String remmisionId) {
        this.remmisionId = remmisionId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public StockDTO getParent() {
        return parent;
    }

    public void setParent(StockDTO parent) {
        this.parent = parent;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductRemmisionId() {
        return productRemmisionId;
    }

    public void setProductRemmisionId(String productRemmisionId) {
        this.productRemmisionId = productRemmisionId;
    }

    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }
}
