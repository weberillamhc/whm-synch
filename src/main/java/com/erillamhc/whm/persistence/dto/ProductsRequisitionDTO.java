package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductsRequisitionDTO {
	
	private String idProductRequisition;
	private Integer quantity;
	private Integer productId;
	private String requisitionsId;
	
	
	public String getIdProductRequisition() {
		return idProductRequisition;
	}
	public void setIdProductRequisition(String idProductRequisition) {
		this.idProductRequisition = idProductRequisition;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getRequisitionsId() {
		return requisitionsId;
	}
	public void setRequisitionsId(String requisitionsId) {
		this.requisitionsId = requisitionsId;
	}
	
	


}
