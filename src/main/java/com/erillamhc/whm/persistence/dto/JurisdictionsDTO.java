package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JurisdictionsDTO extends SynchMinDTO {

    private Integer idJurisdiction;
    private String jurisdictionname;
    private Integer userID;

    public Integer getIdJurisdiction() {
		return idJurisdiction;
	}

	public void setIdJurisdiction(Integer idJurisdiction) {
		this.idJurisdiction = idJurisdiction;
	}

    public String getJurisdictionname() {
        return jurisdictionname;
    }

    public void setJurisdictionname(String jurisdictionname) {
        this.jurisdictionname = jurisdictionname;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }
}
