package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockDTO_M extends StockDTO{
	
	private RemmisionsDTO remmision;
	private BranchOfficeDTO branchOffice;
	
	public RemmisionsDTO getRemmision() {
		return remmision;
	}
	public void setRemmision(RemmisionsDTO remmision) {
		this.remmision = remmision;
	}
	public BranchOfficeDTO getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(BranchOfficeDTO branchOffice) {
		this.branchOffice = branchOffice;
	}
	
	
	
	
}
