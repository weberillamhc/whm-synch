package com.erillamhc.whm.persistence.dto;


import com.erillamhc.whm.persistence.annotation.Validator;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDTO {

	@Validator(required = false)
	private String message;
	private boolean success;
	private Object object;
	@Validator(optional = false)
	private String orderId;
	
	public ResponseDTO() {
	}
	
	public ResponseDTO(String message, boolean success) {
		super();
		this.message = message;
		this.success = success;
	}
	public String getMessage() {
		if(message == null) {
			setMessage("");
		}
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
