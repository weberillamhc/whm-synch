package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockDTO extends FiscalFundDTO{
	
	private String idStock;
	private Integer quantity;
	private String locationkey;
	private Integer status;
	private Integer branchOfficeId;
	private String remmisionId;
	private String parentId;
	private String dateIn;
	private Integer userId;
	private StockDTO parent;
	private String productRemmisionId;
	private Integer productId;
	private BranchOfficeDTO branchOffice;
	
    private List<StockDTO> listStock;
    
	public List<StockDTO> getListStock() {
		return listStock;
	}
	
	public void setListStock(List<StockDTO> listStock) {
		this.listStock = listStock;
	}
	
	public String getIdStock() {
		return idStock;
	}
	public void setIdStock(String stockId) {
		this.idStock = stockId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getLocationkey() {
		return locationkey;
	}
	public void setLocationkey(String locationkey) {
		this.locationkey = locationkey;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setBranchOfficeId(Integer branchOfficeId) {
		this.branchOfficeId = branchOfficeId;
	}
	public String getRemmisionId() {
		return remmisionId;
	}
	public void setRemmisionId(String remmisionId) {
		this.remmisionId = remmisionId;
	}
	public StockDTO getParent() {
		return parent;
	}
	public void setParent(StockDTO parent) {
		this.parent = parent;
	}
	public Integer getBranchOfficeId() {
		return branchOfficeId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public BranchOfficeDTO getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOfficeDTO branchOffice) {
		this.branchOffice = branchOffice;
	}


	public String getDateIn() {
		return dateIn;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	public String getProductRemmisionId() {
		return productRemmisionId;
	}

	public void setProductRemmisionId(String productRemmisionId) {
		this.productRemmisionId = productRemmisionId;
	}
}
