package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RolePermissionsDTO extends  RolesDTO{
    List<PermissionsDTO>  permissions;
    private Integer userID;

    public List<PermissionsDTO> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionsDTO> permissions) {
        this.permissions = permissions;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }
}
