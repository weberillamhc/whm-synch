package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddSuppliersToBiddingDTO {
	
	private String idBidding;
	private List<SuppliersDTO> suppliers;
	
	public String getIdBidding() {
		return idBidding;
	}
	public void setIdBidding(String idBidding) {
		this.idBidding = idBidding;
	}
	public List<SuppliersDTO> getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(List<SuppliersDTO> suppliers) {
		this.suppliers = suppliers;
	}
	
	

}
