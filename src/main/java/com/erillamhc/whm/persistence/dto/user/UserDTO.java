package com.erillamhc.whm.persistence.dto.user;

public class UserDTO {
	
	private int id_user;
	private Integer idUser;


	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}


	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
}
