package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FiscalFundDTO extends SynchMinDTO {

	private Integer idFiscalfund;
    private String key;
    private String name;
    private Integer userID;

    public FiscalFundDTO() {
	}
    public FiscalFundDTO(Integer idFiscalfund, String key, String name) {
		super();
		this.idFiscalfund = idFiscalfund;
		this.key = key;
		this.name = name;
	}

	public Integer getIdFiscalfund() {
		return idFiscalfund;
	}

	public void setIdFiscalfund(Integer idFiscalfund) {
		this.idFiscalfund = idFiscalfund;
	}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }
}
