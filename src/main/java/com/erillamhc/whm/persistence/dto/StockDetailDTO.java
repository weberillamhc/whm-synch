package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockDetailDTO {

    private String idStock;
    private String dateIn;
    private String remmisionKey;
    private String dateInputRemmision;

    public String getIdStock() {
        return idStock;
    }

    public void setIdStock(String idStock) {
        this.idStock = idStock;
    }

    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public String getRemmisionKey() {
        return remmisionKey;
    }

    public void setRemmisionKey(String remmisionKey) {
        this.remmisionKey = remmisionKey;
    }

    public String getDateInputRemmision() {
        return dateInputRemmision;
    }

    public void setDateInputRemmision(String dateInputRemmision) {
        this.dateInputRemmision = dateInputRemmision;
    }
}
