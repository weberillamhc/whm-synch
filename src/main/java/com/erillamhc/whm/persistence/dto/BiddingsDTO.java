package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * @author FERNANDO-FH
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BiddingsDTO extends SynchMinDTO {
    private String idBidding;
    private String name;
    private String biddingKey;
    private String registrationDate;
    private List<SuppliersDTO> suppliers;
    private BranchOfficeIdDTO branchOffice;
    private Integer idUser;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdBidding() {
        return idBidding;
    }

    public void setIdBidding(String idBidding) {
        this.idBidding = idBidding;
    }

    public String getBiddingKey() {
        return biddingKey;
    }

    public void setBiddingKey(String biddingKey) {
        this.biddingKey = biddingKey;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<SuppliersDTO> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<SuppliersDTO> suppliers) {
        this.suppliers = suppliers;
    }

    public BranchOfficeIdDTO getBranchOffice() {
        return branchOffice;
    }

    public void setBranchOffice(BranchOfficeIdDTO branchOffice) {
        this.branchOffice = branchOffice;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
}
