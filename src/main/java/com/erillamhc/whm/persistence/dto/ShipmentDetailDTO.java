package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentDetailDTO {
	
	private String orderId;
	private String fullAddressOrigin;
	private String outDate;
	private String shippingDate;
	private String fullAddressDestination;
	private Integer status;
	private List<ProductoOrderDTO> productList;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getFullAddressOrigin() {
		return fullAddressOrigin;
	}
	public void setFullAddressOrigin(String fullAddressOrigin) {
		this.fullAddressOrigin = fullAddressOrigin;
	}
	public String getOutDate() {
		return outDate;
	}
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	public String getShippingDate() {
		return shippingDate;
	}
	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}
	public String getFullAddressDestination() {
		return fullAddressDestination;
	}
	public void setFullAddressDestination(String fullAddressDestination) {
		this.fullAddressDestination = fullAddressDestination;
	}

	public List<ProductoOrderDTO> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductoOrderDTO> productList) {
		this.productList = productList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
