package com.erillamhc.whm.persistence.dto.synch;

import com.erillamhc.whm.persistence.dto.SynchDTO;

import java.util.ArrayList;
import java.util.List;

public class SynchFullDTO {

    private List<SynchDTO> errores;
    private List<String> idSsynchronized ;
    private SynchActionDTO records;
    private SynchActionDTO updates;
    private SynchActionDTO deletes;
    private SynchActionDTO updatesStatus;


    public SynchFullDTO() {
        this.records = new SynchActionDTO();
        this.updates = new SynchActionDTO();
        this.deletes = new SynchActionDTO();
        this.updatesStatus = new SynchActionDTO();
        this.errores = new ArrayList<>();
        this.idSsynchronized = new ArrayList<>();
    }

    public SynchActionDTO getRecords() {
        return records;
    }

    public void setRecords(SynchActionDTO records) {
        this.records = records;
    }

    public SynchActionDTO getUpdates() {
        return updates;
    }

    public void setUpdates(SynchActionDTO updates) {
        this.updates = updates;
    }

    public SynchActionDTO getDeletes() {
        return deletes;
    }

    public void setDeletes(SynchActionDTO deletes) {
        this.deletes = deletes;
    }

    public SynchActionDTO getUpdatesStatus() {
        return updatesStatus;
    }

    public void setUpdatesStatus(SynchActionDTO updatesStatus) {
        this.updatesStatus = updatesStatus;
    }

    public List<SynchDTO> getErrores() {
        return errores;
    }

    public void setErrores(List<SynchDTO> errores) {
        this.errores = errores;
    }

    public List<String> getIdSsynchronized() {
        return idSsynchronized;
    }

    public void setIdSsynchronized(List<String> idSsynchronized) {
        this.idSsynchronized = idSsynchronized;
    }
}
