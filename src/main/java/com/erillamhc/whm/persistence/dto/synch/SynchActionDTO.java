package com.erillamhc.whm.persistence.dto.synch;

import com.erillamhc.whm.persistence.dto.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SynchActionDTO {

    private List<BiddingsDTO> biddings;
    private List<DecreaseDTO> decreases;
    private List<DoctorDTO> doctors;
    private List<IncidenceDTO> incidences;
    private List<OrdersDTO> orders;
    private List<OrdersStockDTO> ordersStock;
    private List<OrderUserDTO> orderUsers;
    private List<ProductDTO> products;
    private List<ProductRemmisionDTO> productRemmisions;
    private List<ReceptionDetailDTO> receptions;
    private List<ReceptionDetailDTO> receptionsComments;
    private List<ReceptionOrderStockDTO> receptionStocks;
    private List<RemmisionsDTO> remmisions;
    private List<RequisitionGobDTO> requisitionsGob;
    private List<ShipmentsDTO> shipments;
    private List<StockSimpleDTO> stocks;
    private List<StockHistoryDTO> stocksHistory;
    private List<SuppliersDTO> suppliers;
    private List<BranchOfficeDTO> branchOffices;
    private List<UsersDTO> users;
    private List<CategoriesDTO> categories;
    private List<FiscalFundDTO> fiscalFunds;
    private List<JurisdictionsDTO> jurisdictions;
    private List<RolesDTO> roles;

    public SynchActionDTO() {
        this.biddings = new ArrayList<>();
        this.decreases = new ArrayList<>();
        this.doctors = new ArrayList<>();
        this.incidences = new ArrayList<>();
        this.orders = new ArrayList<>();
        this.ordersStock = new ArrayList<>();
        this.orderUsers = new ArrayList<>();
        this.products = new ArrayList<>();
        this.productRemmisions = new ArrayList<>();
        this.receptions = new ArrayList<>();
        this.receptionsComments = new ArrayList<>();
        this.receptionStocks = new ArrayList<>();
        this.remmisions = new ArrayList<>();
        this.requisitionsGob = new ArrayList<>();
        this.shipments = new ArrayList<>();
        this.stocks = new ArrayList<>();
        this.stocksHistory = new ArrayList<>();
        this.suppliers = new ArrayList<>();
        this.branchOffices = new ArrayList<>();
        this.users = new ArrayList<>();
        this.categories = new ArrayList<>();
        this.fiscalFunds = new ArrayList<>();
        this.jurisdictions = new ArrayList<>();
        this.roles = new ArrayList<>();
    }

    public List<OrdersDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersDTO> orders) {
        this.orders = orders;
    }

    public List<BiddingsDTO> getBiddings() {
        return biddings;
    }

    public void setBiddings(List<BiddingsDTO> biddings) {
        this.biddings = biddings;
    }

    public List<DecreaseDTO> getDecreases() {
        return decreases;
    }

    public void setDecreases(List<DecreaseDTO> decreases) {
        this.decreases = decreases;
    }

    public List<DoctorDTO> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<DoctorDTO> doctors) {
        this.doctors = doctors;
    }

    public List<IncidenceDTO> getIncidences() {
        return incidences;
    }

    public void setIncidences(List<IncidenceDTO> incidences) {
        this.incidences = incidences;
    }

    public List<OrdersStockDTO> getOrdersStock() {
        return ordersStock;
    }

    public void setOrdersStock(List<OrdersStockDTO> ordersStock) {
        this.ordersStock = ordersStock;
    }

    public List<OrderUserDTO> getOrderUsers() {
        return orderUsers;
    }

    public void setOrderUsers(List<OrderUserDTO> orderUsers) {
        this.orderUsers = orderUsers;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public List<ProductRemmisionDTO> getProductRemmisions() {
        return productRemmisions;
    }

    public void setProductRemmisions(List<ProductRemmisionDTO> productRemmisions) {
        this.productRemmisions = productRemmisions;
    }

    public List<ReceptionDetailDTO> getReceptions() {
        return receptions;
    }

    public void setReceptions(List<ReceptionDetailDTO> receptions) {
        this.receptions = receptions;
    }

    public List<ReceptionDetailDTO> getReceptionsComments() {
        return receptionsComments;
    }

    public void setReceptionsComments(List<ReceptionDetailDTO> receptionsComments) {
        this.receptionsComments = receptionsComments;
    }

    public List<ReceptionOrderStockDTO> getReceptionStocks() {
        return receptionStocks;
    }

    public void setReceptionStocks(List<ReceptionOrderStockDTO> receptionStocks) {
        this.receptionStocks = receptionStocks;
    }

    public List<RemmisionsDTO> getRemmisions() {
        return remmisions;
    }

    public void setRemmisions(List<RemmisionsDTO> remmisions) {
        this.remmisions = remmisions;
    }

    public List<RequisitionGobDTO> getRequisitionsGob() {
        return requisitionsGob;
    }

    public void setRequisitionsGob(List<RequisitionGobDTO> requisitionsGob) {
        this.requisitionsGob = requisitionsGob;
    }

    public List<ShipmentsDTO> getShipments() {
        return shipments;
    }

    public void setShipments(List<ShipmentsDTO> shipments) {
        this.shipments = shipments;
    }

    public List<StockSimpleDTO> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockSimpleDTO> stocks) {
        this.stocks = stocks;
    }

    public List<StockHistoryDTO> getStocksHistory() {
        return stocksHistory;
    }

    public void setStocksHistory(List<StockHistoryDTO> stocksHistory) {
        this.stocksHistory = stocksHistory;
    }

    public List<SuppliersDTO> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<SuppliersDTO> suppliers) {
        this.suppliers = suppliers;
    }

    public List<BranchOfficeDTO> getBranchOffices() {
        return branchOffices;
    }

    public void setBranchOffices(List<BranchOfficeDTO> branchOffices) {
        this.branchOffices = branchOffices;
    }

    public List<UsersDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UsersDTO> users) {
        this.users = users;
    }

    public List<CategoriesDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoriesDTO> categories) {
        this.categories = categories;
    }

    public List<FiscalFundDTO> getFiscalFunds() {
        return fiscalFunds;
    }

    public void setFiscalFunds(List<FiscalFundDTO> fiscalFunds) {
        this.fiscalFunds = fiscalFunds;
    }

    public List<JurisdictionsDTO> getJurisdictions() {
        return jurisdictions;
    }

    public void setJurisdictions(List<JurisdictionsDTO> jurisdictions) {
        this.jurisdictions = jurisdictions;
    }

    public List<RolesDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesDTO> roles) {
        this.roles = roles;
    }
}
