package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnWithReturnStockDTO extends  ReturnDTO{

    private List<ReturnStockDTO> returnStocks;

    public List<ReturnStockDTO> getReturnStocks() {
        return returnStocks;
    }

    public void setReturnStocks(List<ReturnStockDTO> returnStocks) {
        this.returnStocks = returnStocks;
    }
}
