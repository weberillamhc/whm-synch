package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.text.SimpleDateFormat;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductsRemmisionDTO {

	private String idProductRemmison;
	private String lot;
	private double unitprice;
	private Integer quantity;
	private String expirationDate;
	private Date expirationdate;
	private String remissionId;
	private Integer productId;
	private ProductsDTO product;
	private RemmisionsDTO remmision;
	private String stockId;
	
	private String productCode;
	private String fiscalFound;
	
	
	

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getFiscalFound() {
		return fiscalFound;
	}

	public void setFiscalFound(String fiscalFound) {
		this.fiscalFound = fiscalFound;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public double getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getExpirationdate() {
		return expirationdate;
	}

	public void setExpirationdate(Date expirationdate) {
		this.expirationdate = expirationdate;
	}

	public String getRemissionId() {
		return remissionId;
	}

	public void setRemissionId(String fk_remissionid) {
		this.remissionId = fk_remissionid;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer fk_idproduct) {
		this.productId = fk_idproduct;
	}

	public ProductsDTO getProduct() {
		return product;
	}

	public void setProduct(ProductsDTO product) {
		this.product = product;
	}

	public RemmisionsDTO getRemmision() {
		return remmision;
	}

	public void setRemmision(RemmisionsDTO remmision) {
		this.remmision = remmision;
	}

	public String getIdProductRemmison() {
		return idProductRemmison;
	}

	public void setIdProductRemmison(String idProductRemmison) {
		this.idProductRemmison = idProductRemmison;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {

		if(expirationDate != null){
			try{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				setExpirationdate(simpleDateFormat.parse(expirationDate));
			}catch (Exception e){

			}
		}
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return "ProductsRemmisionDTO [idProductRemmison=" + idProductRemmison + ", lot=" + lot + ", unitprice="
				+ unitprice + ", quantity=" + quantity + ", expirationdate=" + expirationdate + ", fk_remissionid="
				+ remissionId + ", fk_idproduct=" + productId + ", product=" + product + ", remmision="
				+ remmision + "]";
	}
	

}
