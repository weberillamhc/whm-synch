package com.erillamhc.whm.persistence.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockOrderDTO {

	private String orderId;
	private String stockId;
	private Integer quantity;
	
	private Integer originId;
	private Integer destinationId;
	private Integer userId;
	private Integer status;
	private Integer type;
	private Integer prescription;

	private Integer idUser;

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public Integer getOriginId() {
		return originId;
	}


	public void setOriginId(Integer originId) {
		this.originId = originId;
	}


	public Integer getDestinationId() {
		return destinationId;
	}


	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}



	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getPrescription() {
		return prescription;
	}

	public void setPrescription(Integer prescription) {
		this.prescription = prescription;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
}
