package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductStockDTO {
	
	private Integer branchOfficeId;
	private Integer fiscalFound;
	
	
	public Integer getBranchOfficeId() {
		return branchOfficeId;
	}
	public void setBranchOfficeId(Integer branchOfficeId) {
		this.branchOfficeId = branchOfficeId;
	}
	public Integer getFiscalFound() {
		return fiscalFound;
	}
	public void setFiscalFound(Integer fiscalFound) {
		this.fiscalFound = fiscalFound;
	}
	
	

}
