package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsersDTO  extends SynchMinDTO {
	private Integer idUser;
	private String name;
	private String lastname;
	private String email;
	private String password;	
	private String passwordConfirm;
	private boolean available;
	private String roleId;
	private String roleName;
	private Integer branchOfficeId;
	private BranchOfficeDTO branchOffice;
	private List<BranchOfficeDTO> branchOffices;
	private Integer userID;
	private List<UserBranchOfficeDTO> userBranchOffice;


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirm() {
		return passwordConfirm;
	}
	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public BranchOfficeDTO getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(BranchOfficeDTO branchOffice) {
		this.branchOffice = branchOffice;
	}
	public Integer getBranchOfficeId() {
		return branchOfficeId;
	}
	public void setBranchOfficeId(Integer branchOfficeId) {
		this.branchOfficeId = branchOfficeId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<BranchOfficeDTO> getBranchOffices() {
		return branchOffices;
	}

	public void setBranchOffices(List<BranchOfficeDTO> branchOffices) {
		this.branchOffices = branchOffices;
	}

	@Override
	public String toString() {
		return "UsersDTO{" +
				"name='" + name + '\'' +
				", lastname='" + lastname + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", available=" + available +
				", roleId='" + roleId + '\'' +
				", roleName='" + roleName + '\'' +
				", branchOfficeId=" + branchOfficeId +
				", branchOffice=" + branchOffice +
				'}';
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public List<UserBranchOfficeDTO> getUserBranchOffice() {
		return userBranchOffice;
	}

	public void setUserBranchOffice(List<UserBranchOfficeDTO> userBranchOffice) {
		this.userBranchOffice = userBranchOffice;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
}
