package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SuppliersBiddingsDTO extends SupplierBiddingDTO {

    private SuppliersDTO supplier;
    private BiddingsDTO bidding;

    public SuppliersDTO getSupplier() {
        return supplier;
    }

    public void setSupplier(SuppliersDTO supplier) {
        this.supplier = supplier;
    }

    public BiddingsDTO getBidding() {
        return bidding;
    }

    public void setBidding(BiddingsDTO bidding) {
        this.bidding = bidding;
    }


}
