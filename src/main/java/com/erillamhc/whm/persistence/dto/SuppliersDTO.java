package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SuppliersDTO extends SynchMinDTO {

    private Integer idSupplier;
    private String name;
    private String supplierKey;
    private Integer userID;

    public SuppliersDTO() {
    }

    public SuppliersDTO(String name) {
        this.name = name;
    }
    public SuppliersDTO(String name, Integer idSupplier) {
        this.name = name;
        this.idSupplier = idSupplier;
    }

    public Integer getIdSupplier() {
        return idSupplier;
    }

    public void setIdSupplier(Integer supplierId) {
        this.idSupplier = supplierId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplierKey() {
        return supplierKey;
    }

    public void setSupplierKey(String supplierKey) {
        this.supplierKey = supplierKey;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }
}
