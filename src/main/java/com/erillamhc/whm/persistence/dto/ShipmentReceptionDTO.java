package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentReceptionDTO {
	
	private String idShipment;
	private String driver;
	private String tag;
	private String status;
	private String dateshipment;
	private Integer destinationId;
	private String orderId;
	private BranchOfficeDTO origin;

	public String getIdShipment() {
		return idShipment;
	}

	public void setIdShipment(String idShipment) {
		this.idShipment = idShipment;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateshipment() {
		return dateshipment;
	}

	public void setDateshipment(String dateshipment) {
		this.dateshipment = dateshipment;
	}

	public Integer getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BranchOfficeDTO getOrigin() {
		return origin;
	}

	public void setOrigin(BranchOfficeDTO origin) {
		this.origin = origin;
	}
}
