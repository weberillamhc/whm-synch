package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockOutDTO {

    private String idStock;
    private Integer quantity;
    private String status;
    private String locationKey;
    private String dateInputRemmision;
    private String dateInStock;
    private String remmisionKey;
    private String productName;

    public String getIdStock() {
        return idStock;
    }

    public void setIdStock(String idStock) {
        this.idStock = idStock;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocationKey() {
        return locationKey;
    }

    public void setLocationKey(String locationKey) {
        this.locationKey = locationKey;
    }

    public String getDateInputRemmision() {
        return dateInputRemmision;
    }

    public void setDateInputRemmision(String dateInputRemmision) {
        this.dateInputRemmision = dateInputRemmision;
    }

    public String getRemmisionKey() {
        return remmisionKey;
    }

    public void setRemmisionKey(String remmisionKey) {
        this.remmisionKey = remmisionKey;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDateInStock() {
        return dateInStock;
    }

    public void setDateInStock(String dateInStock) {
        this.dateInStock = dateInStock;
    }


}
