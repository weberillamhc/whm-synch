package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RolesDTO extends SynchMinDTO {
	
	private String idRole;
	private String name;
	private String description;
	private List<RolesPermissionsDTO> permmisions;
	
	
	public String getIdRole() {
		return idRole;
	}
	public void setIdRole(String idRole) {
		this.idRole = idRole;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public List<RolesPermissionsDTO> getPermmisions() {
		return permmisions;
	}

	public void setPermmisions(List<RolesPermissionsDTO> permmisions) {
		this.permmisions = permmisions;
	}
}
