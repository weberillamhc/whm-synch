package com.erillamhc.whm.persistence.dto;

import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequisitionGobDTO extends SynchMinDTO {

	private String idRequisitionGob;
	private String dateRequisition;
	private String requisitionKey;
	private Integer status;
	private String biddingId;
	private Integer branchOfficeId;
	private Integer fiscalFundId;
	private Integer userId;
	private BiddingsDTO bidding;
	private BranchOfficeDTO branchOffice;
	private FiscalFundDTO fiscalFund;

	public RequisitionGobDTO() {	
	}
	public RequisitionGobDTO(String idRequisitionGob) {
		this.idRequisitionGob = idRequisitionGob;
	}

	public String getIdRequisitionGob() {
		return idRequisitionGob;
	}

	public void setIdRequisitionGob(String idRequisitionGob) {
		this.idRequisitionGob = idRequisitionGob;
	}

	public String getDateRequisition() {
		return dateRequisition;
	}

	public void setDateRequisition(String dateRequisition) {
		this.dateRequisition = dateRequisition;
	}

	public String getRequisitionKey() {
		return requisitionKey;
	}

	public void setRequisitionKey(String requisitionKey) {
		this.requisitionKey = requisitionKey;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BiddingsDTO getBidding() {
		return bidding;
	}

	public void setBidding(BiddingsDTO bidding) {
		this.bidding = bidding;
	}

	public BranchOfficeDTO getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOfficeDTO branchOffice) {
		this.branchOffice = branchOffice;
	}

	public FiscalFundDTO getFiscalFund() {
		return fiscalFund;
	}

	public void setFiscalFund(FiscalFundDTO fiscalFund) {
		this.fiscalFund = fiscalFund;
	}
	public String getBiddingId() {
		return biddingId;
	}
	public void setBiddingId(String biddingId) {
		this.biddingId = biddingId;
	}
	public Integer getBranchOfficeId() {
		return branchOfficeId;
	}
	public void setBranchOfficeId(Integer branchOfficeId) {
		this.branchOfficeId = branchOfficeId;
	}
	public Integer getFiscalFundId() {
		return fiscalFundId;
	}
	public void setFiscalFundId(Integer fiscalFundId) {
		this.fiscalFundId = fiscalFundId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
