package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DecreaseOrderDTO {


    private String idDecrease;
    private String reason;
    private String date;
    private String orderId;
    private String decreaseUser;

    public String getIdDecrease() {
        return idDecrease;
    }

    public void setIdDecrease(String idDecrease) {
        this.idDecrease = idDecrease;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDecreaseUser() {
        return decreaseUser;
    }

    public void setDecreaseUser(String decreaseUser) {
        this.decreaseUser = decreaseUser;
    }
}
