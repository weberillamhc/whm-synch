package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddRemmisionsDTO extends RemmisionsDTO{
	private Integer idUser;
	private List<ProductsRemmisionDTO> produdctsRemmision;

	public List<ProductsRemmisionDTO> getProdudctsRemmision() {
		return produdctsRemmision;
	}
	public void setProdudctsRemmision(List<ProductsRemmisionDTO> produdctsRemmision) {
		this.produdctsRemmision = produdctsRemmision;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
}
