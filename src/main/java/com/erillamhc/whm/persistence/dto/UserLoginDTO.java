package com.erillamhc.whm.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLoginDTO {

    private Integer idUser;
    private String name;
    private String lastname;
    private String email;
    private String password;
    private RolesDTO role;

    private List<PermissionsDTO> permissions;
    private List<BranchOfficeDTO> branchOffices;

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public RolesDTO getRole() {
        return role;
    }

    public void setRole(RolesDTO role) {
        this.role = role;
    }

    public List<PermissionsDTO> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionsDTO> permissions) {
        this.permissions = permissions;
    }

    public List<BranchOfficeDTO> getBranchOffices() {
        return branchOffices;
    }

    public void setBranchOffices(List<BranchOfficeDTO> branchOffices) {
        this.branchOffices = branchOffices;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
