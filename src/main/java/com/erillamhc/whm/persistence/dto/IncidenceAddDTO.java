package com.erillamhc.whm.persistence.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IncidenceAddDTO {

    private Integer idBranchOffice;
    private Integer idUser;
    private Integer typeIncidence;
    private SupplierProductDTO supplierProduct;
    private ProductIncidenceDTO product;
    private String reason;

    public Integer getIdBranchOffice() {
        return idBranchOffice;
    }

    public void setIdBranchOffice(Integer idBranchOffice) {
        this.idBranchOffice = idBranchOffice;
    }

    public Integer getTypeIncidence() {
        return typeIncidence;
    }

    public void setTypeIncidence(Integer typeIncidence) {
        this.typeIncidence = typeIncidence;
    }

    public SupplierProductDTO getSupplierProduct() {
        return supplierProduct;
    }

    public void setSupplierProduct(SupplierProductDTO supplierProduct) {
        this.supplierProduct = supplierProduct;
    }

    public ProductIncidenceDTO getProduct() {
        return product;
    }

    public void setProduct(ProductIncidenceDTO product) {
        this.product = product;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
}
