package com.erillamhc.whm.persistence.util;

public enum TypeIncidence {

    FALTANTE(1), SOBRANTE(2), DANADO(3),NO_SOLICITADO(4), CADUCADO(5), CUSTOM(-1);

    private final Integer value;

    TypeIncidence(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public String getDescription(Integer value) {
        switch (value) {
            case 1:
                return "Faltante";
            case 2:
                return "Sobrante";
            case 3:
                return "Da�ado";
            case 4:
                return "No solicitado";
            case 5:
                return "Caducado";
            case 6:
                return "Error de captura";
            case 7:
                return "Otro";
            default:
                return "No identificado";
        }
    }
}
