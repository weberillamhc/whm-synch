package com.erillamhc.whm.persistence.util;


/**
 *
 * @author Fernando FH
 */
public enum ReceptionEnum {

    CON_INCIDENCIA(0), COMPLETO(1), CUSTOM();

    private final Integer status;

    ReceptionEnum() {
        this.status = null;
    }
    ReceptionEnum(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDescription(Integer status) {
        switch (status) {
            case 0:
                return "Con incidencia";
            case 1:
                return "Completo";
            default:
                return "No definido";
        }
    }
}
