package com.erillamhc.whm.persistence.util;

/**
*
* 
*/
public enum OrderEnum {
   PENDIENTE(1), PICKING(2), VALIDADO(3), ENVIADO(4), DISPENSACION_PENDIENTE(5), DISPENSADO(6), MERMA_PENDIENTE(7), MERMA_FINALIZADA(8), CUSTOM(-1);
   
   private final Integer status;
   
   private OrderEnum(Integer status) {
       this.status = status;
   }

   public Integer getStatus() {
       return status;
   }

    public String getDescription(Integer status) {
        switch (status) {
            case 1:
                return "Pendiente";
            case 2:
                return "Picking";
            case 3:
                return "Validado";
            case 4:
                return "Enviado";
            case 5:
                return "Dispensación Pendiente";
            case 6:
                return "Dispensado";
            case 7:
                return "Merma Pendiente";
            case 8:
                return "Merma Finalizada";
            default:
                return "No definido";
        }
    }

}
