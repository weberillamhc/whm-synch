package com.erillamhc.whm.persistence.util;

/**
*
* 
*/
public enum DecreaseEnum {
   CUSTOM(-1);

   private final Integer status;

   private DecreaseEnum(Integer status) {
       this.status = status;
   }

   public Integer getStatus() {
       return status;
   }

    public String getDescription(Integer status) {
        switch (status) {
            case 1:
                return "Autorización";
            case 2:
                return "Caducidad";
            case 3:
                return "Canje";
            case 4:
                return "Deterioro";
            case 5:
                return "Extravío";
            case 6:
                return "Error de surtido";
            default:
                return "No definido";
        }
    }

}
