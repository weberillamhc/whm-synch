package com.erillamhc.whm.persistence.util;

public enum IncidenceStatus {

    PENDIENTE(0), RESUELTA(1), CUSTOM(-1);

    private final Integer value;

    IncidenceStatus(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public String getDescription(Integer value) {
        switch (value) {
            case 0:
                return "Pendiente";
            case 1:
                return "Resuelta";
            default:
                return "No identificado";
        }
    }
}
