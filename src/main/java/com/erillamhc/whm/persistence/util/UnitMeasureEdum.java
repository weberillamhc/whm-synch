package com.erillamhc.whm.persistence.util;

/**
 * @author Fernando FH
 **/
public enum UnitMeasureEdum {


    INDIVIDUAL(1), EQUIPO(2),PIEZA(3), ENVASE(4), PAQUETE(5), PAR(6), CUSTOM();

    private final Integer value;

    UnitMeasureEdum() {
        this.value = null;
    }

    UnitMeasureEdum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public String getDescription(Integer type) {
        if (type == 1) {
            return "Individual";
        } else if (type == 2) {
            return "Equipo";
        }else if(type == 3){
            return "Pieza";
        }else if(type == 4){
            return "Envase";
        }else if(type == 5){
            return "Paquete";
        }else if(type == 6){
            return  "Par";
        }else if(type == 7){
            return "Rollo";
        }else if(type == 8){
            return "Tableta";
        }else if(type == 9){
            return "Frasco";
        }
        return "No definido";
    }
}
