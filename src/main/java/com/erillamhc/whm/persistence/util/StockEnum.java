package com.erillamhc.whm.persistence.util;

/**
 * @author Fernando FH
 */
public enum StockEnum {

    AGOTADO(0), EN_PISO(1), ACOMODADO(2), BLOQUEADO(3), ENVIADO(4), CUSTOM();

    private final Integer stock;

    private StockEnum() {
        this.stock = null;
    }

    private StockEnum(Integer stock) {
        this.stock = stock;
    }

    public Integer getStock() {
        return stock;
    }

    public String getDescription(Integer status) {
        switch (status) {
            case 0:
                return "Agotado";
            case 1:
                return "En piso";
            case 2:
                return "Acomodado";
            case 3:
                return "Bloqueado";
            case 4:
                return "Enviado";
            default:
                return "No definido";
        }
    }

}
