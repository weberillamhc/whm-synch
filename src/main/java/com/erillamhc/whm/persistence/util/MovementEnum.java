package com.erillamhc.whm.persistence.util;

/**
 *
 * @author Fernando FH
 */
public enum MovementEnum {

    REGISTRO(1), MODIFICACION(2), ELIMINACION(3), MODIFICACION_STATUS(4),CUSTOM();

    private final Integer value;

    MovementEnum() {
        this.value = null;
    }
    MovementEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public String getDescription(Integer type) {
        if (type == 1) {
            return "Registro";
        }else if(type == 2){
            return "Modificacion";
        }else if(type == 3){
            return "Eliminacion";
        }else if(type == 4){
            return "Modificacion status";
        }
        return "No definido";
    }
}
