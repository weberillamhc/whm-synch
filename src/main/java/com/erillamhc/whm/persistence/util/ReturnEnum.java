package com.erillamhc.whm.persistence.util;

public enum ReturnEnum {

    PENDIENTE(1), RECIBIDO(2), CUSTOM(-1);

    private final Integer status;

    ReturnEnum(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }


    public String getDescription(Integer status) {
        switch (status) {
            case 1:
                return "Pendiente";
            case 2:
                return "Recibido";
            default:
                return "No definido";
        }
    }
}
