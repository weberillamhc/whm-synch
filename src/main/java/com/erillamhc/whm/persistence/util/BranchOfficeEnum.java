package com.erillamhc.whm.persistence.util;

/**
*
* @author Fernando FH
*/
public enum BranchOfficeEnum {

    INACTIVO(0), ACTIVO(1), CUSTOM();

   private final Integer value;

   BranchOfficeEnum() {
        this.value = null;
    }
    BranchOfficeEnum(Integer value) {
       this.value = value;
   }

   public Integer getValue() {
       return value;
   }


    public String getDescription(Integer status) {
        if (status == 1) {
            return "Activo";
        }else if(status == 0){
            return "Inactivo";
        }
        return "No definido";
    }
}
