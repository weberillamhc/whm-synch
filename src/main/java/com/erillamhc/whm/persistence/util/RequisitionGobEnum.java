package com.erillamhc.whm.persistence.util;

/**
*
* @author Fernando FH
*/
public enum RequisitionGobEnum {
	
	PENDIENTE(1), CUSTOM();
  
   private final Integer requisitionGob;

    private RequisitionGobEnum() {
        this.requisitionGob = null;
    }
   private RequisitionGobEnum(Integer requisitionGob) {
       this.requisitionGob = requisitionGob;
   }

   public Integer getRequisitionGob() {
       return requisitionGob;
   }


    public String getDescription(Integer status) {
        switch (status) {
            case 1:
                return "Pendiente";
            default:
                return "No definido";
        }
    }
}
