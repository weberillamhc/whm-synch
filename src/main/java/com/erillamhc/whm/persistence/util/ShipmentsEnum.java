package com.erillamhc.whm.persistence.util;

/**
*
* 
*/
public enum ShipmentsEnum {
    PENDIENTE(1), ENVIADO(2), TRANSITO(3), RECIBIDO(4), CUSTOM(-1);
   
   private final Integer status;
   
   private ShipmentsEnum(Integer status) {
       this.status = status;
   }

   public Integer getStatus() {
       return status;
   }


    public String getDescription(Integer status) {
        switch (status) {
            case 1:
                return "Pendiente";
            case 2:
                return "Enviado";
            case 3:
                return "Transito";
            case 4:
                return "Recibido";
            default:
                return "No definido";
        }
    }
}
