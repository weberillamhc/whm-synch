package com.erillamhc.whm.persistence.util;


/**
 *
 * @author Fernando FH
 */
public enum SynchStatusEnum {


    SINCRONIZADO(1), NO_SINCRONIZADO(0), CUSTOM();

    private final Integer value;

    SynchStatusEnum() {
        this.value = null;
    }
    SynchStatusEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public String getDescription(Integer type) {
        if (type == 1) {
            return "Sincronizado";
        }else if(type == 2){
            return "No sincronizado";
        }
        return "No definido";
    }
}
