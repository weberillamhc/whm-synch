package com.erillamhc.whm.persistence.util;

/**
*
* @author Fernando FH
*/
public enum RemmisionEnum {
	
	PENDIENTE(1), VALIDANDO(2), INGRESADO(3), CUSTOM();
  
   private final Integer remmision;
   
   private RemmisionEnum() {
       this.remmision = null;
   }
   private RemmisionEnum(Integer remmision) {
       this.remmision = remmision;
   }

   public Integer getRemmision() {
       return remmision;
   }
   
   public String getDescription(Integer status) {
		switch (status) {
		case 1:
			return "Pendiente";
		case 2:
			return "Validando";
		case 3:
			return "Ingresado";
		default:
			return "No definido";
		}
	}
}

