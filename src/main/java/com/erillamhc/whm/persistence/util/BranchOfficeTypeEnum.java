package com.erillamhc.whm.persistence.util;

/**
*
* @author Fernando FH
*/
public enum BranchOfficeTypeEnum {

    CENTRAL(1), REGIONAL(2), CENTRO_SALUD(3), CUSTOM();

   private final Integer value;

   BranchOfficeTypeEnum() {
        this.value = null;
    }
    BranchOfficeTypeEnum(Integer value) {
       this.value = value;
   }

   public Integer getValue() {
       return value;
   }


    public String getDescription(Integer type) {
        if (type == 1) {
            return "Central";
        }else if(type == 2){
            return "Regional";
        }else if(type == 3){
            return "Centro de salud";
        }else if(type == 4){
            return "Clinicas COVID";
        }else if(type == 5){
            return "Gerenciales";
        }else if(type == 6){
            return "Hospitales Generales y Basicos";
        }
        return "No definido";
    }
}
