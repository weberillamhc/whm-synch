package com.erillamhc.whm.persistence.util;

public enum OrderStockEnum {

    ORDINARIO(1), INCIDENCIA(0), CUSTOM(-1);

    private final Integer status;

    OrderStockEnum(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }


    public String getDescription(Integer status) {
        switch (status) {
            case 0:
                return "Inicidencia";
            case 1:
                return "Resuelta";
            default:
                return "No definido";
        }
    }
}
