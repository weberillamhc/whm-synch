package com.erillamhc.whm.persistence.util;

import java.util.Date;

import com.erillamhc.whm.persistence.dto.*;
import com.erillamhc.whm.persistence.entity.*;

/**
 *
 * @author Fernando FH
 */
public class ParserFacade {

	private ParserFacade() {
		// No instance a static class.
	}

	public static RemmisionsDTO parseRemmision(Remmision remmision, boolean withDepdencies) {
		if (remmision != null) {
			RemmisionsDTO dto = new RemmisionsDTO();
			dto.setIdRemmision(remmision.getIdRemmision());
			dto.setDateinput(remmision.getDateinput());
			dto.setRemissionkey(remmision.getRemmisionKey());
			return dto;
		}
		return null;
	}

	public static RemmisionsDTO parseRemmision(Remmision remmision) {
		return parseRemmision(remmision, false);
	}

	public static ProductsDTO parseProduct(Product product) {
		if (product != null) {
			ProductsDTO dto = new ProductsDTO();
			dto.setIdProduct(product.getIdProduct());
			dto.setName(product.getName());
			dto.setDescription(product.getDescription());
			dto.setProductKey(product.getProductKey());
			return dto;
		}
		return null;
	}
	public static SuppliersDTO parseSupplier(Supplier supplier) {
		if (supplier != null) {
			SuppliersDTO dto = new SuppliersDTO();
			dto.setIdSupplier(supplier.getIdSupplier());
			dto.setName(supplier.getName());
			dto.setSupplierKey(supplier.getSupplierKey());
			return dto;
		}
		return null;
	}

	public static BranchOfficeDTO parseBranchOffice(BranchOffice branchOffice, boolean withDepdencies) {
		if (branchOffice != null) {
			BranchOfficeDTO dto = new BranchOfficeDTO();
			dto.setIdBranchoffice(branchOffice.getIdBranchoffice());
			dto.setName(branchOffice.getName());
			dto.setType(branchOffice.getType());
			if (withDepdencies) {
				dto.setParent(branchOffice.getBranchOffice().getIdBranchoffice());
				dto.setJurisdictionId(branchOffice.getJurisdiction().getIdJurisdiction());
			}
			return dto;
		}
		return null;
	}

	public static BranchOfficeDTO parseBranchOffice(BranchOffice branchOffice) {
		return parseBranchOffice(branchOffice, false);
	}

	public static StockDTO parseStock(Stock stock, boolean withDepdencies) {
		if (stock != null) {
			StockDTO dto = new StockDTO();
			dto.setIdStock(stock.getIdStock());
			dto.setLocationkey(stock.getLocationkey());
			dto.setQuantity(stock.getQuantity());
			dto.setStatus(stock.getStatus());
			if (withDepdencies) {
				/*Agregar dependencias*/
			}
			return dto;
		}
		return null;
	}

	public static StockDTO parseStock(Stock stock) {
		return parseStock(stock, false);
	}

}
