package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the doctor database table.
 * 
 */
@Entity
@NamedQuery(name="Doctor.findAll", query="SELECT d FROM Doctor d")
public class Doctor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_doctor")
	private String idDoctor;


	@Column(name="last_name")
	private String lastName;

	private String name;

	@Column(name="professional_id")
	private String professionalId;
	
	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_branch_officeid")
	private BranchOffice branchOffice;

	//bi-directional many-to-one association to MedicalPrescription
	@OneToMany(mappedBy="doctor")
	private List<MedicalPrescription> medicalPrescriptions;

	public Doctor() {
	}

	public Doctor(String idDoctor) {
		this.idDoctor = idDoctor;
	}

	public String getIdDoctor() {
		return this.idDoctor;
	}

	public void setIdDoctor(String idDoctor) {
		this.idDoctor = idDoctor;
	}


	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfessionalId() {
		return this.professionalId;
	}

	public void setProfessionalId(String professionalId) {
		this.professionalId = professionalId;
	}

	public List<MedicalPrescription> getMedicalPrescriptions() {
		return this.medicalPrescriptions;
	}

	public void setMedicalPrescriptions(List<MedicalPrescription> medicalPrescriptions) {
		this.medicalPrescriptions = medicalPrescriptions;
	}

	public MedicalPrescription addMedicalPrescription(MedicalPrescription medicalPrescription) {
		getMedicalPrescriptions().add(medicalPrescription);
		medicalPrescription.setDoctor(this);

		return medicalPrescription;
	}

	public MedicalPrescription removeMedicalPrescription(MedicalPrescription medicalPrescription) {
		getMedicalPrescriptions().remove(medicalPrescription);
		medicalPrescription.setDoctor(null);

		return medicalPrescription;
	}

	public BranchOffice getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}

	
	
}