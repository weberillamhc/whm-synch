package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;


/**
 * The persistent class for the reception_stock database table.
 * 
 */
@Entity
@Table(name="return_product")
@NamedQuery(name="ReturnProduct.findAll", query="SELECT r FROM ReturnProduct r")
public class ReturnProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_return_product")
	private String idReturnProduct;

	@Column(name="date_return")
	private Timestamp dateReturn;

	@Column(name="date_reception")
	private Timestamp dateRecepction;

	private Integer status;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="destination_branch_office_id")
	private BranchOffice destinationBranchOffice;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="destination_supplier_id")
	private Supplier destinationSupplier;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="branch_office_id")
	private BranchOffice origin;




	// bi-directional many-to-one association to BranchOffice
	@OneToMany(mappedBy = "returnProduct")
	private List<ReturnStock> returnStocks;

	public String getIdReturnProduct() {
		return idReturnProduct;
	}

	public void setIdReturnProduct(String idReturnProduct) {
		this.idReturnProduct = idReturnProduct;
	}

	public Timestamp getDateReturn() {
		return dateReturn;
	}

	public void setDateReturn(Timestamp dateReturn) {
		this.dateReturn = dateReturn;
	}

	public Timestamp getDateRecepction() {
		return dateRecepction;
	}

	public void setDateRecepction(Timestamp dateRecepction) {
		this.dateRecepction = dateRecepction;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BranchOffice getOrigin() {
		return origin;
	}

	public void setOrigin(BranchOffice origin) {
		this.origin = origin;
	}


	public List<ReturnStock> getReturnStocks() {
		return returnStocks;
	}

	public void setReturnStocks(List<ReturnStock> returnStocks) {
		this.returnStocks = returnStocks;
	}

	public BranchOffice getDestinationBranchOffice() {
		return destinationBranchOffice;
	}

	public void setDestinationBranchOffice(BranchOffice destinationBranchOffice) {
		this.destinationBranchOffice = destinationBranchOffice;
	}

	public Supplier getDestinationSupplier() {
		return destinationSupplier;
	}

	public void setDestinationSupplier(Supplier destinationSupplier) {
		this.destinationSupplier = destinationSupplier;
	}


}