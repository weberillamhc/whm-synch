package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the remmisions database table.
 * 
 */
@Entity
@Table(name="remmisions")
@NamedQueries({
	@NamedQuery(name="Remmision.findAll", query="SELECT r FROM Remmision r"),
	@NamedQuery(name="Remmision.findByRemmisionKey", query="SELECT r FROM Remmision r WHERE r.remmisionKey = :remmisionKey"),
	@NamedQuery(name="Remmision.findByRemissionkeyAndIdNotEqual", query="SELECT r FROM Remmision r WHERE r.remmisionKey = :remmisionKey AND r.idRemmision <> :idRemmision")
})
public class Remmision implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_remmision")
	private String idRemmision;

	private Timestamp dateinput;

	@Column(name="remmision_key")
	private String remmisionKey;

	private Integer status;

	//bi-directional many-to-one association to ProductsRemmision
	@OneToMany(mappedBy="remmision")
	private List<ProductsRemmision> productsRemmisions;

	//bi-directional many-to-one association to RequisitionGob
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="requisition_gob_id")
	private RequisitionGob requisitionGob;

	//bi-directional many-to-one association to RequisitionGob
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;

	public Remmision() {
	}
	public Remmision(String idRemmision) {
		this.idRemmision = idRemmision;
	}

	public String getIdRemmision() {
		return this.idRemmision;
	}

	public void setIdRemmision(String idRemmision) {
		this.idRemmision = idRemmision;
	}

	public Timestamp getDateinput() {
		return this.dateinput;
	}

	public void setDateinput(Timestamp dateinput) {
		this.dateinput = dateinput;
	}

	public String getRemmisionKey() {
		return this.remmisionKey;
	}

	public void setRemmisionKey(String remmisionKey) {
		this.remmisionKey = remmisionKey;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ProductsRemmision> getProductsRemmisions() {
		return this.productsRemmisions;
	}

	public void setProductsRemmisions(List<ProductsRemmision> productsRemmisions) {
		this.productsRemmisions = productsRemmisions;
	}

	public ProductsRemmision addProductsRemmision(ProductsRemmision productsRemmision) {
		getProductsRemmisions().add(productsRemmision);
		productsRemmision.setRemmision(this);

		return productsRemmision;
	}

	public ProductsRemmision removeProductsRemmision(ProductsRemmision productsRemmision) {
		getProductsRemmisions().remove(productsRemmision);
		productsRemmision.setRemmision(null);

		return productsRemmision;
	}

	public RequisitionGob getRequisitionGob() {
		return this.requisitionGob;
	}

	public void setRequisitionGob(RequisitionGob requisitionGob) {
		this.requisitionGob = requisitionGob;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}