package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the decrease database table.
 * 
 */
@Entity
@NamedQuery(name="Decrease.findAll", query="SELECT d FROM Decrease d")
public class Decrease implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_decrease")
	private String idDecrease;

	/*@Column(name="\"fk_orderId\"")
	private String fk_orderId;*/
	
	//bi-directional many-to-one association to Order
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="\"fk_orderId\"")
	private Order order;

	private Integer status;
	
	private Timestamp date;

	public Decrease() {
	}

	public String getIdDecrease() {
		return this.idDecrease;
	}

	public void setIdDecrease(String idDecrease) {
		this.idDecrease = idDecrease;
	}


	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}
	
	

}