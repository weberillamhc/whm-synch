package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the movements database table.
 * 
 */
@Entity
@Table(name="movements")
@NamedQuery(name="Movement.findAll", query="SELECT m FROM Movement m")
public class Movement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idmovement;


	private Timestamp datemovement;

	private String entity;

	@Column(name="data_entity")
	private String data;

	private Integer movementtype;

	private String originmovement;

	@Column(name="id_entity")
	private String idEntity;

	@Column(name="id_entity_secondary")
	private String idEntitySecondary;

	@Column(name="user_id")
	private Integer userId;

	@Column(name="id_branch_office")
	private Integer idBranchOffice;

	public Movement() {
	}

	public String getIdmovement() {
		return idmovement;
	}

	public void setIdmovement(String idmovement) {
		this.idmovement = idmovement;
	}

	public Timestamp getDatemovement() {
		return this.datemovement;
	}

	public void setDatemovement(Timestamp datemovement) {
		this.datemovement = datemovement;
	}

	public String getEntity() {
		return this.entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Integer getMovementtype() {
		return this.movementtype;
	}

	public void setMovementtype(Integer movementtype) {
		this.movementtype = movementtype;
	}

	public String getOriginmovement() {
		return this.originmovement;
	}

	public void setOriginmovement(String originmovement) {
		this.originmovement = originmovement;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


	public String getIdEntity() {
		return idEntity;
	}

	public void setIdEntity(String idEntity) {
		this.idEntity = idEntity;
	}

	public String getIdEntitySecondary() {
		return idEntitySecondary;
	}

	public void setIdEntitySecondary(String idEntitySecondary) {
		this.idEntitySecondary = idEntitySecondary;
	}

	public Integer getIdBranchOffice() {
		return idBranchOffice;
	}

	public void setIdBranchOffice(Integer idBranchOffice) {
		this.idBranchOffice = idBranchOffice;
	}
}