package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;


/**
 * The persistent class for the orders_stock database table.
 * 
 */
@Entity
@Table(name="orders_stock")
@NamedQueries({
		@NamedQuery(name="OrdersStock.findAll", query="SELECT o FROM OrdersStock o"),
		@NamedQuery(name="OrdersStock.findByStockAndOrder", query="SELECT o FROM OrdersStock o WHERE o.order = :order AND o.stock = :stock")
})
public class OrdersStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private OrdersStockPK id;
	
	private Integer prescriptions;

	private Integer quantity;

	private Integer status;

	//bi-directional many-to-one association to Order
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_idorder",insertable = false, updatable = false)
	private Order order;

	//bi-directional many-to-one association to Stock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_idstock", insertable = false, updatable = false)
	private Stock stock;
	
	//bi-directional many-to-one association to ReceptionStock
	@OneToMany(mappedBy="ordersStock")
	private List<ReceptionStock> receptionStocks;

	// bi-directional ont-to-one association to BranchOffice
	@OneToOne(mappedBy = "ordersStock")
	private Incidence incidence;

	public OrdersStock() {
	}

	public OrdersStock(Order order, Stock stock) {
		this.order = order;
		this.stock = stock;
	}

	public OrdersStockPK getId() {
		return this.id;
	}

	public void setId(OrdersStockPK id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Integer getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(Integer prescriptions) {
		this.prescriptions = prescriptions;
	}

	public List<ReceptionStock> getReceptionStocks() {
		return this.receptionStocks;
	}

	public void setReceptionStocks(List<ReceptionStock> receptionStocks) {
		this.receptionStocks = receptionStocks;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Incidence getIncidenceOrder() {
		return incidence;
	}

	public void setIncidenceOrder(Incidence incidence) {
		this.incidence = incidence;
	}
}