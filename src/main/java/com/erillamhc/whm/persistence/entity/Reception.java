package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the reception database table.
 * 
 */
@Entity
@NamedQuery(name="Reception.findAll", query="SELECT r FROM Reception r")
public class Reception implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reception")
	private String idReception;

	@Column(name="date_reception")
	private Timestamp dateReception;

	private Integer status;

	//bi-directional many-to-one association to Shipment
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="shipment_id")
	private Shipment shipment;



     // bi-directional ont-to-one association to BranchOffice
    @OneToOne(mappedBy = "reception")
	private ReceptionComment receptionComment;

	//bi-directional many-to-one association to ReceptionStock
	@OneToMany(mappedBy="reception")
	private List<ReceptionStock> receptionStocks;

	public Reception() {
	}

	public String getIdReception() {
		return this.idReception;
	}

	public void setIdReception(String idReception) {
		this.idReception = idReception;
	}

	public Timestamp getDateReception() {
		return this.dateReception;
	}

	public void setDateReception(Timestamp dateReception) {
		this.dateReception = dateReception;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Shipment getShipment() {
		return this.shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}



	public List<ReceptionStock> getReceptionStocks() {
		return this.receptionStocks;
	}

	public void setReceptionStocks(List<ReceptionStock> receptionStocks) {
		this.receptionStocks = receptionStocks;
	}

	public ReceptionStock addReceptionStock(ReceptionStock receptionStock) {
		getReceptionStocks().add(receptionStock);
		receptionStock.setReception(this);

		return receptionStock;
	}

	public ReceptionStock removeReceptionStock(ReceptionStock receptionStock) {
		getReceptionStocks().remove(receptionStock);
		receptionStock.setReception(null);

		return receptionStock;
	}

	public ReceptionComment getReceptionComment() {
		return receptionComment;
	}

	public void setReceptionComment(ReceptionComment receptionComment) {
		this.receptionComment = receptionComment;
	}
}