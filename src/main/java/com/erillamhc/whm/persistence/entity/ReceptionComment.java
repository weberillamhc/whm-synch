package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the reception_comment database table.
 * 
 */
@Entity
@Table(name="reception_comment")
@NamedQuery(name="ReceptionComment.findAll", query="SELECT r FROM ReceptionComment r")
public class ReceptionComment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reception_comment")
	private String idReceptionComment;

	private String comment;

	//bi-directional ont-to-one association to Reception
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="reception_id")
	private Reception reception;

	public ReceptionComment() {
	}

	public String getIdReceptionComment() {
		return this.idReceptionComment;
	}

	public void setIdReceptionComment(String idReceptionComment) {
		this.idReceptionComment = idReceptionComment;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Reception getReception() {
		return this.reception;
	}

	public void setReception(Reception reception) {
		this.reception = reception;
	}

}