package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the branch_office database table.
 */
@Entity
@Table(name = "branch_office")
@NamedQuery(name = "BranchOffice.findAll", query = "SELECT b FROM BranchOffice b")
public class BranchOffice implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_branchoffice")
    private Integer idBranchoffice;

    // bi-directional many-to-one association to Jurisdiction
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_jurisdictionid")
    private Jurisdiction jurisdiction;

    private String name;

    private Integer type;

    private Integer status;

    @Column(name = "branch_office_key")
    private String branchOfficeKey;

    @Column(name = "warehouse_id")
    private String warehouseId;

    // bi-directional many-to-one association to BranchOffice
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent")
    private BranchOffice branchOffice;

    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "branchOffice")
    private List<BranchOffice> branchOffices;

    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "branchOffice")
    private List<UserBranchOffice> usersBranchOffice;

    // bi-directional many-to-one association to BranchOffice
    @OneToOne(mappedBy = "branchOffice")
    private AddressBranchOffice addressBranchOffice;

    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "branchOffice")
    private List<Doctor> doctors;

    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "branchOffice")
    private List<Shipment> shipments;

    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "branchOffice")
    private List<Incidence> incidences;


    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "supplierBranchOffice")
    private List<Incidence> supplierIncidences;

    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "destinationBranchOffice")
    private List<ReturnProduct> destinacionsBranchOffice;


    // bi-directional many-to-one association to BranchOffice
    @OneToMany(mappedBy = "destinationBranchOffice")
    private List<ReturnProduct> destinations;


    @OneToMany(mappedBy = "origin")
    private List<ReturnProduct> returnProducts;



    @OneToMany(mappedBy = "branchOffice")
    private List<Stock> stocks;

    public BranchOffice() {
    }

    public BranchOffice(Integer idBranchoffice) {
        this.idBranchoffice = idBranchoffice;
    }

    public Integer getIdBranchoffice() {
        return this.idBranchoffice;
    }

    public void setIdBranchoffice(Integer idBranchoffice) {
        this.idBranchoffice = idBranchoffice;
    }


    public Jurisdiction getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Jurisdiction jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BranchOffice getBranchOffice() {
        return this.branchOffice;
    }

    public void setBranchOffice(BranchOffice branchOffice) {
        this.branchOffice = branchOffice;
    }

    public List<BranchOffice> getBranchOffices() {
        return this.branchOffices;
    }

    public void setBranchOffices(List<BranchOffice> branchOffices) {
        this.branchOffices = branchOffices;
    }

    public BranchOffice addBranchOffice(BranchOffice branchOffice) {
        getBranchOffices().add(branchOffice);
        branchOffice.setBranchOffice(this);

        return branchOffice;
    }

    public BranchOffice removeBranchOffice(BranchOffice branchOffice) {
        getBranchOffices().remove(branchOffice);
        branchOffice.setBranchOffice(null);

        return branchOffice;
    }

    public List<UserBranchOffice> getUsersBranchOffice() {
        return usersBranchOffice;
    }

    public void setUsersBranchOffice(List<UserBranchOffice> usersBranchOffice) {
        this.usersBranchOffice = usersBranchOffice;
    }

    public UserBranchOffice addUserBranchOffice(UserBranchOffice userBranchOffice) {
        getUsersBranchOffice().add(userBranchOffice);
        userBranchOffice.setBranchOffice(this);

        return userBranchOffice;
    }

    public UserBranchOffice removeUserBranchOffice(UserBranchOffice userBranchOffice) {
        getUsersBranchOffice().remove(userBranchOffice);
        userBranchOffice.setBranchOffice(null);

        return userBranchOffice;
    }

    public Doctor addDoctor(Doctor doctor) {
        getDoctors().add(doctor);
        doctor.setBranchOffice(this);
        return doctor;
    }

    public Doctor removeDoctor(Doctor doctor) {
        getDoctors().remove(doctor);
        doctor.setBranchOffice(null);
        return doctor;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public String getBranchOfficeKey() {
        return branchOfficeKey;
    }

    public void setBranchOfficeKey(String branchOfficeKey) {
        this.branchOfficeKey = branchOfficeKey;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

	public AddressBranchOffice getAddressBranchOffice() {
		return addressBranchOffice;
	}

    public Incidence addIncidenceOrder(Incidence incidence) {
        getIncidenceOrders().add(incidence);
        incidence.setBranchOffice(this);
        return incidence;
    }

    public Incidence removeIncidenceOrder(Incidence incidence) {
        getIncidenceOrders().remove(incidence);
        incidence.setBranchOffice(null);
        return incidence;
    }

	public void setAddressBranchOffice(AddressBranchOffice addressBranchOffice) {
		this.addressBranchOffice = addressBranchOffice;
	}

    public List<Incidence> getIncidenceOrders() {
        return incidences;
    }

    public void setIncidenceOrders(List<Incidence> incidences) {
        this.incidences = incidences;
    }



    public List<ReturnProduct> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<ReturnProduct> destinations) {
        this.destinations = destinations;
    }

    public List<Incidence> getIncidences() {
        return incidences;
    }

    public void setIncidences(List<Incidence> incidences) {
        this.incidences = incidences;
    }

    public List<Incidence> getSupplierIncidences() {
        return supplierIncidences;
    }

    public void setSupplierIncidences(List<Incidence> supplierIncidences) {
        this.supplierIncidences = supplierIncidences;
    }

    public List<ReturnProduct> getDestinacionsBranchOffice() {
        return destinacionsBranchOffice;
    }

    public void setDestinacionsBranchOffice(List<ReturnProduct> destinacionsBranchOffice) {
        this.destinacionsBranchOffice = destinacionsBranchOffice;
    }

    public List<ReturnProduct> getReturnProducts() {
        return returnProducts;
    }

    public void setReturnProducts(List<ReturnProduct> returnProducts) {
        this.returnProducts = returnProducts;
    }

    public List<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipment> shipments) {
        this.shipments = shipments;
    }

    public List<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(List<Stock> stocks) {
        this.stocks = stocks;
    }
}