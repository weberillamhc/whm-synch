package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQueries({
	@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
	@NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"), 
	@NamedQuery(name = "User.findByEmailAndIdNotEqual", query = "SELECT u FROM User u WHERE u.email = :email AND u.idUser <> :idUser")
})
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_user")
	private Integer idUser;

	private Boolean available;

	private String email;

	private String lastname;

	private String name;

	private String password;

	//bi-directional many-to-one association to BranchOffice
	@OneToMany(mappedBy="user")
	private List<UserBranchOffice> userBranchOffices;

	//bi-directional many-to-one association to Role
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_role")
	private Role role;



	public User() {
	}

	public User( Integer user) {
		this.idUser = user;
	}

	public Integer getIdUser() {
		return this.idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Boolean getAvailable() {
		return this.available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserBranchOffice> getUserBranchOffices() {
		return userBranchOffices;
	}

	public void setUserBranchOffices(List<UserBranchOffice> userBranchOffices) {
		this.userBranchOffices = userBranchOffices;
	}

	public UserBranchOffice addUserBranchOffice(UserBranchOffice userBranchOffice) {
		getUserBranchOffices().add(userBranchOffice);
		userBranchOffice.setUser(this);

		return userBranchOffice;
	}

	public UserBranchOffice removeUserBranchOffice(UserBranchOffice userBranchOffice) {
		getUserBranchOffices().remove(userBranchOffice);
		userBranchOffice.setUser(null);

		return userBranchOffice;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}


}