package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;


/**
 * The persistent class for the incidence_order database table.
 * 
 */
@Entity
@Table(name="incidence")
@NamedQuery(name="IncidenceOrder.findAll", query="SELECT i FROM Incidence i")
public class Incidence implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_incidence")
	private String idIncidence;

	private String reason;

	private Integer status;

	@Column(name="date_incidence")
	private Timestamp dateIncidence;

	//bi-directional many-to-one association to OrdersStock
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="fk_idorder", referencedColumnName="fk_idorder"),
		@JoinColumn(name="fk_idstock", referencedColumnName="fk_idstock")
		})
	private OrdersStock ordersStock;

	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="branch_office_id")
	private BranchOffice branchOffice;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="supplier_branch_office_id")
	private BranchOffice supplierBranchOffice;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="supplier_id")
	private Supplier supplier;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="product_id")
	private Product product;

	@Column(name="type_incidence")
	private Integer typeIncidence;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="stock_id")
	private Stock stock;



	// bi-directional ont-to-one association to BranchOffice
	@OneToOne(mappedBy = "incidence")
	private ReturnStock returnStock;

	public Incidence() {
	}



	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Timestamp getDateIncidence() {
		return dateIncidence;
	}

	public void setDateIncidence(Timestamp dateIncidence) {
		this.dateIncidence = dateIncidence;
	}

	public OrdersStock getOrdersStock() {
		return ordersStock;
	}

	public void setOrdersStock(OrdersStock ordersStock) {
		this.ordersStock = ordersStock;
	}

	public BranchOffice getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}


	public String getIdIncidence() {
		return idIncidence;
	}

	public void setIdIncidence(String idIncidence) {
		this.idIncidence = idIncidence;
	}

	public BranchOffice getSupplierBranchOffice() {
		return supplierBranchOffice;
	}

	public void setSupplierBranchOffice(BranchOffice supplierBranchOffice) {
		this.supplierBranchOffice = supplierBranchOffice;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getTypeIncidence() {
		return typeIncidence;
	}

	public void setTypeIncidence(Integer typeIncidence) {
		this.typeIncidence = typeIncidence;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public ReturnStock getReturnStock() {
		return returnStock;
	}

	public void setReturnStock(ReturnStock returnStock) {
		this.returnStock = returnStock;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}