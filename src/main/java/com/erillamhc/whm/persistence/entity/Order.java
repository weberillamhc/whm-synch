package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the orders database table.
 * 
 */
@Entity
@Table(name="orders")
@NamedQueries({
		@NamedQuery(name="Order.findAll", query="SELECT o FROM Order o"),
		@NamedQuery(name="Order.findBranchOfficeByOrder", query="SELECT usb FROM Order o JOIN o.userBranchOffice usb WHERE o.idOrder = :order"),
		@NamedQuery(name="Order.findByStatus", query="SELECT o FROM Order o JOIN o.userBranchOffice usbr WHERE usbr.user = :user AND usbr.branchOffice = :branch AND o.status = :status")
})

public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_order")
	private String idOrder;

	private Timestamp dateout;

	private Integer status;
	

	//bi-directional many-to-one association to OrdersStock
	@OneToMany(mappedBy="order")
	private List<OrdersStock> ordersStocks;

	//bi-directional many-to-one association to Shipment
	@OneToMany(mappedBy="order")
	private List<Shipment> shipments;
	
	//bi-directional many-to-one association to Order
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_user_branch",insertable = true, updatable = true)
	private UserBranchOffice userBranchOffice;

	public Order() {
	}

	public Order(String idOrder) {
		this.idOrder = idOrder;
	}

	public String getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(String idOrder) {
		this.idOrder = idOrder;
	}

	public Timestamp getDateout() {
		return dateout;
	}

	public void setDateout(Timestamp dateout) {
		this.dateout = dateout;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<OrdersStock> getOrdersStocks() {
		return ordersStocks;
	}

	public void setOrdersStocks(List<OrdersStock> ordersStocks) {
		this.ordersStocks = ordersStocks;
	}

	public List<Shipment> getShipments() {
		return shipments;
	}

	public void setShipments(List<Shipment> shipments) {
		this.shipments = shipments;
	}

	public UserBranchOffice getUserBranchOffice() {
		return userBranchOffice;
	}

	public void setUserBranchOffice(UserBranchOffice userBranchOffice) {
		this.userBranchOffice = userBranchOffice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	

}