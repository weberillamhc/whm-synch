package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the reception_stock database table.
 * 
 */
@Entity
@Table(name="return_stock")
@NamedQuery(name="ReturnStock.findAll", query="SELECT r FROM ReturnStock r")
public class ReturnStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_return_stock")
	private String idReturnStock;

	@Column(name="quantity")
	private Integer quantity;

	//bi-directional many-to-one association to Shipment
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="return_product_id")
	private ReturnProduct returnProduct;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="incidence_id")
	private Incidence incidence;


	public String getIdReturnStock() {
		return idReturnStock;
	}

	public void setIdReturnStock(String idReturnStock) {
		this.idReturnStock = idReturnStock;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ReturnProduct getReturnProduct() {
		return returnProduct;
	}

	public void setReturnProduct(ReturnProduct returnProduct) {
		this.returnProduct = returnProduct;
	}

	public Incidence getIncidence() {
		return incidence;
	}

	public void setIncidence(Incidence incidence) {
		this.incidence = incidence;
	}
}