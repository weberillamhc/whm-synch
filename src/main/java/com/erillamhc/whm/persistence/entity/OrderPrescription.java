package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the order_prescription database table.
 * 
 */
@Entity
@Table(name="order_prescription")
@NamedQuery(name="OrderPrescription.findAll", query="SELECT o FROM OrderPrescription o")
public class OrderPrescription implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private OrderPrescriptionPK id;

	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_orderid", insertable = false, updatable = false)
	private Order order;

	//bi-directional many-to-one association to Product
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="\"fk_prescriptionId\"", insertable = false, updatable = false)
	private MedicalPrescription medicalPrescription;
	
	private Timestamp date;

	public OrderPrescription() {
	}

	public OrderPrescriptionPK getId() {
		return this.id;
	}

	public void setId(OrderPrescriptionPK id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public MedicalPrescription getMedicalPrescription() {
		return medicalPrescription;
	}

	public void setMedicalPrescription(MedicalPrescription medicalPrescription) {
		this.medicalPrescription = medicalPrescription;
	}
}