package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the biddings database table.
 * 
 */
@Entity
@Table(name="biddings")
@NamedQueries({
	@NamedQuery(name="Bidding.findAll", query="SELECT b FROM Bidding b"),
	@NamedQuery(name="Bidding.findByBiddingKey", query="SELECT b FROM Bidding b WHERE b.biddingKey = :biddingKey")
})
public class Bidding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_bidding")
	private String idBidding;

	@Column(name="bidding_key")
	private String biddingKey;

	private String name;

	@Column(name="registration_date")
	private Timestamp registrationDate;

	//bi-directional many-to-one association to RequisitionGob
	@OneToMany(mappedBy="bidding")
	private List<RequisitionGob> requisitionGobs;

	//bi-directional many-to-one association to Requisition
	@OneToMany(mappedBy="bidding")
	private List<Requisition> requisitions;

	//bi-directional many-to-many association to Supplier
	@ManyToMany(mappedBy="biddings")
	private List<Supplier> suppliers;

	public Bidding() {
	}
	public Bidding(String idBidding) {
		this.idBidding = idBidding;
	}

	public String getIdBidding() {
		return this.idBidding;
	}

	public void setIdBidding(String idBidding) {
		this.idBidding = idBidding;
	}

	public String getBiddingKey() {
		return this.biddingKey;
	}

	public void setBiddingKey(String biddingKey) {
		this.biddingKey = biddingKey;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RequisitionGob> getRequisitionGobs() {
		return this.requisitionGobs;
	}

	public void setRequisitionGobs(List<RequisitionGob> requisitionGobs) {
		this.requisitionGobs = requisitionGobs;
	}

	public RequisitionGob addRequisitionGob(RequisitionGob requisitionGob) {
		getRequisitionGobs().add(requisitionGob);
		requisitionGob.setBidding(this);

		return requisitionGob;
	}

	public RequisitionGob removeRequisitionGob(RequisitionGob requisitionGob) {
		getRequisitionGobs().remove(requisitionGob);
		requisitionGob.setBidding(null);

		return requisitionGob;
	}

	public List<Requisition> getRequisitions() {
		return this.requisitions;
	}

	public void setRequisitions(List<Requisition> requisitions) {
		this.requisitions = requisitions;
	}

	public Requisition addRequisition(Requisition requisition) {
		getRequisitions().add(requisition);
		requisition.setBidding(this);

		return requisition;
	}

	public Requisition removeRequisition(Requisition requisition) {
		getRequisitions().remove(requisition);
		requisition.setBidding(null);

		return requisition;
	}

	public List<Supplier> getSuppliers() {
		return this.suppliers;
	}

	public void setSuppliers(List<Supplier> suppliers) {
		this.suppliers = suppliers;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}
}