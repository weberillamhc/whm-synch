package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the stock database table.
 */
@Entity
@Table(name = "stock_history")
@NamedQuery(name = "StockHistory.findAll", query = "SELECT s FROM StockHistory s")
public class StockHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_stock_history")
    private String idStockHistory;

    private Integer quantity;

    private String locationkey;

    private Integer status;

    @Column(name = "date_in")
    private Timestamp dateIn;

    //bi-directional many-to-one association to BranchOffice
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stock_id")
    private Stock stock;

    @Column(name = "user_id")
    private Integer userId;


    public StockHistory() {
    }

    public StockHistory(String idStock) {
        this.idStockHistory = idStockHistory;
    }

    public String getIdStockHistory() {
        return idStockHistory;
    }

    public void setIdStockHistory(String idStockHistory) {
        this.idStockHistory = idStockHistory;
    }

    public Timestamp getDateIn() {
        return this.dateIn;
    }

    public void setDateIn(Timestamp dateIn) {
        this.dateIn = dateIn;
    }

    public String getLocationkey() {
        return this.locationkey;
    }

    public void setLocationkey(String locationkey) {
        this.locationkey = locationkey;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}