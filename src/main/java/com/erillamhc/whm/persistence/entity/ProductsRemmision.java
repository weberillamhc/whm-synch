package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the products_remmision database table.
 * 
 */
@Entity
@Table(name="products_remmision")
@NamedQuery(name="ProductsRemmision.findAll", query="SELECT p FROM ProductsRemmision p")
public class ProductsRemmision implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_productremmison")
	private String idProductremmison;

	@Temporal(TemporalType.DATE)
	private Date expirationdate;

	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_remissionid")
	private Remmision remmision;

	private String lot;

	private Integer quantity;

	private double unitprice;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="productsRemmision")
	private List<Stock> stocks;

	public ProductsRemmision() {
	}

	public String getIdProductremmison() {
		return this.idProductremmison;
	}

	public void setIdProductremmison(String idProductremmison) {
		this.idProductremmison = idProductremmison;
	}

	public Date getExpirationdate() {
		return this.expirationdate;
	}

	public void setExpirationdate(Date expirationdate) {
		this.expirationdate = expirationdate;
	}

	
	public Remmision getRemmision() {
		return remmision;
	}

	public void setRemmision(Remmision remmision) {
		this.remmision = remmision;
	}

	public String getLot() {
		return this.lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public double getUnitprice() {
		return this.unitprice;
	}

	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}

	public List<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public Stock addStock(Stock stock) {
		getStocks().add(stock);
		stock.setProductsRemmision(this);

		return stock;
	}

	public Stock removeStock(Stock stock) {
		getStocks().remove(stock);
		stock.setProductsRemmision(null);

		return stock;
	}

}