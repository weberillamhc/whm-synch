package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the order_prescription database table.
 * 
 */
@Embeddable
public class OrderPrescriptionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="fk_orderid", insertable=false, updatable=false)
	private String fkOrderid;

	@Column(name="\"fk_prescriptionId\"", insertable=false, updatable=false)
	private String fk_prescriptionId;

	public OrderPrescriptionPK(String order,String prescription) {
		this.fkOrderid = order;
		this.fk_prescriptionId = prescription;
	}

	public OrderPrescriptionPK() {
	}

	public String getFkOrderid() {
		return this.fkOrderid;
	}
	public void setFkOrderid(String fkOrderid) {
		this.fkOrderid = fkOrderid;
	}
	public String getFk_prescriptionId() {
		return this.fk_prescriptionId;
	}
	public void setFk_prescriptionId(String fk_prescriptionId) {
		this.fk_prescriptionId = fk_prescriptionId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof OrderPrescriptionPK)) {
			return false;
		}
		OrderPrescriptionPK castOther = (OrderPrescriptionPK)other;
		return 
			this.fkOrderid.equals(castOther.fkOrderid)
			&& this.fk_prescriptionId.equals(castOther.fk_prescriptionId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fkOrderid.hashCode();
		hash = hash * prime + this.fk_prescriptionId.hashCode();
		
		return hash;
	}
}