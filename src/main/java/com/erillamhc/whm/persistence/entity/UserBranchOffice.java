package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;


/**
 * The persistent class for the UserBranchOffice database table.
 * 
 */
@Entity
@Table(name="user_branch_office")
@NamedQueries({
		@NamedQuery(name="UserBranchOffice.findAll", query="SELECT e FROM UserBranchOffice e"),
		@NamedQuery(name="UserBranchOffice.findByUserAndBranchOffice", query="SELECT e FROM UserBranchOffice e WHERE e.user = :user AND e.branchOffice = :branch")
})
public class UserBranchOffice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_user_branch_office")
	private Integer idUserBranchOffice;


	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="branch_office_id")
	private BranchOffice branchOffice;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;

	//bi-directional many-to-one association to Shipment
	@OneToMany(mappedBy="userBranchOffice")
	private List<Order> orders;

	public UserBranchOffice() {
	}

	public Integer getIdUserBranchOffice() {
		return idUserBranchOffice;
	}

	public void setIdUserBranchOffice(Integer idUserBranchOffice) {
		this.idUserBranchOffice = idUserBranchOffice;
	}
	public BranchOffice getBranchOffice() {
		return this.branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}