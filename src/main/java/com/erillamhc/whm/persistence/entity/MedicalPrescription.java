package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the medical_prescription database table.
 * 
 */
@Entity
@Table(name="medical_prescription")
@NamedQueries({
		@NamedQuery(name="MedicalPrescription.findAll", query="SELECT m FROM MedicalPrescription m"),
		@NamedQuery(name="MedicalPrescription.findPrescriptionByOrder", query="SELECT m FROM MedicalPrescription m JOIN m.orderPrescriptions orpr WHERE orpr.order = :order")
})
public class MedicalPrescription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_prescription")
	private String idPrescription;

	private String curp;

	@Column(name="medical_record")
	private String medicalRecord;

	@Column(name="prescription_key")
	private String prescriptionKey;
	

	//bi-directional many-to-one association to Doctor
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_doctorid")
	private Doctor doctor;
	
	//bi-directional many-to-one association to OrderPrescription
	@OneToMany(mappedBy="medicalPrescription")
	private List<OrderPrescription> orderPrescriptions;

	public MedicalPrescription() {
	}

	public MedicalPrescription(String idPrescription) {
		 this.idPrescription = idPrescription;
	}

	public String getIdPrescription() {
		return this.idPrescription;
	}

	public void setIdPrescription(String idPrescription) {
		this.idPrescription = idPrescription;
	}

	public String getCurp() {
		return this.curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getMedicalRecord() {
		return this.medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public String getPrescriptionKey() {
		return this.prescriptionKey;
	}

	public void setPrescriptionKey(String prescriptionKey) {
		this.prescriptionKey = prescriptionKey;
	}

	public Doctor getDoctor() {
		return this.doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public List<OrderPrescription> getOrderPrescriptions() {
		return orderPrescriptions;
	}

	public void setOrderPrescriptions(List<OrderPrescription> orderPrescriptions) {
		this.orderPrescriptions = orderPrescriptions;
	}
	
	



}