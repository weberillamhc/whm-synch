package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the movements database table.
 * 
 */
@Entity
@Table(name="synch")
@NamedQuery(name="Synch.findAll", query="SELECT s FROM Synch s")
public class Synch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_synch")
	private String idSynch;

	private String entity;

	private Integer status;

	@Column(name="type_synch")
	private Integer typeSynch;

	@Column(name="date_synch")
	private Timestamp dateSynch;

	@Column(name="registration_date")
	private Timestamp registrationDate;

	@Column(name="id_branch_office")
	private Integer idBranchOffice;

	@Column(name="id_user")
	private Integer idUser;

	@Column(name="id_entity")
	private String idEntity;

	@Column(name="id_entity_secondary")
	private String idEntitySecondary;

	@Column(name="data_entity")
	private String dataEntity;


	public Synch() {
	}

	public String getIdSynch() {
		return idSynch;
	}

	public void setIdSynch(String idSynch) {
		this.idSynch = idSynch;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTypeSynch() {
		return typeSynch;
	}

	public void setTypeSynch(Integer typeSynch) {
		this.typeSynch = typeSynch;
	}

	public Timestamp getDateSynch() {
		return dateSynch;
	}

	public void setDateSynch(Timestamp dateSynch) {
		this.dateSynch = dateSynch;
	}

	public Integer getIdBranchOffice() {
		return idBranchOffice;
	}

	public void setIdBranchOffice(Integer idBranchOffice) {
		this.idBranchOffice = idBranchOffice;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getIdEntity() {
		return idEntity;
	}

	public void setIdEntity(String idEntity) {
		this.idEntity = idEntity;
	}

	public String getIdEntitySecondary() {
		return idEntitySecondary;
	}

	public void setIdEntitySecondary(String idEntitySecondary) {
		this.idEntitySecondary = idEntitySecondary;
	}

	public String getDataEntity() {
		return dataEntity;
	}

	public void setDataEntity(String dataEntity) {
		this.dataEntity = dataEntity;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}
}