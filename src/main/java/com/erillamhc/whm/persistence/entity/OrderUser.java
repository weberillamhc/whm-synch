package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;


/**
 * The persistent class for the order_user database table.
 * 
 */
@Entity
@Table(name="order_user")
@NamedQueries({
		@NamedQuery(name="OrderUser.findAll", query="SELECT o FROM OrderUser o")
})

public class OrderUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_order_user")
	private Integer idOrderUser;


	//bi-directional many-to-one association to Order
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_order_id")
	private Order order;
	
	
	//bi-directional many-to-one association to Order
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_user_id")
	private User user;

	private Timestamp datemovement;

	private Integer status;

	public OrderUser() {
	}

	public Integer getIdOrderUser() {
		return this.idOrderUser;
	}

	public void setIdOrderUser(Integer idOrderUser) {
		this.idOrderUser = idOrderUser;
	}


	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getDatemovement() {
		return datemovement;
	}

	public void setDatemovement(Timestamp datemovement) {
		this.datemovement = datemovement;
	}
}