package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the reception_stock database table.
 * 
 */
@Entity
@Table(name="reception_stock")
@NamedQuery(name="ReceptionStock.findAll", query="SELECT r FROM ReceptionStock r")
public class ReceptionStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reception_stock")
	private String idReceptionStock;

	private Integer quantity;

	//bi-directional many-to-one association to OrdersStock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="fk_idorder", referencedColumnName="fk_idorder"),
		@JoinColumn(name="fk_idstock", referencedColumnName="fk_idstock")
		})
	private OrdersStock ordersStock;

	//bi-directional many-to-one association to Reception
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="reception_id")
	private Reception reception;

	public ReceptionStock() {
	}

	public String getIdReceptionStock() {
		return this.idReceptionStock;
	}

	public void setIdReceptionStock(String idReceptionStock) {
		this.idReceptionStock = idReceptionStock;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public OrdersStock getOrdersStock() {
		return this.ordersStock;
	}

	public void setOrdersStock(OrdersStock ordersStock) {
		this.ordersStock = ordersStock;
	}

	public Reception getReception() {
		return this.reception;
	}

	public void setReception(Reception reception) {
		this.reception = reception;
	}

}