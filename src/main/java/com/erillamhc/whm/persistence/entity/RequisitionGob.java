package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the requisition_gob database table.
 * 
 */
@Entity
@Table(name="requisition_gob")
@NamedQueries({
	@NamedQuery(name="RequisitionGob.findAll", query="SELECT r FROM RequisitionGob r"),
	@NamedQuery(name="RequisitionGob.findByRequisitionKey", query="SELECT r FROM RequisitionGob r WHERE r.requisitionKey =:requisitionKey"),
	@NamedQuery(name="RequisitionGob.findByRequisitiongKeyAndIdNotEqual", query="SELECT r FROM RequisitionGob r WHERE r.requisitionKey = :requisitionKey AND r.idRequisitionGob <> :idRequisitionGob")
})
public class RequisitionGob implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_requisition_gob")
	private String idRequisitionGob;

	@Column(name="date_requisition")
	private Timestamp dateRequisition;

	@Column(name="requisition_key")
	private String requisitionKey;

	private Integer status;

	//bi-directional many-to-one association to Remmision
	@OneToMany(mappedBy="requisitionGob")
	private List<Remmision> remmisions;

	//bi-directional many-to-one association to Bidding
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="bidding_id")
	private Bidding bidding;

	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="branch_office_id")
	private BranchOffice branchOffice;

	//bi-directional many-to-one association to FiscalFund
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="fiscal_fund_id")
	private FiscalFund fiscalFund;

	public RequisitionGob() {
	}
	public RequisitionGob(String idRequisitionGob) {
		this.idRequisitionGob = idRequisitionGob;
	}

	public String getIdRequisitionGob() {
		return this.idRequisitionGob;
	}

	public void setIdRequisitionGob(String idRequisitionGob) {
		this.idRequisitionGob = idRequisitionGob;
	}

	public Timestamp getDateRequisition() {
		return this.dateRequisition;
	}

	public void setDateRequisition(Timestamp dateRequisition) {
		this.dateRequisition = dateRequisition;
	}

	public String getRequisitionKey() {
		return this.requisitionKey;
	}

	public void setRequisitionKey(String requisitionKey) {
		this.requisitionKey = requisitionKey;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Remmision> getRemmisions() {
		return this.remmisions;
	}

	public void setRemmisions(List<Remmision> remmisions) {
		this.remmisions = remmisions;
	}

	public Remmision addRemmision(Remmision remmision) {
		getRemmisions().add(remmision);
		remmision.setRequisitionGob(this);

		return remmision;
	}

	public Remmision removeRemmision(Remmision remmision) {
		getRemmisions().remove(remmision);
		remmision.setRequisitionGob(null);

		return remmision;
	}

	public Bidding getBidding() {
		return this.bidding;
	}

	public void setBidding(Bidding bidding) {
		this.bidding = bidding;
	}

	public BranchOffice getBranchOffice() {
		return this.branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}

	public FiscalFund getFiscalFund() {
		return this.fiscalFund;
	}

	public void setFiscalFund(FiscalFund fiscalFund) {
		this.fiscalFund = fiscalFund;
	}

}