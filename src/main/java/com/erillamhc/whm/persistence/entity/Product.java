package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the products database table.
 * 
 */
@Entity
@Table(name="products")
@NamedQueries({
	@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p"),
	@NamedQuery(name="Product.findWithRequisiton", query="SELECT p FROM Product p INNER JOIN p.productsRequisitions pr WHERE p.idProduct = :id "),
	@NamedQuery(name="Product.findWithRemmision", query="SELECT p FROM Product p INNER JOIN p.stocks pr WHERE p.idProduct = :id "),
	@NamedQuery(name="Product.findByProductKeyAndIdNotEqual", query="SELECT p FROM Product p WHERE p.idProduct <> :idProduct AND p.productKey = :productKey"),
	@NamedQuery(name="Product.findByProductKey", query="SELECT p FROM Product p WHERE p.productKey = :productKey")
})
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_product")
	private Integer idProduct;

	private String description;

	private String name;

	@Column(name="product_key")
	private String productKey;

	@Column(name = "unit_masurement")
	private Integer unitMeasurement;

	//bi-directional many-to-many association to Category
	@ManyToMany
	@JoinTable(
		name="product_categories"
		, joinColumns={
			@JoinColumn(name="id_product")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_category")
			}
		)
	private List<Category> categories;

	//bi-directional many-to-one association to ProductsRequisition
	@OneToMany(mappedBy="product")
	private List<ProductsRequisition> productsRequisitions;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="product")
	private List<Stock> stocks;

	//bi-directional many-to-one association to StockProduct
	@OneToMany(mappedBy="product")
	private List<StockProduct> stockProducts;

	@OneToMany(mappedBy = "product")
	private List<Incidence> incidences;

	public Product() {
	}
	public Product(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public Integer getIdProduct() {
		return this.idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductKey() {
		return this.productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public List<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<ProductsRequisition> getProductsRequisitions() {
		return this.productsRequisitions;
	}

	public void setProductsRequisitions(List<ProductsRequisition> productsRequisitions) {
		this.productsRequisitions = productsRequisitions;
	}

	public ProductsRequisition addProductsRequisition(ProductsRequisition productsRequisition) {
		getProductsRequisitions().add(productsRequisition);
		productsRequisition.setProduct(this);

		return productsRequisition;
	}

	public ProductsRequisition removeProductsRequisition(ProductsRequisition productsRequisition) {
		getProductsRequisitions().remove(productsRequisition);
		productsRequisition.setProduct(null);

		return productsRequisition;
	}

	public List<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public Stock addStock(Stock stock) {
		getStocks().add(stock);
		stock.setProduct(this);

		return stock;
	}

	public Stock removeStock(Stock stock) {
		getStocks().remove(stock);
		stock.setProduct(null);

		return stock;
	}

	public List<StockProduct> getStockProducts() {
		return this.stockProducts;
	}

	public void setStockProducts(List<StockProduct> stockProducts) {
		this.stockProducts = stockProducts;
	}

	public StockProduct addStockProduct(StockProduct stockProduct) {
		getStockProducts().add(stockProduct);
		stockProduct.setProduct(this);

		return stockProduct;
	}

	public StockProduct removeStockProduct(StockProduct stockProduct) {
		getStockProducts().remove(stockProduct);
		stockProduct.setProduct(null);

		return stockProduct;
	}

	public Integer getUnitMeasurement() {
		return unitMeasurement;
	}

	public void setUnitMeasurement(Integer unitMeasurement) {
		this.unitMeasurement = unitMeasurement;
	}

	public List<Incidence> getIncidences() {
		return incidences;
	}

	public void setIncidences(List<Incidence> incidences) {
		this.incidences = incidences;
	}
}