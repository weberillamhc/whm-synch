package com.erillamhc.whm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the stock database table.
 * 
 */
@Entity
@NamedQuery(name="Stock.findAll", query="SELECT s FROM Stock s")
public class Stock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_stock")
	private String idStock;

	@Column(name="date_in")
	private Timestamp dateIn;

	//bi-directional many-to-one association to BranchOffice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_branchofficeid")
	private BranchOffice branchOffice;

	private String locationkey;

	//bi-directional many-to-one association to Product
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="product_id")
	private Product product;

	private Integer quantity;

	private Integer status;

	//bi-directional many-to-one association to ProductsRemmision
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="product_remmision_id")
	private ProductsRemmision productsRemmision;

	//bi-directional many-to-one association to Stock
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent")
	private Stock parent;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="parent")
	private List<Stock> stocks;

	//bi-directional many-to-one association to Stock
	@OneToMany(mappedBy="stock")
	private List<StockHistory> stockHistories;

	//bi-directional many-to-one association to OrdersStock
	@OneToMany(mappedBy="stock")
	private List<OrdersStock> ordersStock;


	@OneToMany(mappedBy = "stock")
	private List<Incidence> incidences;

	public Stock() {
	}

	public Stock(String idStock) {
		this.idStock = idStock;
	}
	
	public String getIdStock() {
		return this.idStock;
	}

	public void setIdStock(String idStock) {
		this.idStock = idStock;
	}

	public Timestamp getDateIn() {
		return this.dateIn;
	}

	public void setDateIn(Timestamp dateIn) {
		this.dateIn = dateIn;
	}


	public BranchOffice getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getLocationkey() {
		return this.locationkey;
	}

	public void setLocationkey(String locationkey) {
		this.locationkey = locationkey;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public ProductsRemmision getProductsRemmision() {
		return this.productsRemmision;
	}

	public void setProductsRemmision(ProductsRemmision productsRemmision) {
		this.productsRemmision = productsRemmision;
	}

	public Stock getParent() {
		return parent;
	}

	public void setParent(Stock parent) {
		this.parent = parent;
	}

	public List<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public Stock addStock(Stock stock) {
		getStocks().add(stock);
		stock.setParent(this);

		return stock;
	}

	public Stock removeStock(Stock stock) {
		getStocks().remove(stock);
		stock.setParent(null);

		return stock;
	}

	public List<StockHistory> getStockHistories() {
		return stockHistories;
	}

	public void setStockHistories(List<StockHistory> stockHistories) {
		this.stockHistories = stockHistories;
	}


	public List<OrdersStock> getOrdersStock() {
		return ordersStock;
	}

	public void setOrdersStock(List<OrdersStock> ordersStock) {
		this.ordersStock = ordersStock;
	}

	public List<Incidence> getIncidences() {
		return incidences;
	}

	public void setIncidences(List<Incidence> incidences) {
		this.incidences = incidences;
	}
}