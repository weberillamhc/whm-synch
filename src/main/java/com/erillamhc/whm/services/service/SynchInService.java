package com.erillamhc.whm.services.service;

import java.util.logging.Logger;

import com.erillamhc.whm.persistence.dao.*;
import com.erillamhc.whm.persistence.dto.*;
import com.erillamhc.whm.persistence.dto.synch.SynchFullDTO;
import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.util.SynchStatusEnum;
import com.erillamhc.whm.persistence.util.SynchTypeEnum;
import com.erillamhc.whm.persistence.util.UtilFacade;
import com.erillamhc.whm.services.app.aop.ValidationInterceptor;
import com.erillamhc.whm.services.util.UtilService;
import com.erillamhc.whm.services.util.constant.ConstantService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/*
 *
 * @author Fernando FH
 */
@Path(ConstantService.SYNCH_IN_SERVICE)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(ConstantService.APPLICATION_JSON_UTF8)
@ValidationInterceptor
public class SynchInService {

    private static final Logger LOGGER = Logger.getLogger(SynchInService.class.getName());

    @Inject
    private SynchDAO synchDAO;
    @Inject
    private BiddingDAO biddingDAO;
    @Inject
    private DecreaseDAO decreaseDAO;
    @Inject
    private DoctorDAO doctorDAO;
    @Inject
    private IncidenceDAO incidenceDAO;
    @Inject
    private OrderDAO orderDAO;
    @Inject
    private OrderStockDAO orderStockDAO;
    @Inject
    private StockDAO stockDAO;
    @Inject
    private StockHistoryDAO stockHistoryDAO;
    @Inject
    private UserDAO userDAO;
    @Inject
    private OrderUserDAO orderUserDAO;
    @Inject
    private ProductDAO productDAO;
    @Inject
    private ProductsRemmisionDAO productsRemmisionDAO;
    @Inject
    private ReceptionDAO receptionDAO;
    @Inject
    private ReceptionCommentDAO receptionCommentDAO;
    @Inject
    private ReceptionStockDAO receptionStockDAO;
    @Inject
    private RemmisionDAO remmisionDAO;
    @Inject
    private RequisitionGobDAO requisitionGobDAO;
    @Inject
    private ReturnDAO returnDAO;
    @Inject
    private ShipmentsDAO shipmentsDAO;
    @Inject
    private ReturnStockDAO returnStockDAO;
    @Inject
    private SupplierDAO supplierDAO;
    @Inject
    private BranchofficeDAO branchofficeDAO;
    @Inject
    private CategoryDAO categoryDAO;
    @Inject
    private FiscalFundDAO fiscalFundDAO;
    @Inject
    private JurisdictionDAO jurisdictionDAO;
    @Inject
    private RoleDAO roleDAO;

    private SynchFullDTO response = null;
    private String mensajeError = "";


    @POST
    @Path(ConstantService.SYNCH_UPDATE_SYNCH_STATUS)
    public Response updateStatus(SynchFullDTO request) {
        try {
            response = new SynchFullDTO();


            for (String idSynch : request.getIdSsynchronized()) {
                try {
                    Synch synch = synchDAO.findByID(idSynch);
                    if (synch != null) {
                        synch.setStatus(SynchStatusEnum.SINCRONIZADO.getValue());
                        synchDAO.update(synch);
                    }
                } catch (Exception e) {
                    LOGGER.info("Error actualizando status: " + e.getMessage());
                }
            }

        } catch (Exception e) {
            LOGGER.info("Error updateStatus" + e.getMessage());
            e.printStackTrace();
            return UtilService.getResponseOKOutcomeDTO(false);
        }
        return UtilService.getResponseOKOutcomeDTO(true);
    }

    @POST
    @Path(ConstantService.SYNCH_SYNCHRONIZED)
    public Response findAllSynchIn(SynchFullDTO request) {
        try {
            response = new SynchFullDTO();


            /* REALIZAR REGISTROS POR ORDEN DE DEPENDCIA, EL EJEMPLO COMO ESTE, PRIMERO LOS PROVEEDORES, PARA DESPUES REGISTRAR LA LICITACION,
            * */
            //Proveedores
            boolean result = synchSupplier(request.getRecords().getSuppliers(),
                    request.getUpdates().getSuppliers(),
                    request.getDeletes().getSuppliers(), request.getUpdatesStatus().getSuppliers());

            //Licitaciones
            result = synchBidding(request.getRecords().getBiddings(),
                    request.getUpdates().getBiddings(),
                    request.getDeletes().getBiddings(), request.getUpdatesStatus().getBiddings());


        } catch (Exception e) {
            LOGGER.info("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return UtilService.getResponseOKOutcomeDTO(response);
    }

    private boolean synchSupplier(List<SuppliersDTO> records, List<SuppliersDTO> updates, List<SuppliersDTO> deletes, List<SuppliersDTO> updateStatus) {
        try {

            //Registros
            for (SuppliersDTO suppliersDTO : records) {

                Supplier supplier = new Supplier();
                supplier.setName(suppliersDTO.getName().trim());
                supplier.setSupplierKey(suppliersDTO.getSupplierKey().trim());
                supplierDAO.save(supplier);

                //Se agrega a la lista de los ids sincronizados
                response.getIdSsynchronized().add(suppliersDTO.getIdSynch());
            }

            //Actualizaciones
            for (SuppliersDTO suppliersDTO : updates) {
                Supplier supplier = supplierDAO.findByID(suppliersDTO.getIdSupplier());
                if (supplier != null) {
                    supplier.setName(suppliersDTO.getName().trim());
                    supplier.setSupplierKey(suppliersDTO.getSupplierKey().trim());
                    supplierDAO.update(supplier);
                    //Se agrega a la lista de los ids sincronizados
                    response.getIdSsynchronized().add(suppliersDTO.getIdSynch());
                }
            }

            //Eliminaciones
            for (SuppliersDTO suppliersDTO : deletes) {
                Supplier supplier = supplierDAO.findByIDWithBiddings(suppliersDTO.getIdSupplier());
                if (supplier != null) {
                    if (UtilFacade.nonEmptyList(supplier.getBiddings())) {
                        supplierDAO.delete(supplier);
                        //Se agrega a la lista de los ids sincronizados
                        response.getIdSsynchronized().add(suppliersDTO.getIdSynch());
                    }
                }
            }

            //Actualizacion status
            for (SuppliersDTO suppliersDTO : updateStatus) {
                //La entidad no cuenta con status
                //Se agrega a la lista de los ids sincronizados
                response.getIdSsynchronized().add(suppliersDTO.getIdSynch());
            }

        } catch (Exception e) {
            LOGGER.info("Error " + e.getMessage());
            mensajeError = e.getMessage();
            e.printStackTrace();
            return false;
        }

        return true;
    }


    private boolean synchBidding(List<BiddingsDTO> records, List<BiddingsDTO> updates, List<BiddingsDTO> deletes, List<BiddingsDTO> updatesStatus) {
        try {

            //Registros
            for (BiddingsDTO biddingDTO : records) {
                Bidding bidding = new Bidding();
                bidding.setName(biddingDTO.getName().trim());
                bidding.setBiddingKey(biddingDTO.getBiddingKey().trim());
                bidding.setRegistrationDate(UtilFacade.toDate(biddingDTO.getRegistrationDate()));
                biddingDAO.save(bidding);

                if (biddingDTO.getSuppliers() != null) {
                    for (SuppliersDTO suppliersDTO : biddingDTO.getSuppliers()) {
                        Supplier supplier = supplierDAO.findByIDWithBiddings(suppliersDTO.getIdSupplier());
                        if (supplier != null) {
                            supplier.getBiddings().add(bidding);
                            supplierDAO.update(supplier);
                        }
                    }
                }
                //Se agrega a la lista de los ids sincronizados
                response.getIdSsynchronized().add(biddingDTO.getIdSynch());
            }

            //Actualizaciones
            for (BiddingsDTO biddingDTO : updates) {
                Bidding bidding = biddingDAO.findByID(biddingDTO.getIdBidding());
                if (bidding != null) {
                    bidding.setName(biddingDTO.getName().trim());
                    bidding.setBiddingKey(biddingDTO.getBiddingKey().trim());
                    bidding.setRegistrationDate(UtilFacade.toDate(biddingDTO.getRegistrationDate()));
                    biddingDAO.update(bidding);
                    //Se agrega a la lista de los ids sincronizados
                    response.getIdSsynchronized().add(biddingDTO.getIdSynch());
                }
            }

            //Eliminaciones
            for (BiddingsDTO biddingDTO : deletes) {
                Bidding bidding = biddingDAO.findByIdWithRequisitionGobs(biddingDTO.getIdBidding());
                if (bidding != null) {
                    if (UtilFacade.nonEmptyList(bidding.getRequisitionGobs())) {
                        biddingDAO.delete(bidding);
                        //Se agrega a la lista de los ids sincronizados
                        response.getIdSsynchronized().add(biddingDTO.getIdSynch());
                    }
                }
            }

            //Actualizacion status
            for (BiddingsDTO biddingDTO : updatesStatus) {
                //La entidad no cuenta con status
                //Se agrega a la lista de los ids sincronizados
                response.getIdSsynchronized().add(biddingDTO.getIdSynch());
            }

        } catch (Exception e) {
            LOGGER.info("Error " + e.getMessage());
            mensajeError = e.getMessage();
            e.printStackTrace();
            return false;
        }

        return true;
    }


}
