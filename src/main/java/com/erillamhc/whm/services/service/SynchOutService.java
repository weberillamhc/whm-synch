package com.erillamhc.whm.services.service;

import com.erillamhc.whm.persistence.dao.*;
import com.erillamhc.whm.persistence.dto.IdDTO;
import com.erillamhc.whm.persistence.dto.*;
import com.erillamhc.whm.persistence.dto.synch.SynchActionDTO;
import com.erillamhc.whm.persistence.dto.synch.SynchFullDTO;
import com.erillamhc.whm.persistence.dto.synch.SynchMinDTO;
import com.erillamhc.whm.persistence.entity.*;
import com.erillamhc.whm.persistence.exception.SimpleDAOException;
import com.erillamhc.whm.persistence.util.SynchTypeEnum;
import com.erillamhc.whm.persistence.util.UtilFacade;
import com.erillamhc.whm.services.app.aop.ValidationInterceptor;
import com.erillamhc.whm.services.util.UtilService;
import com.erillamhc.whm.services.util.constant.ConstantService;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
 *
 * @author Fernando FH
 */
@Path(ConstantService.SYNCH_OUT_SERVICE)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(ConstantService.APPLICATION_JSON_UTF8)
@ValidationInterceptor
public class SynchOutService {

    private static final Logger LOGGER = Logger.getLogger(SynchOutService.class.getName());

    @Inject
    private SynchDAO synchDAO;
    @Inject
    private BiddingDAO biddingDAO;
    @Inject
    private DecreaseDAO decreaseDAO;
    @Inject
    private DoctorDAO doctorDAO;
    @Inject
    private IncidenceDAO incidenceDAO;
    @Inject
    private OrderDAO orderDAO;
    @Inject
    private OrderStockDAO orderStockDAO;
    @Inject
    private StockDAO stockDAO;
    @Inject
    private StockHistoryDAO stockHistoryDAO;
    @Inject
    private UserDAO userDAO;
    @Inject
    private OrderUserDAO orderUserDAO;
    @Inject
    private ProductDAO productDAO;
    @Inject
    private ProductsRemmisionDAO productsRemmisionDAO;
    @Inject
    private ReceptionDAO receptionDAO;
    @Inject
    private ReceptionCommentDAO receptionCommentDAO;
    @Inject
    private ReceptionStockDAO receptionStockDAO;
    @Inject
    private RemmisionDAO remmisionDAO;
    @Inject
    private RequisitionGobDAO requisitionGobDAO;
    @Inject
    private ReturnDAO returnDAO;
    @Inject
    private ShipmentsDAO shipmentsDAO;
    @Inject
    private ReturnStockDAO returnStockDAO;
    @Inject
    private SupplierDAO supplierDAO;
    @Inject
    private BranchofficeDAO branchofficeDAO;
    @Inject
    private CategoryDAO categoryDAO;
    @Inject
    private FiscalFundDAO fiscalFundDAO;
    @Inject
    private JurisdictionDAO jurisdictionDAO;
    @Inject
    private RoleDAO roleDAO;

    private SynchFullDTO response = null;
    private String mensajeError = "";

    @GET
    @Path(ConstantService.SYNCH_NOT_SYNCHRONIZED)
    public Response findAllSynchOut() {

        try {
            boolean builded = false;
            List<Synch> objects = synchDAO.findAllByStatus(0);
            response = new SynchFullDTO();

            Integer prueba = -1;
            for (Synch obj : objects) {
                prueba++;
                if (prueba < 1000) {
                    mensajeError = "No definido";
                    if (obj.getTypeSynch() == SynchTypeEnum.ELIMINACION.getValue()) { //
                        builded = buildDelete(obj);
                    } else {
                        builded = buildEntity(obj, obj.getTypeSynch());
                    }

                    if (!builded) {
                        SynchDTO synchDTO = new SynchDTO();
                        synchDTO.setIdSynch(obj.getIdSynch());
                        synchDTO.setEntity(obj.getEntity());
                        synchDTO.setIdEntity(obj.getIdEntity());
                        synchDTO.setIdEntitySecondary(obj.getIdEntitySecondary());
                        synchDTO.setTypeSynch(obj.getTypeSynch());
                        synchDTO.setRegistrationDateSynch(UtilFacade.formatDate(obj.getRegistrationDate()));
                        synchDTO.setIdBranchOffice(obj.getIdBranchOffice());
                        synchDTO.setIdUser(obj.getIdUser());
                        synchDTO.setDataEntity(obj.getDataEntity());
                        synchDTO.setMeesage(mensajeError);
                        response.getErrores().add(synchDTO);
                    }
                }

            }
        } catch (Exception e) {
            LOGGER.info("Error " + e.getMessage());
            e.printStackTrace();
        }
        return UtilService.getResponseOKOutcomeDTO(response);
    }


    private boolean buildEntity(Synch synch, Integer type) {
        try {
            if (UtilFacade.ENTITY_BIDDING.equalsIgnoreCase(synch.getEntity())) {
                BiddingOutDTO biddingDTO = new BiddingOutDTO();
                Bidding bidding = biddingDAO.findByIdWithSuppliers(synch.getIdEntity());
                if (bidding != null) {
                    biddingDTO.setIdBidding(bidding.getIdBidding());
                    biddingDTO.setName(bidding.getName());
                    biddingDTO.setBiddingKey(bidding.getBiddingKey());
                    biddingDTO.setRegistrationDate(UtilFacade.formatDate(bidding.getRegistrationDate()));
                    if (bidding.getSuppliers() != null && !bidding.getSuppliers().isEmpty()) {
                        biddingDTO.setSuppliers(new ArrayList<>());
                        for (Supplier supplier : bidding.getSuppliers()) {
                            SuppliersDTO suppliersDTO = new SuppliersDTO();
                            suppliersDTO.setIdSupplier(supplier.getIdSupplier());
                            suppliersDTO.setName(supplier.getName());
                            suppliersDTO.setSupplierKey(supplier.getSupplierKey());
                            biddingDTO.getSuppliers().add(suppliersDTO);
                        }
                    }
                    setDataSynch(biddingDTO, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getBiddings().add(biddingDTO);
                            break;
                        case 2:
                            response.getUpdates().getBiddings().add(biddingDTO);
                            break;
                        case 4:
                            response.getUpdatesStatus().getBiddings().add(biddingDTO);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_DECREASE.equalsIgnoreCase(synch.getEntity())) {
                DecreaseDTO decreaseDTO = new DecreaseDTO();
                Decrease decrease = decreaseDAO.finByIdWithOrder(synch.getIdEntity());
                if (decrease != null) {
                    decreaseDTO.setIdDecrease(decrease.getIdDecrease());
                    decreaseDTO.setStatus(decrease.getStatus());
                    decreaseDTO.setDate(UtilFacade.formatDate(decrease.getDate()));
                    decreaseDTO.setIdOrder(decrease.getOrder().getIdOrder());
                    setDataSynch(decreaseDTO, synch);
                    switch (type) {
                        case 1:
                            response.getRecords().getDecreases().add(decreaseDTO);
                            break;
                        case 2:
                            response.getUpdates().getDecreases().add(decreaseDTO);
                            break;
                        case 4:
                            response.getUpdatesStatus().getDecreases().add(decreaseDTO);
                            break;
                    }

                }
            } else if (UtilFacade.ENTITY_DOCTOR.equalsIgnoreCase(synch.getEntity())) {
                DoctorDTO dto = new DoctorDTO();
                Doctor doctor = doctorDAO.findByIDWithBranch(synch.getIdEntity());
                if (doctor != null) {
                    dto.setIdDoctor(doctor.getIdDoctor());
                    dto.setName(doctor.getName());
                    dto.setLastName(doctor.getLastName());
                    dto.setBranchOfficeId(doctor.getBranchOffice().getIdBranchoffice());
                    dto.setProfessionalId(doctor.getProfessionalId());
                    setDataSynch(dto, synch);
                    switch (type) {
                        case 1:
                            response.getRecords().getDoctors().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getDoctors().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getDoctors().add(dto);
                            break;
                    }

                }
            } else if (UtilFacade.ENTITY_INCIDENCE.equalsIgnoreCase(synch.getEntity())) {
                IncidenceDTO dto = new IncidenceDTO();
                Incidence incidence = incidenceDAO.findByIdFull(synch.getIdEntity());
                if (incidence != null) {
                    dto.setIdIncidence(incidence.getIdIncidence());
                    dto.setReason(incidence.getReason());
                    dto.setDateIncidence(UtilFacade.formatDate(incidence.getDateIncidence()));
                    if (incidence.getBranchOffice() != null) {
                        dto.setIdBranchOffice(incidence.getBranchOffice().getIdBranchoffice());
                    }
                    if (incidence.getOrdersStock() != null) {
                        dto.setIdStock(incidence.getOrdersStock().getId().getFkIdstock());
                        dto.setIdOrder(incidence.getOrdersStock().getId().getFkIdorder());
                    }
                    if (incidence.getSupplierBranchOffice() != null) {
                        dto.setIdBranchOfficeSupplier(incidence.getSupplierBranchOffice().getIdBranchoffice());
                    }
                    if (incidence.getSupplier() != null) {
                        dto.setIdSupplier(incidence.getSupplier().getIdSupplier());
                    }
                    if (incidence.getProduct() != null) {
                        dto.setIdProduct(incidence.getProduct().getIdProduct());
                    }
                    dto.setTypeIncidence(incidence.getTypeIncidence());
                    if (incidence.getStock() != null) {
                        dto.setIdStockSimple(incidence.getStock().getIdStock());
                    }
                    dto.setStatus(incidence.getStatus());

                    setDataSynch(dto, synch);
                    switch (type) {
                        case 1:
                            response.getRecords().getIncidences().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getIncidences().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getIncidences().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_ORDER.equalsIgnoreCase(synch.getEntity())) {
                OrdersDTO dto = new OrdersDTO();
                Order order = orderDAO.findByIDWithOrigin(synch.getIdEntity());
                if (order != null) {
                    dto.setIdOrder(order.getIdOrder());
                    dto.setStatus(order.getStatus());
                    dto.setDateOut(UtilFacade.formatDate(order.getDateout()));
                    dto.setUserId(order.getUserBranchOffice().getUser().getIdUser());
                    dto.setOriginId(order.getUserBranchOffice().getBranchOffice().getIdBranchoffice());

                    setDataSynch(dto, synch);
                    switch (type) {
                        case 1:
                            response.getRecords().getOrders().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getOrders().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getOrders().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_ORDER_STOCK.equalsIgnoreCase(synch.getEntity())) {
                String idOrder = synch.getIdEntity();
                String idStock = synch.getIdEntitySecondary();
                Order orden = orderDAO.findByID(idOrder);
                Stock stock = stockDAO.findByID(idStock);
                if (orden != null && stock != null) {
                    OrdersStockDTO dto = new OrdersStockDTO();
                    OrdersStock orderStock = orderStockDAO.findByStockAndOrder(orden, stock);
                    if (orderStock != null) {
                        dto.setIdOrder(idOrder);
                        dto.setIdStock(idStock);
                        dto.setQuantity(orderStock.getQuantity());
                        dto.setPrescriptions(orderStock.getPrescriptions());
                        dto.setStatus(orderStock.getStatus());
                        dto.setStatus(orderStock.getStatus());
                        setDataSynch(dto, synch);
                        switch (type) {
                            case 1:
                                response.getRecords().getOrdersStock().add(dto);
                                break;
                            case 2:
                                response.getUpdates().getOrdersStock().add(dto);
                                break;
                            case 4:
                                response.getUpdatesStatus().getOrdersStock().add(dto);
                                break;
                        }
                    }
                }

            } else if (UtilFacade.ENTITY_ORDER_USER.equalsIgnoreCase(synch.getEntity())) {
                OrderUserDTO dto = new OrderUserDTO();
                OrderUser orderUser = orderUserDAO.findByIDWithUserAndOrder(Integer.parseInt(synch.getIdEntity()));
                if (orderUser != null) {
                    dto.setOrderId(orderUser.getOrder().getIdOrder());
                    dto.setUserId(orderUser.getUser().getIdUser());
                    dto.setStatus(orderUser.getStatus());
                    dto.setDatemovement(UtilFacade.formatDate(orderUser.getDatemovement()));
                    setDataSynch(dto, synch);
                    switch (type) {
                        case 1:
                            response.getRecords().getOrderUsers().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getOrderUsers().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getOrderUsers().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_PRODUCT.equalsIgnoreCase(synch.getEntity())) {
                ProductDTO dto = new ProductDTO();
                Product product = productDAO.findByIDWithCateories(Integer.parseInt(synch.getIdEntity()));
                if (product != null) {
                    dto.setIdProduct(product.getIdProduct());
                    dto.setName(product.getName());
                    dto.setDescription(product.getDescription());
                    dto.setProductKey(product.getProductKey());
                    dto.setUnitMeasurement(product.getUnitMeasurement());

                    if (product.getCategories() != null && !product.getCategories().isEmpty()) {
                        dto.setProductsCategories(new ArrayList<>());
                        for (Category category : product.getCategories()) {
                            CategoriesDTO categoriesDTO = new CategoriesDTO();
                            categoriesDTO.setIdCategory(category.getIdCategory());
                            categoriesDTO.setName(category.getName());
                            dto.getProductsCategories().add(categoriesDTO);
                        }
                    }

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getProducts().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getProducts().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getProducts().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_PRODUCT_REMMISION.equalsIgnoreCase(synch.getEntity())) {
                ProductRemmisionDTO dto = new ProductRemmisionDTO();
                ProductsRemmision productsRemmision = productsRemmisionDAO.findByIDWithRemmision(synch.getIdSynch());
                if (productsRemmision != null) {
                    dto.setIdProductRemmison(productsRemmision.getIdProductremmison());
                    dto.setLot(productsRemmision.getLot());
                    dto.setQuantity(productsRemmision.getQuantity());
                    dto.setExpirationDate(UtilFacade.formatDate(productsRemmision.getExpirationdate()));
                    dto.setRemmisionId(productsRemmision.getRemmision().getIdRemmision());
                    dto.setUnitprice(productsRemmision.getUnitprice());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getProductRemmisions().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getProductRemmisions().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getProductRemmisions().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_RECEPTION.equalsIgnoreCase(synch.getEntity())) {
                ReceptionDetailDTO dto = new ReceptionDetailDTO();
                Reception reception = receptionDAO.findByIDWithShipmentAndComment(synch.getIdEntity());
                if (reception != null) {
                    dto.setIdReception(reception.getIdReception());
                    dto.setDateReception(UtilFacade.formatDate(reception.getDateReception()));
                    dto.setStatus(reception.getStatus());
                    dto.setIdShipment(reception.getShipment().getIdShipment());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getReceptions().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getReceptions().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getReceptions().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_RECEPTION_COMMENT.equalsIgnoreCase(synch.getEntity())) {
                ReceptionDetailDTO dto = new ReceptionDetailDTO();
                ReceptionComment receptionComment = receptionCommentDAO.findByID(synch.getIdEntity());
                if (receptionComment != null) {
                    dto.setIdReceptionComment(receptionComment.getIdReceptionComment());
                    dto.setComment(receptionComment.getComment());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getReceptionsComments().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getReceptionsComments().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getReceptionsComments().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_RECEPTION_STOCK.equalsIgnoreCase(synch.getEntity())) {
                ReceptionOrderStockDTO dto = new ReceptionOrderStockDTO();
                ReceptionStock receptionStock = receptionStockDAO.findByIDWithReceptionAndOrderStock(synch.getIdEntity());
                if (receptionStock != null) {
                    dto.setIdReceptionStock(receptionStock.getIdReceptionStock());
                    dto.setReceptionId(receptionStock.getReception().getIdReception());
                    dto.setIdOrder(receptionStock.getOrdersStock().getOrder().getIdOrder());
                    dto.setIdStock(receptionStock.getOrdersStock().getStock().getIdStock());
                    dto.setQuantity(receptionStock.getQuantity());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getReceptionStocks().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getReceptionStocks().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getReceptionStocks().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_REMMISION.equalsIgnoreCase(synch.getEntity())) {
                RemmisionsDTO dto = new RemmisionsDTO();

                Remmision remmision = remmisionDAO.findByIdWithUserAndRequisitionGob(synch.getIdEntity());
                if (remmision != null) {
                    dto.setIdRemmision(remmision.getIdRemmision());
                    dto.setDateInput(UtilFacade.formatDate(remmision.getDateinput()));
                    dto.setStatus(remmision.getStatus());
                    dto.setRemissionkey(remmision.getRemmisionKey());
                    dto.setRequisitionGobId(remmision.getRequisitionGob().getIdRequisitionGob());
                    if (remmision.getUser() != null) {
                        dto.setIdUser(remmision.getUser().getIdUser());
                    }

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getRemmisions().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getRemmisions().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getRemmisions().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_REQUISITION_GOB.equalsIgnoreCase(synch.getEntity())) {
                RequisitionGobDTO dto = new RequisitionGobDTO();
                RequisitionGob requisitionGob = requisitionGobDAO.findByIDFiscalFundAndBidding(synch.getIdEntity());
                if (requisitionGob != null) {
                    dto.setIdRequisitionGob(requisitionGob.getIdRequisitionGob());
                    dto.setRequisitionKey(requisitionGob.getRequisitionKey());
                    dto.setStatus(requisitionGob.getStatus());
                    dto.setDateRequisition(UtilFacade.formatDate(requisitionGob.getDateRequisition()));
                    dto.setBranchOfficeId(requisitionGob.getBranchOffice().getIdBranchoffice());
                    dto.setBiddingId(requisitionGob.getBidding().getIdBidding());
                    dto.setFiscalFundId(requisitionGob.getFiscalFund().getIdFiscalfund());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getRequisitionsGob().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getRequisitionsGob().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getRequisitionsGob().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_RETURN_PRODUCT.equalsIgnoreCase(synch.getEntity())) {
                ReturnDTO dto = new ReturnDTO();
                //Pendiente hasta modulo de devoluciones
//                ReturnProduct returnProduct = returnDAO.findByIDWithBranchsAndSupplier(synch.getIdEntity());
//                RequisitionGob requisitionGob = requisitionGobDAO.findByIDFiscalFundAndBidding(synch.getIdEntity());
//                if (requisitionGob != null) {
//                    dto.setIdRequisitionGob(requisitionGob.getIdRequisitionGob());
//                    dto.setRequisitionKey(requisitionGob.getRequisitionKey());
//                    dto.setStatus(requisitionGob.getStatus());
//                    dto.setDateRequisition(UtilFacade.formatDate(requisitionGob.getDateRequisition()));
//                    dto.setBranchOfficeId(requisitionGob.getBranchOffice().getIdBranchoffice());
//                    dto.setBiddingId(requisitionGob.getBidding().getIdBidding());
//                    dto.setFiscalFundId(requisitionGob.getFiscalFund().getIdFiscalfund());
//
//                    setDataSynch(dto, synch);
//
//                    switch (type) {
//                        case 1:
//                            response.getRecords().getRequisitionsGob().add(dto);
//                            break;
//                        case 2:
//                            response.getUpdates().getRequisitionsGob().add(dto);
//                            break;
//                        case 4:
//                            response.getUpdatesStatus().getRequisitionsGob().add(dto);
//                            break;
//                    }
//                }
            } else if (UtilFacade.ENTITY_SHIPMENT.equalsIgnoreCase(synch.getEntity())) {
                ShipmentsDTO dto = new ShipmentsDTO();
                Shipment shipment = shipmentsDAO.findByIDWithDestinationAndOrder(synch.getIdEntity());
                if (shipment != null) {
                    dto.setIdShipment(shipment.getIdShipment());
                    dto.setDriver(shipment.getDriver());
                    dto.setTag(shipment.getTag());
                    dto.setStatus(shipment.getStatus());
                    dto.setDateshipment(UtilFacade.formatDate(shipment.getDateshipment()));
                    dto.setDestinationId(shipment.getBranchOffice().getIdBranchoffice());
                    dto.setOrderId(shipment.getOrder().getIdOrder());
                    dto.setNumber(shipment.getNumber());
                    dto.setLicenseplate(shipment.getLicenseplate());


                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getShipments().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getShipments().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getShipments().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_STOCK.equalsIgnoreCase(synch.getEntity())) {
                StockSimpleDTO dto = new StockSimpleDTO();
                Stock stock = stockDAO.findByIDWithBranchOfficeAndParentAndProductAndProductRemmision(synch.getIdEntity());
                if (stock != null) {
                    dto.setIdStock(stock.getIdStock());
                    dto.setQuantity(stock.getQuantity());
                    dto.setLocationkey(stock.getLocationkey());
                    dto.setStatus(stock.getStatus());
                    dto.setBranchOfficeId(stock.getBranchOffice().getIdBranchoffice());
                    if (stock.getParent() != null) {
                        dto.setParentId(stock.getParent().getIdStock());
                    }
                    dto.setDateIn(UtilFacade.formatDate(stock.getDateIn()));
                    dto.setProductId(stock.getProduct().getIdProduct());
                    dto.setProductRemmisionId(stock.getProductsRemmision().getIdProductremmison());


                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getStocks().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getStocks().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getStocks().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_STOCK_HISTORY.equalsIgnoreCase(synch.getEntity())) {
                StockHistoryDTO dto = new StockHistoryDTO();
                StockHistory stockHistory = stockHistoryDAO.findByIDWithParent(synch.getIdEntity());
                if (stockHistory != null) {
                    dto.setIdStockHistory(stockHistory.getIdStockHistory());
                    dto.setQuantity(stockHistory.getQuantity());
                    dto.setLocationkey(stockHistory.getLocationkey());
                    dto.setStatus(stockHistory.getStatus());
                    dto.setDateIn(UtilFacade.formatDate(stockHistory.getDateIn()));
                    dto.setStockId(stockHistory.getStock().getIdStock());
                    dto.setUserId(stockHistory.getUserId());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getStocksHistory().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getStocksHistory().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getStocksHistory().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_SUPPLIER.equalsIgnoreCase(synch.getEntity())) {
                SuppliersDTO dto = new SuppliersDTO();
                Supplier supplier = supplierDAO.findByID(Integer.parseInt(synch.getIdEntity()));
                if (supplier != null) {
                    dto.setIdSupplier(supplier.getIdSupplier());
                    dto.setName(supplier.getName());
                    dto.setSupplierKey(supplier.getSupplierKey());

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getSuppliers().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getSuppliers().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getSuppliers().add(dto);
                            break;
                    }
                }

            } else if (UtilFacade.ENTITY_BRANCH_OFFICE.equalsIgnoreCase(synch.getEntity())) {
                BranchOfficeDTO dto = new BranchOfficeDTO();
                BranchOffice branchOffice = branchofficeDAO.findByIdWithParentAndJurisdiction(Integer.parseInt(synch.getIdEntity()));
                if (branchOffice != null) {
                    dto.setIdBranchoffice(branchOffice.getIdBranchoffice());
                    dto.setName(branchOffice.getName());
                    dto.setType(branchOffice.getType());
                    dto.setJurisdictionId(branchOffice.getJurisdiction().getIdJurisdiction());
                    if (branchOffice.getBranchOffice() != null) {
                        dto.setParent(branchOffice.getBranchOffice().getIdBranchoffice());
                    }
                    dto.setBranchOfficeKey(branchOffice.getBranchOfficeKey());
                    dto.setWarehouseId(branchOffice.getWarehouseId());
                    dto.setStatus(branchOffice.getStatus());
                    if (branchOffice.getAddressBranchOffice() != null) {
                        dto.setFullAddress(branchOffice.getAddressBranchOffice().getFulladdress());
                    }

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getBranchOffices().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getBranchOffices().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getBranchOffices().add(dto);
                            break;
                    }
                }

            } else if (UtilFacade.ENTITY_USER.equalsIgnoreCase(synch.getEntity())) {
                UsersDTO dto = new UsersDTO();
                User user = userDAO.findByIdWithBranchOfficesAndRole(Integer.parseInt(synch.getIdEntity()));
                if (user != null) {
                    dto.setIdUser(user.getIdUser());
                    dto.setName(user.getName());
                    dto.setLastname(user.getLastname());
                    dto.setEmail(user.getEmail());
                    dto.setPassword(user.getPassword());
                    dto.setAvailable(user.getAvailable());
                    dto.setRoleId(user.getRole().getIdRole());
                    if (user.getUserBranchOffices() != null) {
                        dto.setUserBranchOffice(new ArrayList<>());
                        for (UserBranchOffice ub : user.getUserBranchOffices()) {
                            UserBranchOfficeDTO ubDTO = new UserBranchOfficeDTO();
                            ubDTO.setIdUserBranchOffice(ub.getIdUserBranchOffice());
                            ubDTO.setUserId(user.getIdUser());
                            ubDTO.setBranchOfficeId(ub.getBranchOffice().getIdBranchoffice());
                            dto.getUserBranchOffice().add(ubDTO);
                        }
                    }

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getUsers().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getUsers().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getUsers().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_CATEGORIES.equalsIgnoreCase(synch.getEntity())) {
                CategoriesDTO dto = new CategoriesDTO();
                Category category = categoryDAO.findByID(Integer.parseInt(synch.getIdEntity()));
                if (category != null) {
                    dto.setIdCategory(category.getIdCategory());
                    dto.setName(category.getName());
                    dto.setDescription(category.getDescription());
                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getCategories().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getCategories().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getCategories().add(dto);
                            break;
                    }
                }
            } else if (UtilFacade.ENTITY_FISCAL_FUND.equalsIgnoreCase(synch.getEntity())) {
                FiscalFundDTO dto = new FiscalFundDTO();
                FiscalFund fiscalFund = fiscalFundDAO.findByID(Integer.parseInt(synch.getIdEntity()));
                if (fiscalFund != null) {
                    dto.setIdFiscalfund(fiscalFund.getIdFiscalfund());
                    dto.setName(fiscalFund.getName());
                    dto.setKey(fiscalFund.getKey());
                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getFiscalFunds().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getFiscalFunds().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getFiscalFunds().add(dto);
                            break;
                    }
                }
            }else if (UtilFacade.ENTITY_JURISDICTION.equalsIgnoreCase(synch.getEntity())) {
                JurisdictionsDTO dto = new JurisdictionsDTO();
                Jurisdiction jurisdiction = jurisdictionDAO.findByID(Integer.parseInt(synch.getIdEntity()));
                if (jurisdiction != null) {
                    dto.setIdJurisdiction(jurisdiction.getIdJurisdiction());
                    dto.setJurisdictionname(jurisdiction.getJurisdictionname());
                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getJurisdictions().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getJurisdictions().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getJurisdictions().add(dto);
                            break;
                    }
                }
            }else if (UtilFacade.ENTITY_ROLE.equalsIgnoreCase(synch.getEntity())) {
                RolesDTO dto = new RolesDTO();
                Role rol = roleDAO.findByIdWithPermissions(synch.getIdEntity());
                if (rol != null) {
                    dto.setIdRole(rol.getIdRole());
                    dto.setName(rol.getName());
                    dto.setDescription(rol.getDescription());
                    dto.setPermmisions(new ArrayList<>());

                    if (rol.getPermissions() != null) {
                        for(Permission permission: rol.getPermissions()){
                            RolesPermissionsDTO rl = new RolesPermissionsDTO();
                            rl.setPermissionId(permission.getIdPermission());
                            rl.setRoleId(rol.getIdRole());
                        }

                    }

                    setDataSynch(dto, synch);

                    switch (type) {
                        case 1:
                            response.getRecords().getRoles().add(dto);
                            break;
                        case 2:
                            response.getUpdates().getRoles().add(dto);
                            break;
                        case 4:
                            response.getUpdatesStatus().getRoles().add(dto);
                            break;
                    }
                }
            }
            return true;
        } catch (Exception e) {
            LOGGER.info("Error " + e.getMessage());
            mensajeError = e.getMessage();
            e.printStackTrace();
            return false;
        }
    }


    private boolean buildDelete(Synch synch) {
        try {
            if (UtilFacade.ENTITY_BIDDING.equalsIgnoreCase(synch.getEntity())) {
                BiddingsDTO biddingDTO = new BiddingsDTO();
                biddingDTO.setIdBidding(synch.getIdEntity());
                setDataSynch(biddingDTO, synch);
                response.getDeletes().getBiddings().add(biddingDTO);
            } else if (UtilFacade.ENTITY_DECREASE.equalsIgnoreCase(synch.getEntity())) {
                DecreaseDTO decreaseDTO = new DecreaseDTO();
                decreaseDTO.setIdDecrease(synch.getIdEntity());
                setDataSynch(decreaseDTO, synch);
                response.getDeletes().getDecreases().add(decreaseDTO);
            } else if (UtilFacade.ENTITY_DOCTOR.equalsIgnoreCase(synch.getEntity())) {
                DoctorDTO dto = new DoctorDTO();
                dto.setIdDoctor(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getDoctors().add(dto);
            } else if (UtilFacade.ENTITY_INCIDENCE.equalsIgnoreCase(synch.getEntity())) {
                IncidenceDTO dto = new IncidenceDTO();
                dto.setIdIncidence(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getIncidences().add(dto);
            } else if (UtilFacade.ENTITY_ORDER.equalsIgnoreCase(synch.getEntity())) {
                OrdersDTO dto = new OrdersDTO();
                dto.setIdOrder(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getOrders().add(dto);
            } else if (UtilFacade.ENTITY_ORDER_STOCK.equalsIgnoreCase(synch.getEntity())) {
                String idOrder = synch.getIdEntity();
                String idStock = synch.getIdEntitySecondary();
                OrdersStockDTO dto = new OrdersStockDTO();
                dto.setIdOrder(idOrder);
                dto.setIdStock(idStock);
                setDataSynch(dto, synch);
                response.getDeletes().getOrdersStock().add(dto);
            } else if (UtilFacade.ENTITY_ORDER_USER.equalsIgnoreCase(synch.getEntity())) {
                OrderUserDTO dto = new OrderUserDTO();
                dto.setIdOrderUser(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getOrderUsers().add(dto);
            } else if (UtilFacade.ENTITY_PRODUCT.equalsIgnoreCase(synch.getEntity())) {
                ProductDTO dto = new ProductDTO();
                dto.setIdProduct(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getProducts().add(dto);
            } else if (UtilFacade.ENTITY_PRODUCT_REMMISION.equalsIgnoreCase(synch.getEntity())) {
                ProductRemmisionDTO dto = new ProductRemmisionDTO();
                dto.setIdProductRemmison(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getProductRemmisions().add(dto);
            } else if (UtilFacade.ENTITY_RECEPTION.equalsIgnoreCase(synch.getEntity())) {
                ReceptionDetailDTO dto = new ReceptionDetailDTO();
                dto.setIdReception(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getReceptions().add(dto);
            } else if (UtilFacade.ENTITY_RECEPTION_COMMENT.equalsIgnoreCase(synch.getEntity())) {
                ReceptionDetailDTO dto = new ReceptionDetailDTO();
                dto.setIdReceptionComment(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getReceptionsComments().add(dto);
            } else if (UtilFacade.ENTITY_RECEPTION_STOCK.equalsIgnoreCase(synch.getEntity())) {
                ReceptionOrderStockDTO dto = new ReceptionOrderStockDTO();
                dto.setIdReceptionStock(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getReceptionStocks().add(dto);
            } else if (UtilFacade.ENTITY_REMMISION.equalsIgnoreCase(synch.getEntity())) {
                RemmisionsDTO dto = new RemmisionsDTO();
                dto.setIdRemmision(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getRemmisions().add(dto);
            } else if (UtilFacade.ENTITY_REQUISITION_GOB.equalsIgnoreCase(synch.getEntity())) {
                RequisitionGobDTO dto = new RequisitionGobDTO();
                dto.setIdRequisitionGob(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getRequisitionsGob().add(dto);
            } else if (UtilFacade.ENTITY_SHIPMENT.equalsIgnoreCase(synch.getEntity())) {
                ShipmentsDTO dto = new ShipmentsDTO();
                dto.setIdShipment(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getShipments().add(dto);
            } else if (UtilFacade.ENTITY_STOCK.equalsIgnoreCase(synch.getEntity())) {
                StockSimpleDTO dto = new StockSimpleDTO();
                dto.setIdStock(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getStocks().add(dto);
            } else if (UtilFacade.ENTITY_STOCK_HISTORY.equalsIgnoreCase(synch.getEntity())) {
                StockHistoryDTO dto = new StockHistoryDTO();
                dto.setIdStockHistory(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getStocksHistory().add(dto);
            } else if (UtilFacade.ENTITY_SUPPLIER.equalsIgnoreCase(synch.getEntity())) {
                SuppliersDTO dto = new SuppliersDTO();
                dto.setIdSupplier(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getSuppliers().add(dto);
            } else if (UtilFacade.ENTITY_BRANCH_OFFICE.equalsIgnoreCase(synch.getEntity())) {
                BranchOfficeDTO dto = new BranchOfficeDTO();
                dto.setIdBranchoffice(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getBranchOffices().add(dto);
            } else if (UtilFacade.ENTITY_USER.equalsIgnoreCase(synch.getEntity())) {
                UsersDTO dto = new UsersDTO();
                dto.setIdUser(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getUsers().add(dto);
            } else if (UtilFacade.ENTITY_CATEGORIES.equalsIgnoreCase(synch.getEntity())) {
                CategoriesDTO dto = new CategoriesDTO();
                dto.setIdCategory(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getCategories().add(dto);
            } else if (UtilFacade.ENTITY_FISCAL_FUND.equalsIgnoreCase(synch.getEntity())) {
                FiscalFundDTO dto = new FiscalFundDTO();
                dto.setIdFiscalfund(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getFiscalFunds().add(dto);
            }else if (UtilFacade.ENTITY_JURISDICTION.equalsIgnoreCase(synch.getEntity())) {
                JurisdictionsDTO dto = new JurisdictionsDTO();
                dto.setIdJurisdiction(Integer.parseInt(synch.getIdEntity()));
                setDataSynch(dto, synch);
                response.getDeletes().getJurisdictions().add(dto);
            }else if (UtilFacade.ENTITY_ROLE.equalsIgnoreCase(synch.getEntity())) {
                RolesDTO dto = new RolesDTO();
                dto.setIdRole(synch.getIdEntity());
                setDataSynch(dto, synch);
                response.getDeletes().getRoles().add(dto);
            }
            return true;
        } catch (Exception e) {
            LOGGER.info("Error " + e.getMessage());
            mensajeError = e.getMessage();
            e.printStackTrace();
            return false;
        }
    }

    private void setDataSynch(SynchMinDTO dto, Synch synch) {
        dto.setIdSynch(synch.getIdSynch());
        dto.setEntitySynch(synch.getEntity());
        dto.setStatusSynch(synch.getStatus());
        dto.setRegistrationDateSynch(UtilFacade.formatDate(synch.getRegistrationDate()));
        dto.setDataEntitySynch(synch.getDataEntity());
        dto.setTypeSynch(synch.getTypeSynch());
    }


// ***********************************         PENDIENTES DE VER SU USO **********************************/
    //	public static String ENTITY_MOVEMENT= "movement";

//	public static String ENTITY_ORDER_PRESCRIPTION = "order_prescription";

    //	public static String ENTITY_MEDICAL_PRESCRIPTION = "medical_prescription";
//	public static String ENTITY_PRODUCT_REQUISITION = "products_requisition";
//	public static String ENTITY_REQUISITION = "requisitions";
//	public static String ENTITY_STOCK_PRODUCTS = "stock_products";


    /*********************************      PENDIENTE HASTA REALIZAR M�DULO *****************************/
    //	public static String ENTITY_RETURN_PRODUCT = "return_product";
//	public static String ENTITY_RETURN_STOCK = "return_stock";

}
