package com.erillamhc.whm.services.util.constant;

import javax.ws.rs.core.MediaType;

public class ConstantService {

    public static final String HEADER_SERVICE_KEY = "SRVC_KEY";
    public static final String HEADER_AUTH_TOKEN = "AUTH_TKN";
    public static final String APPLICATION_PATH = "app";
    public static final String APPLICATION_PATH_SEC = "sc/";
    public static final String APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON + "; charset=UTF-8";
    public static final String SYNCH_OUT_SERVICE = "synoutsv";
    public static final String SYNCH_IN_SERVICE = "syninsv";
    public static final String SYNCH_NOT_SYNCHRONIZED = "syn01";
    public static final String SYNCH_SYNCHRONIZED = "syn02";
    public static final String SYNCH_UPDATE_SYNCH_STATUS = "syn03";




    private ConstantService() {}
}